<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BidangRequest;
use App\Bidang;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class BidangController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the bidang
        $bidangs = Bidang::all();
        // Show the page
        return view('admin.bidang.index', compact('bidangs'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $bidangs = Bidang::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','namaBidang','created_at'])->orderBy('id', 'DESC')->get();
        return DataTables::of($bidangs)
            ->editColumn('created_at',function(Bidang $bidangs) {
                return $bidangs->created_at->diffForHumans();
            })
 
            ->addColumn('actions',function($bidangs) {
                $actions = '<a onclick="showForm('.$bidangs->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view bidang"></i></a>
                            <a onclick="editForm('.$bidangs->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update bidang"></i></a>';
                $actions .= '<a onclick="deleteForm('.$bidangs->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete bidang"></i></a>';
              
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $bidangs = Bidang::where('id',$id)->first();    
        return json_encode($bidangs);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BidangRequest $request)
    {

        $bidangs = new Bidang($request->all());

        if ($bidangs->save()) {
            return json_encode($bidangs);
        }
         else {
            return Redirect::route('admin.bidang')->withInput()->with('error', 'Gagal menyimpan data');
        }
    }


    public function edit($bidangs)
    {
        
    }

    public function update(BidangRequest $request, $id)
    {

        $bidangs = Bidang::find($id);
        if ($bidangs->update($request->all())) {
            return json_encode($bidangs);
        } else {
            return Redirect::route('admin/bidang')->withInput()->with('error', trans('bidang/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $bidangs = Bidang::find($id);
        if ($bidangs->delete()) {
            return json_encode($bidangs);
        } else {
            return Redirect::route('admin/bidang')->withInput()->with('error', trans('bidang/message.error.delete'));
        }
    }
}
