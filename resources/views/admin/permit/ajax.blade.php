<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.permit.data') !!}',
        columns: [
            // { data: 'rownum' },
            { data: 'mahasiswa', name: 'mahasiswa' },
            { data: 'unit', name: 'unit' },
            { data: 'noPermit', name: 'noPermit' },
            { data: 'tglDikeluarkan', name:'tglDikeluarkan'},
            { data: 'tglBerakhir', name:'tglBerakhir'},
            { data: 'sisa_waktu'},
            { data: 'file'},
            { data: 'status_permit', name:'status_permit'},
            @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
            @endif
            
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

// OPEN MODAL Sex
function StatusModal(status){
    $('#StatusModal').modal('show');
    $('#StatusModal .modal-title').text('Data Permit Aktif');
    if(status == 'H'){
        $('#StatusModal .modal-title').text('Data Permit Tidak Aktif');
    }

    $(function() {
        // Datatable untuk Kelamin
     $('#table_status').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        bDestroy: true,
        ajax: '{!! url('admin/permit/data_status') !!}'+"/"+status,
        columns: [
            // { data: 'rownum' },
            { data: 'mahasiswa', name: 'mahasiswa' },
            { data: 'unit', name: 'unit' },
            { data: 'noPermit', name: 'noPermit' },
            { data: 'tglDikeluarkan', name:'tglDikeluarkan'},
            { data: 'tglBerakhir', name:'tglBerakhir'},
            { data: 'sisa_waktu'},
            { data: 'file'},
            { data: 'status_permit', name:'status_permit'},
            @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
            @endif
           
        ],

    });
        
    });

}

// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Permit');
    $('.btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Permit');
    $("#dokumenPermit").attr('required',false); 

    $('.btnSave').removeClass('hide');
    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('admin/permit') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

                $('#id').val(data.id);
                $("#npm option[value='" + data.npm + "']").prop("selected", true).change();
                $("#unit_id option[value='" + data.unit_id + "']").prop("selected", true).change();
                $("#noPermit").val(data.noPermit);
                $("#tglDikeluarkan").val(data.tglDikeluarkan);
                $("#tglBerakhir").val(data.tglBerakhir);
                // $("#dokumenPermit").val(data.dokumenPermit);
        
                if(data.status_permit == 1){

                    // document.getElementById('status_permit').attr("checked", true);
                    document.getElementById('status_permit').checked = true;
                }else{
                    document.getElementById('status_permit').attr("checked", false);
                    // document.getElementById('status_permit').checked = false;

                }
              

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Permit');
    $('.btnSave').addClass('hide');

    $.ajax({
        url: "{{ url('admin/permit') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#npm option[value='" + data.npm + "']").prop("selected", true).change();
                $("#jPendidikan option[value='" + data.jPendidikan + "']").prop("selected", true).change();
                $("#noPermit").val(data.noPermit).attr('readonly', true);
                $("#tglDikeluarkan").val(data.tglDikeluarkan).attr('readonly', true);
                $("#tglBerakhir").val(data.tglBerakhir).attr('readonly', true);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete permit');

    $.ajax({
        url: "{{ url('admin/permit') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#pesan_hapus").text(data.noPermit);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();

            url = "{{ url('admin/permit/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.permit.store') }}";
            } 
            else {
            url = "{{ url('admin/permit') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                    $('.btnSave').attr('disabled', true);
                    $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
        
                // processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    
                
                $('#addModal').modal('hide');
                table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                    if(syabdan['responseJSON']['errors']) {
                            if(syabdan['responseJSON']['errors']['npm']){
                                $( '#npmError' ).html(syabdan['responseJSON']['errors']['npm'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['noPermit']){
                                $( '#noPermitError' ).html(syabdan['responseJSON']['errors']['noPermit'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['tglDikeluarkan']){
                                $( '#tglDikeluarkanError' ).html(syabdan['responseJSON']['errors']['tglDikeluarkan'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['tglBerakhir']){
                                $( '#tglBerakhirError' ).html(syabdan['responseJSON']['errors']['tglBerakhir'][0]);
                            }

                          
                    }
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});

$('#npm').on('change',function(){
				var npm = $('#npm').val();
	
					$.ajax({
						type :'GET',
						url  :  "{{ url('admin/permit/get_mahasiswa') }}" + '/' + npm,
						dataType: "json",
						success:function(data)
						{
                            $("#mahasiswaln_id").val(data.id);
						}
					}); 
			
				  
			});


$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});

$(".select2").select2();
</script>
