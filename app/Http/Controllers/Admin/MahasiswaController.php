<?php

namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MahasiswaRequest;
use App\Mahasiswaln;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class MahasiswaController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
 
        if(Sentinel::inRole('user')){
            $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
                        ->where('users.id', Sentinel::getUser()->id)
                        ->first();
            $Mahasiswalns = Mahasiswaln::where('users.id', Sentinel::getUser()->id);
            if($fakultas){
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_fakultas');
            }else{
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_prodi');

            }
            $Mahasiswalns->where('status_mahasiswa','A')->get();
            $tot_mahasiswa_aktif                        =  $Mahasiswalns->count();


            $Mahasiswalns2 = Mahasiswaln::where('users.id', Sentinel::getUser()->id);
            if($fakultas){
                $Mahasiswalns2->join('users','unit_id','mahasiswalns.kode_fakultas');
            }else{
                $Mahasiswalns2->join('users','unit_id','mahasiswalns.kode_prodi');

            }
            $Mahasiswalns2->where('status_mahasiswa','<>','A')->get();
            $tot_mahasiswa_tidak_aktif                  = $Mahasiswalns2->count();


            $Mahasiswalns3 = Mahasiswaln::where('users.id', Sentinel::getUser()->id);
            if($fakultas){
                $Mahasiswalns3->join('users','unit_id','mahasiswalns.kode_fakultas');
            }else{
                $Mahasiswalns3->join('users','unit_id','mahasiswalns.kode_prodi');

            }
            $Mahasiswalns3->where('kelamin', 'L')->get();
            $tot_pria  =  $Mahasiswalns3->count();


           $Mahasiswalns4 = Mahasiswaln::where('users.id', Sentinel::getUser()->id);
                                                            if($fakultas){
                                                                $Mahasiswalns4->join('users','unit_id','mahasiswalns.kode_fakultas');
                                                            }else{
                                                                $Mahasiswalns4->join('users','unit_id','mahasiswalns.kode_prodi');

                                                            }
                                                            
                                                            $Mahasiswalns4->where('kelamin', 'P')
                                                            ->get();
            $tot_wanita = $Mahasiswalns4->count();
        
            if(Sentinel::getUser()->unit_id == 111){
                $tot_mahasiswa_aktif                        = Mahasiswaln::where('status_mahasiswa','A')->count();
                $tot_mahasiswa_tidak_aktif                  = Mahasiswaln::where('status_mahasiswa','<>','A')->count();
                $tot_pria                                   = Mahasiswaln::where('kelamin','L')->count();
                $tot_wanita                                 = Mahasiswaln::where('kelamin','P')->count();
            }
            
        }else{
            $tot_mahasiswa_aktif                        = Mahasiswaln::where('status_mahasiswa','A')->count();
            $tot_mahasiswa_tidak_aktif                  = Mahasiswaln::where('status_mahasiswa','<>','A')->count();
            $tot_pria                                   = Mahasiswaln::where('kelamin','L')->count();
            $tot_wanita                                 = Mahasiswaln::where('kelamin','P')->count();
        }
        
      
        $title = "Data Mahasiswa Asing";
        // Show the page
        return view('admin.mahasiswa.index', compact('title','tot_mahasiswa_aktif','tot_mahasiswa_tidak_aktif','tot_pria','tot_wanita'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));   
        if(Sentinel::inRole('user')){
            $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
                        ->where('users.id', Sentinel::getUser()->id)
                        ->first();

            $Mahasiswalns = Mahasiswaln::select(['mahasiswalns.id as id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'mahasiswalns.email as email','noHp','status_mahasiswa']);
            if($fakultas){
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_fakultas');
            }else{
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_prodi');
            }
            $Mahasiswalns->where('users.id', Sentinel::getUser()->id)
            ->get();

            if(Sentinel::getUser()->unit_id == 111){
                $Mahasiswalns = Mahasiswaln::select(['id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp','status_mahasiswa'])
                ->orderBy('id', 'DESC')->get();
            }
        }else{
            $Mahasiswalns = Mahasiswaln::select(['id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp','status_mahasiswa'])
            ->orderBy('id', 'DESC')->get();
        }             
        
        return DataTables::of($Mahasiswalns)

            ->addColumn('actions',function($Mahasiswalns) {
                $actions = '
                            <a onclick="editForm('.$Mahasiswalns->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Mahasiswa"></i></a>';
                // $actions .= '<a onclick="deleteForm('.$Mahasiswalns->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Mahasiswa"></i></a>';
              
                return $actions;
            })
            ->addColumn('kelamin',function($Mahasiswalns) {
                if($Mahasiswalns->kelamin == 'L'){
                    $sex = "Laki-laki";
                }else{
                    $sex = "Perempuan";

                }
                return $sex;
            })
            ->addColumn('benua',function($Mahasiswalns) {
                if($Mahasiswalns->benua == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->benua ;
                }
                return $data;
            })
            ->addColumn('alamatDomisili',function($Mahasiswalns) {
                if($Mahasiswalns->alamatDomisili == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->alamatDomisili ;
                }
                return $data;
            })
            ->addColumn('penanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->penanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->penanggungjawab ;

                }
                return $data;
            })
            ->addColumn('noHpPenanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->noHpPenanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->noHpPenanggungjawab ;

                }
                return $data;
            })
            ->addColumn('alamatPenanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->alamatPenanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->alamatPenanggungjawab ;

                }
                return $data;
            })
            ->addColumn('noPassport',function($Mahasiswalns) {
                if($Mahasiswalns->noPassport == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->noPassport ;

                }
                return $data;
            })
            ->addColumn('agama',function($Mahasiswalns) {
                if($Mahasiswalns->agama == 1){
                    $data = "Islam";
                }
                elseif($Mahasiswalns->agama == 2){
                    $data =  "Kristen";
                }
                elseif($Mahasiswalns->agama == 3){
                    $data =  "Katolik";
                }
                elseif($Mahasiswalns->agama == 4){
                    $data =  "Hindu";
                }
                elseif($Mahasiswalns->agama == 5){
                    $data =  "Budha";
                }
                return $data;
            })
            ->addColumn('status_mahasiswa',function($Mahasiswalns) {
                if($Mahasiswalns->status_mahasiswa == 'A'){
                    $data = "Aktif";
                }
                elseif($Mahasiswalns->status_mahasiswa == 'C'){
                    $data =  "Cuti";
                }
                elseif($Mahasiswalns->status_mahasiswa == 'D'){
                    $data =  "DropOut";
                }
                elseif($Mahasiswalns->status_mahasiswa == 'L'){
                    $data =  "Lulus";
                }
                elseif($Mahasiswalns->status_mahasiswa == 'N'){
                    $data =  "NonAktif";
                }
                elseif($Mahasiswalns->status_mahasiswa == 'H'){
                    $data =  "Hilang";
                }
                elseif($Mahasiswalns->status_mahasiswa == 'K'){
                    $data =  "?";
                }
                return $data;
            })
            ->addColumn('passportBerakhir',function($Mahasiswalns) {
                if($Mahasiswalns->passportBerakhir == NULL){
                    $data = "Tidak ada data";
                }else{
                    $data = tanggal_indonesia($Mahasiswalns->passportBerakhir);

                }
                return $data;
            })
            ->rawColumns(['actions','agama','status_mahasiswa','passportBerakhir','agama','noPassport','alamatPenanggungjawab','kelamin','benua','alamatDomisili'])
            ->make(true);
    }
    
    public function data_sex($sex)
    {
        $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
        ->where('users.id', Sentinel::getUser()->id)
        ->first();
        DB::statement(DB::raw('set @rownum=0'));        
        if(Sentinel::inRole('user')){
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'mahasiswalns.id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir','mahasiswalns.email','noHp'])
            ->where('users.id', Sentinel::getUser()->id);
            if($fakultas){
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_fakultas');
            }else{
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_prodi');

            }
            $Mahasiswalns->where('kelamin', $sex)->orderBy('mahasiswalns.id', 'DESC')->get();

            if(Sentinel::getUser()->unit_id == 111){
                $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'mahasiswalns.id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp'])->where('kelamin', $sex)->orderBy('id', 'DESC')->get();
            }

        }else{
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'mahasiswalns.id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp'])->where('kelamin', $sex)->orderBy('id', 'DESC')->get();
        }
       
        return DataTables::of($Mahasiswalns)

            ->addColumn('kelamin',function($Mahasiswalns) {
                if($Mahasiswalns->kelamin == 'L'){
                    $sex = "Laki-laki";
                }else{
                    $sex = "Perempuan";

                }
                return $sex;
            })
            ->addColumn('benua',function($Mahasiswalns) {
                if($Mahasiswalns->benua == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->benua ;

                }
                return $data;
            })
            ->addColumn('alamatDomisili',function($Mahasiswalns) {
                if($Mahasiswalns->alamatDomisili == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->alamatDomisili ;

                }
                return $data;
            })
            ->addColumn('penanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->penanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->penanggungjawab ;

                }
                return $data;
            })
            ->addColumn('noHpPenanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->noHpPenanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->noHpPenanggungjawab ;

                }
                return $data;
            })
            ->addColumn('alamatPenanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->alamatPenanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->alamatPenanggungjawab ;

                }
                return $data;
            })
            ->addColumn('noPassport',function($Mahasiswalns) {
                if($Mahasiswalns->noPassport == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->noPassport ;

                }
                return $data;
            })
            ->addColumn('agama',function($Mahasiswalns) {
                if($Mahasiswalns->agama == 1){
                    $data = "Islam";
                }
                elseif($Mahasiswalns->agama == 2){
                    $data =  "Kristen";
                }
                elseif($Mahasiswalns->agama == 3){
                    $data =  "Katolik";
                }
                elseif($Mahasiswalns->agama == 4){
                    $data =  "Hindu";
                }
                elseif($Mahasiswalns->agama == 5){
                    $data =  "Budha";
                }
                return $data;
            })
            
            ->addColumn('passportBerakhir',function($Mahasiswalns) {
                if($Mahasiswalns->passportBerakhir == NULL){
                    $data = "Tidak ada data";
                }else{
                    $data = tanggal_indonesia($Mahasiswalns->passportBerakhir);

                }
                return $data;
            })
            ->rawColumns(['passportBerakhir','kelamin','agama'])
            ->make(true);
    }
    
    public function data_status($status)
    {
        $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
        ->where('users.id', Sentinel::getUser()->id)
        ->first();
        DB::statement(DB::raw('set @rownum=0'));               
        if(Sentinel::inRole('user')){
            
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'mahasiswalns.id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'mahasiswalns.email','noHp'])
            ->where('users.id', Sentinel::getUser()->id);
            if($fakultas){
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_fakultas');
            }else{
                $Mahasiswalns->join('users','unit_id','mahasiswalns.kode_prodi');

            }
            if($status == 'A'){
                $Mahasiswalns->where('status_mahasiswa', $status);
            }
            if($status == 'N'){
                $Mahasiswalns->where('status_mahasiswa', '<>', 'A');
            }
            $Mahasiswalns->orderBy('mahasiswalns.id', 'DESC')->get();

            if(Sentinel::getUser()->unit_id == 111){
                    $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp'])->orderBy('id', 'asc');
                if($status == 'A'){
                    $Mahasiswalns->where('status_mahasiswa', $status);
                }
                if($status == 'N'){
                    $Mahasiswalns->where('status_mahasiswa', '<>', 'A');
                }
                $Mahasiswalns->get();
            }

        }else{
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp'])->orderBy('id', 'asc');
            if($status == 'A'){
                $Mahasiswalns->where('status_mahasiswa', $status);
            }
            if($status == 'N'){
                $Mahasiswalns->where('status_mahasiswa', '<>', 'A');
            }
            $Mahasiswalns->get();
        }
       
        return DataTables::of($Mahasiswalns)

            ->addColumn('kelamin',function($Mahasiswalns) {
                if($Mahasiswalns->kelamin == 'L'){
                    $sex = "Laki-laki";
                }else{
                    $sex = "Perempuan";

                }
                return $sex;
            })
            ->addColumn('benua',function($Mahasiswalns) {
                if($Mahasiswalns->benua == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->benua ;

                }
                return $data;
            })
            ->addColumn('alamatDomisili',function($Mahasiswalns) {
                if($Mahasiswalns->alamatDomisili == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->alamatDomisili ;

                }
                return $data;
            })
            ->addColumn('penanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->penanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->penanggungjawab ;

                }
                return $data;
            })
            ->addColumn('noHpPenanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->noHpPenanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->noHpPenanggungjawab ;

                }
                return $data;
            })
            ->addColumn('alamatPenanggungjawab',function($Mahasiswalns) {
                if($Mahasiswalns->alamatPenanggungjawab == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->alamatPenanggungjawab ;

                }
                return $data;
            })
            ->addColumn('noPassport',function($Mahasiswalns) {
                if($Mahasiswalns->noPassport == NULL){
                    $data = "Tidak ada data ";
                }else{
                    $data = $Mahasiswalns->noPassport ;

                }
                return $data;
            })
            ->addColumn('agama',function($Mahasiswalns) {
                if($Mahasiswalns->agama == 1){
                    $data = "Islam";
                }
                elseif($Mahasiswalns->agama == 2){
                    $data =  "Kristen";
                }
                elseif($Mahasiswalns->agama == 3){
                    $data =  "Katolik";
                }
                elseif($Mahasiswalns->agama == 4){
                    $data =  "Hindu";
                }
                elseif($Mahasiswalns->agama == 5){
                    $data =  "Budha";
                }
                return $data;
            })
            
            ->addColumn('passportBerakhir',function($Mahasiswalns) {
                if($Mahasiswalns->passportBerakhir == NULL){
                    $data = "Tidak ada data";
                }else{
                    $data = tanggal_indonesia($Mahasiswalns->passportBerakhir);

                }
                return $data;
            })
            ->rawColumns(['passportBerakhir','kelamin','agama'])
            ->make(true);
    }
    
    public function data_wisuda()
    {              
        $mahasiswa_wisuda = mahasiswa_asing_wisuda();
        $sumber = $mahasiswa_wisuda;
        $list = json_decode($sumber, true);
        // print_r($list);
        $no=0;
		$data=array();
		for($a=0; $a < count($list); $a++){
			$no++;
			$row=array();
			$row[]=$no;
			$row[]=$list[$a]['npm'];
			$row[]=$list[$a]['nama'];
			$row[]=$list[$a]['nama_fakultas'];
			$row[]=$list[$a]['nama_prodi'];
			$row[]=$list[$a]['kewarganegaraan'];
            $row[]=$list[$a]['tahun_angkatan'];
            if($list[$a]['jenis_kelamin'] == 'L'){
                $sex = 'Laki-laki';
            }else{
                $sex = 'Perempuan';

            }
			$row[]=$sex;
			$row[]=$list[$a]['no_handphone1'];
			$row[]=$list[$a]['email'];
			$row[]=$list[$a]['tempat_lahir'];
			$row[]=$list[$a]['tgl_lahir_ijazah'];
			$row[]=$list[$a]['tahun_wisuda'];
			$row[]=$list[$a]['periode_wisuda'];

			$data[]=$row;
		}
		$output=array("data"=>$data);
		return response()->json($output);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $Mahasiswalns = Mahasiswaln::where('id',$id)->first();    
        return json_encode($Mahasiswalns);
    }
    
    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MahasiswaRequest $request)
    {

        $model = new Mahasiswaln();
        // $model->npm                     = $request->npm;
        // $model->nama                    = $request->nama;
        // $model->fakultas                = $request->fakultas;
        // $model->prodi                   = $request->prodi;
        // $model->asalNegara              = $request->asalNegara;
        // $model->thnMasuk                = $request->thnMasuk;
        // $model->agama                   = $request->agama;
        // $model->kelamin                 = $request->kelamin;
        $model->benua                   = $request->benua;
        $model->alamatDomisili          = $request->alamatDomisili;
        $model->penanggungjawab         = $request->penanggungjawab;
        $model->noHpPenanggungjawab     = $request->noHpPenanggungjawab;
        $model->alamatPenanggungjawab   = $request->alamatPenanggungjawab;
        $model->noPassport              = $request->noPassport;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->passportBerakhir        = date('Y-m-d', strtotime($request->passportBerakhir));
        $model->save();
        
        return json_encode($model);

    }


    public function edit($Mahasiswalns)
    {
        
    }

    public function update(Request $request, $id)
    {
        $model = Mahasiswaln::find($id);
        // $model->npm                     = $request->npm;
        // $model->nama                    = $request->nama;
        // $model->fakultas                = $request->fakultas;
        // $model->prodi                   = $request->prodi;
        // $model->asalNegara              = $request->asalNegara;
        // $model->thnMasuk                = $request->thnMasuk;
        // $model->agama                   = $request->agama;
        // $model->kelamin                 = $request->kelamin;
        $model->benua                   = $request->benua;
        $model->alamatDomisili          = $request->alamatDomisili;
        $model->penanggungjawab         = $request->penanggungjawab;
        $model->noHpPenanggungjawab     = $request->noHpPenanggungjawab;
        $model->alamatPenanggungjawab   = $request->alamatPenanggungjawab;
        $model->noPassport              = $request->noPassport;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->passportBerakhir        = date('Y-m-d', strtotime($request->passportBerakhir));
        $model->update();
        
        return json_encode($model);
    }

    public function destroy($id)
    {
        $Mahasiswalns = Mahasiswaln::find($id);
        if ($Mahasiswalns->delete()) {
            return json_encode($Mahasiswalns);
        } else {
            return Redirect::route('admin/mahasiswa')->withInput()->with('error', trans('mahasiswa/message.error.delete'));
        }
    }
}
