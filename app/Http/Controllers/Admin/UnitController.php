<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UnitRequest;
use App\Unit;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class UnitController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the bidang
        $units = Unit::all();
        // Show the page
        return view('admin.unit.index', compact('units'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $units = Unit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','namaUnit','jPendidikan','created_at'])->orderBy('id', 'DESC')->get();
        return DataTables::of($units)
            ->editColumn('created_at',function(Unit $units) {
                return $units->created_at->diffForHumans();
            })
 
            ->addColumn('actions',function($units) {
                $actions = '<a onclick="showForm('.$units->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view unit"></i></a>
                            <a onclick="editForm('.$units->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update unit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$units->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete unit"></i></a>';
              
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $units = Unit::where('id',$id)->first();    
        return json_encode($units);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(UnitRequest $request)
    {

        $units = new Unit($request->all());

        if ($units->save()) {
            return json_encode($units);
        }
         else {
            return Redirect::route('admin.unit')->withInput()->with('error', 'Gagal menyimpan data');
        }
    }


    public function edit($units)
    {
        
    }

    public function update(UnitRequest $request, $id)
    {

        $units = Unit::find($id);
        if ($units->update($request->all())) {
            return json_encode($units);
        } else {
            return Redirect::route('admin/unit')->withInput()->with('error', trans('unit/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $units = Unit::find($id);
        if ($units->delete()) {
            return json_encode($units);
        } else {
            return Redirect::route('admin/unit')->withInput()->with('error', trans('unit/message.error.delete'));
        }
    }
}
