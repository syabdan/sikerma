<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        orderCellsTop: true,
        fixedHeader: true,
        ajax: '{!! route('admin.kerma.data') !!}',
        columns: [
            { data: 'rownum', searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            { data: 'status', name:'status'},
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', orderable: false, searchable: false},
            { data: 'tglAwal',  orderable: false, searchable: false},
            { data: 'tglAkhir',  orderable: false, searchable: false},
            { data: 'file',  orderable: false, searchable: false},
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10 
                }
                // title: 'Data Mahasiswa Asing'
            }
        ],
        // initComplete: function () {
        //     this.api().columns().every( function () {
        //         var column = this;
        //         var select = $('<select><option value=""></option></select>')
        //             .appendTo( $(column.footer()).empty() )
        //             .on( 'change', function () {
        //                 var val = $.fn.dataTable.util.escapeRegex(
        //                     $(this).val()
        //                 );
 
        //                 column
        //                     .search( val ? '^'+val+'$' : '', true, false )
        //                     .draw();
        //             } );
 
        //         column.data().unique().sort().each( function ( d, j ) {
        //             select.append( '<option value="'+d+'">'+d+'</option>' )
        //         } );
        //     } );
        // }
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

// OPEN MODAL Sex
function StatusModal(status){
    $('#StatusModal').modal('show');
    $('#StatusModal .modal-title').text('Data Kerjasama Aktif');
    if(status == 2){
        $('#StatusModal .modal-title').text('Data Kerjasama Berakhir');
    
    }else if(status == 3){
        $('#StatusModal .modal-title').text('Data Kerjasama Akan Berakhir');
    
    }
    $(function() {
        // Datatable untuk Kelamin
     $('#table_status').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        bDestroy: true,
        ajax: '{!! url('admin/kerma/data_status') !!}'+"/"+status,
        columns: [
            { data: 'rownum', searchable: false},
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', orderable: false, searchable: false},
            { data: 'tglAwal',  orderable: false, searchable: false},
            { data: 'tglAkhir',  orderable: false, searchable: false},
            { data: 'file',  orderable: false, searchable: false},
           
        ],

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open'
            }
        ],
    });
        
    });

}


// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Kerma');
    $('.btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Kerma');
    $('.btnSave').removeClass('hide');
    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('admin/kerma') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

                $('#id').val(data.id);
                $("#prodi_id option[value='" + data.user_id + "']").prop("selected", true).change();
                $("#kategori option[value='" + data.kategori + "']").prop("selected", true).change();
                $("#country_id option[value='" + data.country_id + "']").prop("selected", true).change();
                $("#user_id option[value='" + data.user_id + "']").prop("selected", true).change();
                $("#jenisPartner option[value='" + data.jenis_partner_id + "']").prop("selected", true).change();
                $("#noHp").val(data.noHp);
                $("#tingkat option[value='" + data.tingkat + "']").prop("selected", true).change();
                $("#email").val(data.email);
                $("#namaMitra").val(data.namaMitra);
                $("#tglPengesahan").val(data.tglPengesahan);
                $("#tglAwal").val(data.tglAwal);
                $("#tglAkhir").val(data.tglAkhir);
                
        
                if(data.status_kerma == 1){

                    // document.getElementById('status_kerma').attr("checked", true);
                    document.getElementById('status_kerma').checked = true;
                }else{
                    document.getElementById('status_kerma').attr("checked", false);
                    // document.getElementById('status_kerma').checked = false;

                }
              

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Kerma');
    $('.btnSave').addClass('hide');

    $.ajax({
        url: "{{ url('admin/kerma') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#prodi_id option[value='" + data.user_id + "']").prop("selected", true).change();
                $("#kategori option[value='" + data.kategori + "']").prop("selected", true).change();
                $("#country_id option[value='" + data.country_id + "']").prop("selected", true).change();
                $("#user_id option[value='" + data.user_id + "']").prop("selected", true).change();
                $("#jenisPartner option[value='" + data.jenis_partner_id + "']").prop("selected", true).change();
                $("#noHp").val(data.noHp);
                $("#tingkat option[value='" + data.tingkat + "']").prop("selected", true).change();
                $("#email").val(data.email);
                $("#namaMitra").val(data.namaMitra);
                $("#tglPengesahan").val(data.tglPengesahan);
                $("#tglAwal").val(data.tglAwal);
                $("#tglAkhir").val(data.tglAkhir);
                
        
                if(data.status_kerma == 1){

                    // document.getElementById('status_kerma').attr("checked", true);
                    document.getElementById('status_kerma').checked = true;
                }else{
                    document.getElementById('status_kerma').attr("checked", false);
                    // document.getElementById('status_kerma').checked = false;

                }

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete Kerma');

    $.ajax({
        url: "{{ url('admin/kerma') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#pesan_hapus").text(data.namaMitra);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();

            url = "{{ url('admin/kerma/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.kerma.store') }}";
            } 
            else {
            url = "{{ url('admin/kerma') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                            $('.btnSave').attr('disabled', true);
                            $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    $('#addModal').modal('hide');
                    table.ajax.reload();
                
                },
                error : function(syabdan){
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                    if(syabdan['responseJSON']['errors']) {
                            if(syabdan['responseJSON']['errors']['kategori']){
                                $( '#kategoriError' ).html(syabdan['responseJSON']['errors']['kategori'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['email']){
                                $( '#emailError' ).html(syabdan['responseJSON']['errors']['email'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['tglPengesahan']){
                                $( '#tglPengesahanError' ).html(syabdan['responseJSON']['errors']['tglPengesahan'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['tglAwal']){
                                $( '#tglAwalError' ).html(syabdan['responseJSON']['errors']['tglAwal'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['tglAkhir']){
                                $( '#tglAkhirError' ).html(syabdan['responseJSON']['errors']['tglAkhir'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['dokumenKerma']){
                                $( '#dokumenKermaError' ).html(syabdan['responseJSON']['errors']['dokumenKerma'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['country_id']){
                                $( '#country_idError' ).html(syabdan['responseJSON']['errors']['country_id'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['namaMitra']){
                                $( '#namaMitraError' ).html(syabdan['responseJSON']['errors']['namaMitra'][0]);
                            }
                            
                            if(syabdan['responseJSON']['errors']['tingkat']){
                                $( '#tingkatError' ).html(syabdan['responseJSON']['errors']['tingkat'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['jenisPartner']){
                                $( '#jenisPartnerError' ).html(syabdan['responseJSON']['errors']['jenisPartner'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['status_permit']){
                                $( '#status_permitError' ).html(syabdan['responseJSON']['errors']['status_permit'][0]);
                            }
                
                    }
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});

$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});

$(".select2").select2();

</script>
