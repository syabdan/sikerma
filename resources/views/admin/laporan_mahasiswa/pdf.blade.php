<!DOCTYPE html>
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Laporan Mahasiswa Asing Universitas Islam Riau</title>
    <style type="text/css" >
      body, td, th, input, select {
       font: 8pt Verdana, Arial, sans-serif;
      }
      td, th {
       padding: 1px;
       vertical-align: bottom;
       text-align: left;
      }
      td {
       vertical-align: top;
      }
      th {
       font-weight: bold;
       vertical-align: bottom;
       border-bottom: 1pt solid silver;
       background-color: #9ACD32;
       text-align: center;
       padding: 5px;

      }
      h1,h2{
        margin: 0;
        padding: 0;
      }
      img{
        height: 40;
      }
    }
    .tanda_tangan{
      float:left;
      text-align:center;
      width:50%;
      margin-top: 50px;
    }
   </style>
  </head>
  <table cellPadding=0>
    <tr>
      <td width="10"><img src="{{ asset('assets/img/uir.png') }}" alt="" width="80" ></td>
      <td>
        <h1>Kantor Urusan Internasional & Kerjasama</h1>
        <h2>Universitas Islam Riau</h2>
    </td>
    </tr>
  </table>

  <br><br>
  <body>
    <table id=titleTable cellSpacing=0 cellPadding=0 width="100%" border=1>
    <thead>
    <tr>
    <th>No</th>
    <th>NPM</th>
    <th>Nama</th>
    <th>Fakultas</th>
    <th>Prodi</th>
    <th>Asal Negara</th>
    <th>Tahun Masuk</th>
    <th>Agama</th>
    <th>Kelamin</th>
    <th>Benua</th>
    <th>Alamat Domisili</th>
    <th>Penganggung Jawab</th>
    <th>No.HP PJ</th>
    <!-- <th>Alamat PJ</th> -->
    <th>No. Passport</th>
    <th>Passport Berakhir</th>
    <!-- <th>Periode Wisuda</th> -->
    </tr>
    </thead>
    <tbody>
      <?php $no=1;?>
      @foreach($Mahasiswalns as $mahasiswa)
        <tr>
            <td>{{$no}}</td>
            <td>{{$mahasiswa->npm}}</td>
            <td>{{$mahasiswa->nama}}</td>
            <td>{{$mahasiswa->fakultas}}</td>
            <td>{{$mahasiswa->prodi}}</td>
            <td>{{$mahasiswa->asalNegara}}</td>
            <td>{{$mahasiswa->thnMasuk}}</td>
            <td>{{$mahasiswa->agama}}</td>
            <td>{{$mahasiswa->kelamin}}</td>
            <td>{{$mahasiswa->benua}}</td>
            <td>{{$mahasiswa->alamatDomisili}}</td>
            <td>{{$mahasiswa->penanggungjawab}}</td>
            <td>{{$mahasiswa->noHpPenanggungjawab}}</td>
            <td>{{$mahasiswa->noPassport}}</td>
            <td>@if($mahasiswa->passportBerakhir)
                tanggal_indonesia($mahasiswa->passportBerakhir)
                @else 
              
                @endif</td>
        </tr>
        <?php $no++;?>
      @endforeach
    </tbody>
    </table>

      <!-- <h5>Tanggal Cetak: {!! date('d/m/Y') !!}</h5> -->

    <br><br>
    <div class="tanda_tangan" style="float:right">
    <div>Pekanbaru, {{ tanggal_indonesia(date('Y-m-d'),false) }}</div>


        <h5>KEPALA KUI & Kerjasama</h5>


    <br><br>
    <div ><span style="text-transform:uppercase;text-decoration:underline;font-weight:bold">{{$kepala_kantor}}</span></div>

</div>


  </body>

</html>
