<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubbidangRequest;
use App\Bidang;
use App\SubBidang;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class SubbidangController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the bidang
        $bidangs = Bidang::all();
        // Show the page
        return view('admin.subbidang.index', compact('bidangs'));
    }

    public function data()
    {
        // $subbidangs = SubBidang::get(['id','bidang_id', 'namaSubBidang','created_at']);
        DB::statement(DB::raw('set @rownum=0'));                
        $subbidangs = SubBidang::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','bidang_id', 'namaSubBidang','created_at'])->orderBy('id', 'DESC')->get();
        return DataTables::of($subbidangs)
            ->editColumn('created_at',function(SubBidang $subbidangs) {
                return $subbidangs->created_at->diffForHumans();
            })
 
            ->addColumn('actions',function($subbidangs) {
                $actions = '<a onclick="showForm('.$subbidangs->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view subbidang"></i></a>
                            <a onclick="editForm('.$subbidangs->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update subbidang"></i></a>';
                $actions .= '<a onclick="deleteForm('.$subbidangs->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete subbidang"></i></a>';
              
                return $actions;
            })
            ->addColumn('bidang',function($subbidangs) {          
                return $subbidangs->bidang->namaBidang;
            })
            ->rawColumns(['actions','bidang'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $subbidangs = SubBidang::where('id',$id)->first();    
        return json_encode($subbidangs);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SubBidangRequest $request)
    {

        $subbidangs = new SubBidang($request->all());

        if ($subbidangs->save()) {
            return json_encode($subbidangs);
        }
         else {
            return Redirect::route('admin.subbidang')->withInput()->with('error', 'Gagal menyimpan data');
        }
    }


    public function edit($subbidangs)
    {
        
    }

    public function update(SubbidangRequest $request, $id)
    {

        $subbidangs = SubBidang::find($id);
        if ($subbidangs->update($request->all())) {
            return json_encode($subbidangs);
        } else {
            return Redirect::route('admin/subbidang')->withInput()->with('error', trans('subbidang/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $subbidangs = SubBidang::find($id);
        if ($subbidangs->delete()) {
            return json_encode($subbidangs);
        } else {
            return Redirect::route('admin/subbidang')->withInput()->with('error', trans('subbidang/message.error.delete'));
        }
    }
}
