<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Permit;

class nonAktifkanPermit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nonaktif:permit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Aktifkan Status Permit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses non aktifkan Permit.');

        $data = Permit::select('id')->where('tglBerakhir','<',date('Y-m-d'))->where('status_permit',1)->get();

        foreach($data as $rows){
            Permit::find($rows->id)->update(['status_permit' => 0]);
        }
        $this->info('Selesai...');
    }
}
