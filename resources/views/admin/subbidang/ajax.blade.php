<script>
var table;
$(function() {
    table = $('#table').DataTable({ 
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.subbidang.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'bidang', name: 'bidang' },
            { data: 'namaSubBidang', name: 'namaSubBidang' },
            { data: 'created_at', name:'created_at'},
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

    
// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Sub Bidang');
    $('.btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "editSubSubBidang";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Sub Bidang');
    $('.btnSave').removeClass('hide');
    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('admin/subbidang') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#idSubBidang').val(data.id);
                $("#bidang_id option[value='" + data.bidang_id + "']").prop("selected", true).change();
                $("#namaSubBidang").val(data.namaSubBidang);

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Sub Bidang');
    $('.btnSave').addClass('hide');

    $.ajax({
        url: "{{ url('admin/subbidang') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#idSubBidang').val(data.id);
                $("#bidang_id option[value='" + data.bidang_id + "']").prop("selected", true).change();
                $("#namaSubBidang").val(data.namaSubBidang).attr('readonly', true);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete Sub Bidang');

    $.ajax({
        url: "{{ url('admin/subbidang') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#idSubBidang').val(data.id);
                $("#pesan_hapus").text(data.namaSubBidang);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#idSubBidang').val();

            url = "{{ url('admin/subbidang/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#idSubBidang').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.subbidang.store') }}";
            } 
            else {
            url = "{{ url('admin/subbidang') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                        $('.btnSave').attr('disabled', true);
                        $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    $('#addModal').modal('hide');
                    table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                    if(syabdan['responseJSON']['errors']) {
                            if(syabdan['responseJSON']['errors']['namaSubBidang']){
                                $( '#namaSubBidangError' ).html(syabdan['responseJSON']['errors']['namaSubBidang'][0]);
                            }

                          
                    }
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});



</script>
