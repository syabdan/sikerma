<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.tamu.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'nama', name: 'nama' },
            { data: 'negara', name: 'negara' },
            { data: 'unit', name: 'unit' },
            { data: 'tujuan', name: 'tujuan' },
            { data: 'file', name: 'file' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

    
// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Tamu');
    $('#btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

    $("#unit_id option[value='" + {{Sentinel::getUser()->unit_id}} + "']").prop("selected", true).change();


}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "editTamu";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Tamu');
    $('#btnSave').removeClass('hide');
    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('admin/tamu') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#country_id option[value='" + data.country_id + "']").prop("selected", true).change();
                $("#unit_id option[value='" + data.unit_id + "']").prop("selected", true).change();
                $("#tglKedatangan").val(data.tglKedatangan);
                $("#tujuan").val(data.tujuan);
                $("#nama").val(data.nama);
                $("#noHp").val(data.noHp);
                $("#email").val(data.email);
                $("#keterangan").val(data.keterangan);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Tamu');

    $.ajax({
        url: "{{ url('admin/tamu') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#country_id option[value='" + data.country_id + "']").prop("selected", true).change();
                $("#unit_id option[value='" + data.unit_id + "']").prop("selected", true).change();
                $("#tglKedatangan").val(data.tglKedatangan);
                $("#tujuan").val(data.tujuan);
                $("#nama").val(data.nama);
                $("#noHp").val(data.noHp);
                $("#email").val(data.email);
                $("#keterangan").val(data.keterangan);
                $('#btnSave').addClass('hide');
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete Tamu');

    $.ajax({
        url: "{{ url('admin/tamu') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#pesan_hapus").text(data.id);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();

            url = "{{ url('admin/tamu/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.tamu.store') }}";
            } 
            else {
            url = "{{ url('admin/tamu') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                        $('.btnSave').attr('disabled', true);
                        $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    $('#addModal').modal('hide');
                    table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                    if(syabdan['responseJSON']['errors']) {
                            if(syabdan['responseJSON']['errors']['nama']){
                                $( '#namaError' ).html(syabdan['responseJSON']['errors']['nama'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['email']){
                                $( '#emailError' ).html(syabdan['responseJSON']['errors']['email'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['noHp']){
                                $( '#noHpError' ).html(syabdan['responseJSON']['errors']['noHp'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['email']){
                                $( '#emailError' ).html(syabdan['responseJSON']['errors']['email'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['country_id']){
                                $( '#country_idError' ).html(syabdan['responseJSON']['errors']['country_id'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['dokumentasiTamu']){
                                $( '#dokumenTamuError' ).html(syabdan['responseJSON']['errors']['dokumentasiTamu'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['keterangan']){
                                $( '#keteranganError' ).html(syabdan['responseJSON']['errors']['keterangan'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['tglKedatangan']){
                                $( '#tglKedatanganError' ).html(syabdan['responseJSON']['errors']['tglKedatangan'][0]);
                            }
                            
                            if(syabdan['responseJSON']['errors']['tujuan']){
                                $( '#tujuanError' ).html(syabdan['responseJSON']['errors']['tujuan'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['unit_id']){
                                $( '#unit_idError' ).html(syabdan['responseJSON']['errors']['unit_id'][0]);
                            }
                           
                
                    }
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});

$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});

$(".select2").select2();

</script>