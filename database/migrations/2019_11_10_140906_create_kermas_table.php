<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKermasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kermas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('no_kerma')->nullable();
            $table->string('kategori')->comment("1=MoU, 2=MoA");
            $table->string('email')->unique();
            $table->string('noHp');
            $table->date('tglPengesahan');
            $table->date('tglAwal');
            $table->date('tglAkhir');
            // $table->integer('jenis_kerma_id')->unsigned();
            // $table->foreign('jenis_kerma_id')->references('id')->on('jenis_kermas');
            $table->enum('tingkat', ['Internasional', 'Nasional', 'Lokal'])->comment("1=Internasional, 2=Nasional, 3=Lokal");
            $table->string('dokumenKerma')->comment("Upload PDF Files");
            $table->integer('status_kerma')->comment("1=Aktif, 2=Non Aktif, 3=Lain2");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kermas');
    }
}
