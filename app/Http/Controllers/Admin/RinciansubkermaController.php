<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RinciansubkermaRequest;
use App\RincianSubKerma;
use App\Kerma;
use App\RincianKerma;
use App\SubBidang;
use App\Unit;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class RincianSubkermaController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the bidang
        $rinciansubkermas   =   Rinciansubkerma::all();
        $kermas             =   Kerma::all();
        $rincian_kermas     =   RincianKerma::all();
        $subbidangs         =   SubBidang::all();
        // $unit               =  Unit::select('id', \DB::raw("CONCAT(units.namaUnit,' - ',units.jPendidikan) as nama_unit"))->pluck("nama_unit", "id");

        $units              =   Unit::select('id','namaUnit','jPendidikan')->get();
        $title              =   "Rincian Kegiatan Kerjasama";

        // Show the page    
        return view('admin.rinciansubkerma.index', compact('rinciansubkermas', 'title','subbidangs','kermas','rincian_kermas', 'units'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $rinciansubkermas = RincianSubKerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','rincian_kerma_id','sub_bidang_id', 'tglKegiatan', 'manfaat','dokumenKegiatan', 'bentuk_kegiatan','unit_id','created_at']);
        if(Sentinel::inRole('user')){
            $rinciansubkermas->whereIn('rincian_sub_kermas.unit_id', ['111', Sentinel::getUser()->unit_id ]);
        }
        $rinciansubkermas->orderBy('id', 'DESC');
        return DataTables::of($rinciansubkermas->get())
            ->editColumn('created_at',function(RincianSubKerma $rinciansubkermas) {
                return $rinciansubkermas->created_at->diffForHumans();
            })
 
            ->addColumn('actions',function($rinciansubkermas) {
                $actions = '<a onclick="showForm('.$rinciansubkermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view bidang"></i></a>
                            <a onclick="editForm('.$rinciansubkermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update bidang"></i></a>';
                $actions .= '<a onclick="deleteForm('.$rinciansubkermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete bidang"></i></a>';
              
                return $actions;
            })
            ->addColumn('namaSubBidang',function($rinciansubkermas) {
                return $rinciansubkermas->subBidang->namaSubBidang;
            })
            ->addColumn('namaMitra',function($rinciansubkermas) {
                return $rinciansubkermas->rinciankerma->kerma->namaMitra;
            })
            ->addColumn('unit',function($rinciansubkermas) {
                return $rinciansubkermas->unit->namaUnit;
            })
            ->addColumn('file',function($rinciansubkermas) {
                if($rinciansubkermas->dokumenKegiatan == ''){
                    $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
                }else{
                    $file = '<a href='. url('admin/rincian-subkerjasama/ambilFile/'.$rinciansubkermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
                }
                return $file;
            })
            ->addColumn('tglKegiatan',function($data) {
                return tanggal_indonesia($data->tglKegiatan);
            })
            ->rawColumns(['actions','namaSubBidang','file','namaMitra','unit'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ambilFile($id)
    {
        $model = RincianSubKerma::where('id',$id)->first(); 
        return response()->file(storage_path('/uploads/subkerma/'.$model->dokumenKegiatan));   
    }

    public function show($id)
    {
        $model = RincianSubKerma::where('id',$id)->first();    
        return json_encode($model);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RinciansubkermaRequest $request)
    {

        $model      =   new RincianSubKerma();
        //upload dokumen Permit
        if ($file = $request->file('dokumenSubKerma')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/subkerma/'))
            {
                File::makeDirectory( storage_path() . '/uploads/subkerma/');
            }
            $destinationPath = storage_path() . '/uploads/subkerma/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenKegiatan = $gambarName;
        }
        
        $model->manfaat                = $request->manfaat;
        $model->rincian_kerma_id       = $request->idSub;
        $model->sub_bidang_id          = $request->sub_bidang_id;
        $model->bentuk_kegiatan        = $request->bentuk_kegiatan;
        $model->unit_id                = Sentinel::getUser()->unit_id;
        $model->tglKegiatan            = date('Y-m-d', strtotime($request->tglKegiatan));
        $model->save();

        return json_encode($model);
    }


    public function edit($rinciankermas)
    {
        
    }

    public function update(Request $request, $id)
    {
        $model      =   RincianSubKerma::find($id);
        //upload dokumen Permit
        if ($file = $request->file('dokumenSubKerma')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/subkerma/'))
            {
                File::makeDirectory( storage_path() . '/uploads/subkerma/');
            }
            $destinationPath = storage_path() . '/uploads/subkerma/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenKegiatan = $gambarName;
        }
        
  

        $model->manfaat                = $request->manfaat;
        $model->rincian_kerma_id       = $request->rincian_kerma_id;
        $model->bentuk_kegiatan        = $request->bentuk_kegiatan;
        $model->unit_id                = $request->unit_id;
        $model->tglKegiatan            = date('Y-m-d', strtotime($request->tglKegiatan));
        $model->save();

        return json_encode($model);
    }

    public function destroy($id)
    {
        $model = RincianSubKerma::find($id);
        if ($model->delete()) {
            return json_encode($model);
        } else {
            return Redirect::route('admin/rinciansubkerma')->withInput()->with('error', trans('rinciansubkerma/message.error.delete'));
        }
    }
}
