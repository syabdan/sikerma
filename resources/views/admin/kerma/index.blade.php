@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Kerjasama List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/Buttons/css/buttons.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/pages/advbuttons.css') }}" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Kerjasama</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Kerjasama</a></li>
        <li class="active">Kerjasama List</li>
    </ol>
</section>


<div class="panel-body">
    <div class="row">
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a href="#" class="list-group-item visitor" style="cursor: none;">
                    <p class="pull-right">
                        <i class="fa fa-university f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_kerma}}</h4>
                    <p class="list-group-item-text">Semua Kerjasama</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a onclick="StatusModal(1)" class="list-group-item tumblr" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-check f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_kerma_aktif}}</h4>
                    <p class="list-group-item-text">Kerjasama Aktif</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                
                <a onclick="StatusModal(2)" class="list-group-item google-plus" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-times-circle f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_kerma_habis}}</h4>
                    <p class="list-group-item-text">Kerjasama Habis</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                
                <a onclick="StatusModal(3)" class="list-group-item vimeo" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-hourglass-half f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_kerma_akan_habis}}</h4>
                    <p class="list-group-item-text" style="font-size:9pt">Kerjasama Akan Berakhir</p>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Data Kerjasama 
                </h4>
                <div class="pull-right">
                    <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>Actions</th>
                            <th width="10%">Status</th>
                            <th>Kategori</th>
                            <th>Pihak UIR</th>
                            <th>Nama Mitra</th>
                            <th>Negara</th>
                            <th>Jenis Partner</th>
                            <th>Tingkat</th>
                            <th>Tgl Pengesahan</th>
                            <th>Tgl Awal</th>
                            <th>Tgl Akhir</th>
                            <th>Dokumen</th>
                        </tr>
                    </thead>
                    {{-- <tfoot>
                        <tr class="filters">
                            <th>No</th>
                            <th>Actions</th>
                            <th width="10%">Status</th>
                            <th>Kategori</th>
                            <th>Pihak UIR</th>
                            <th>Nama Mitra</th>
                            <th>Negara</th>
                            <th>Jenis Partner</th>
                            <th>Tingkat</th>
                            <th>Tgl Pengesahan</th>
                            <th>Tgl Awal</th>
                            <th>Tgl Akhir</th>
                            <th>Dokumen</th>
                        </tr>
                    </tfoot> --}}
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>


<div class="modal fade" id="StatusModal"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog" style="width: 80%">
    	<div class="modal-content modal-lg" style="width: 100%">
        <div class="modal-header bg-blue">
        <br>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
     
            <div class="modal-body">
                <div class="row">
                    <div class="panel-body">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-bordered " id="table_status">
                                <thead>
                                    <tr class="filters">
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Pihak UIR</th>
                                    <th>Nama Mitra</th>
                                    <th>Negara</th>
                                    <th>Jenis Partner</th>
                                    <th>Tingkat</th>
                                    <th>Tgl Pengesahan</th>
                                    <th>Tgl Awal</th>
                                    <th>Tgl Akhir</th>
                                    <th>Dokumen</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
				</div>

        </div>
    </div>
</div>


<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus data ini ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" id="btnSave"><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>
        </div>
  </div>
</div>
<div class="modal fade" id="addModal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <br>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />
        <input type="hidden" id="id" name="id" class="form-control refresh">

            <div class="modal-body">  
                <!-- <div class="form-group">
                    <label for="noKerma" class="col-sm-3 control-label">No Kerma *</label>
                    <div class="col-sm-9">
                        <input type="text" id="noKerma" name="noKerma" class="form-control refresh" placeholder="No Kerma" value="{!! old('noKerma') !!}">
                        <span style="color: red;"><i><p id="noKermaError"></p></i></span>
                    </div>
                    {!! $errors->first('No Kerma', '<span class="help-block">:message</span>') !!}
                </div> -->
                @if(count($units) > 0)
                <div class="form-group {{ $errors->first('kategori', 'has-error') }}">
                    <label for="kategori" class="col-sm-3 control-label">Prodi *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Prodi..." name="prodi_id" id="prodi_id" style="width: 100%;">
                        <option value="">Select Prodi ... </option>
                        @foreach($units as $prodi)
                            <option value="{{$prodi->id}}"
                                    @if(old('prodi_id') === '{{$prodi->id}}' ) selected="selected" @endif >{{$prodi->namaUnit}}
                            </option>
                        @endforeach
                         
                        </select>
                        <span style="color: red;"><i><p>Jika Prodi tidak dipilih, otomatis data kerjasama tersimpan milik Fakultas</p></i></span>
                        <span style="color: red;"><i><p id="prodi_idError"></p></i></span>
                    </div>
                    <span class="help-block">{{ $errors->first('prodi_idError', ':message') }}</span>
                </div>
                @endif
                <div class="form-group {{ $errors->first('kategori', 'has-error') }}">
                    <label for="kategori" class="col-sm-3 control-label">Kategori *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Kategori..." name="kategori" id="kategori" style="width: 100%;" required>
                        <option value="">Select kategori ... </option>
                            <option value="MoU"
                                    @if(old('MoU') === 'MoU' ) selected="selected" @endif >Memorandum of Understanding
                            </option>
                            <option value="MoA"
                                    @if(old('MoA') === 'MoA' ) selected="selected" @endif >Memorandum of Agreement
                            </option>
                        </select>
                        <span style="color: red;"><i><p id="kategoriError"></p></i></span>
                    </div>
                    <span class="help-block">{{ $errors->first('kategori', ':message') }}</span>
                </div>

                <div class="form-group">
                    <label for="namaMitra" class="col-sm-3 control-label">Nama Mitra *</label>
                    <div class="col-sm-9">
                        <input type="text" id="namaMitra" name="namaMitra" class="form-control refresh" placeholder="Nama Mitra" value="{!! old('namaMitra') !!}">
                        <span style="color: red;"><i><p id="namaMitraError"></p></i></span>
                    </div>
                    {!! $errors->first('namaMitra', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group {{ $errors->first('tingkat', 'has-error') }}">
                    <label for="tingkat" class="col-sm-3 control-label">Tingkat *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Tingkat..." name="tingkat" id="tingkat" style="width: 100%;" required>
                        <option value="">Select Tingkat ... </option>
                            <option value="Internasional"
                                    @if(old('Internasional') === 'Internasional' ) selected="selected" @endif >Internasional
                            </option>
                            <option value="Nasional"
                                    @if(old('Nasional') === 'Nasional' ) selected="selected" @endif >Nasional
                            </option>
                            <option value="Lokal"
                                    @if(old('Lokal') === 'Lokal' ) selected="selected" @endif >Lokal
                            </option>
                        </select>
                        <span style="color: red;"><i><p id="tingkatError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('tingkat', ':message') }}</span>
                </div>
                
                
                <div class="form-group {{ $errors->first('jenisPartner', 'has-error') }}">
                    <label for="jenisPartner" class="col-sm-3 control-label">Jenis Partner *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Jenis Partner..." name="jenisPartner" id="jenisPartner" style="width: 100%;" required>
                        <option value="">Select Jenis Partner ... </option>
                           @foreach($jenisPartners as $jenisPartner)
                            <option value="{{ $jenisPartner->id }}"
                                    @if(old('jenisPartner') === '{{ $jenisPartner->id }}' ) selected="selected" @endif >{{ $jenisPartner->namaJenisPartner}}
                            </option>
                           @endforeach

                        </select>
                        <span style="color: red;"><i><p id="jenisPartnerError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('jenisPartner', ':message') }}</span>
                </div>

                <div class="form-group">
                    <label for="noHp" class="col-sm-3 control-label">No. Hp *</label>
                    <div class="col-sm-9">
                        <input type="text" id="noHp" name="noHp" class="form-control refresh" placeholder="No HP" value="{!! old('noHp') !!}">
                        <span style="color: red;"><i><p id="noHpError"></p></i></span>
                    </div>
                    {!! $errors->first('noHp', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email </label>
                    <div class="col-sm-9">
                        <input type="text" id="email" name="email" class="form-control refresh" placeholder="Email" value="{!! old('email') !!}">
                        <span style="color: red;"><i><p id="emailError"></p></i></span>
                    </div>
                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="tglPengesahan" class="col-sm-3 control-label">Tanggal Pengesahan *</label>
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker" id="tglPengesahan" name="tglPengesahan" class="form-control refresh" value="{!! old('tglPengesahan') !!}" autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="tglPengesahanError"></p></i></span>
                    </div>
                    {!! $errors->first('tglDikeluarkan', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="tglAwal" class="col-sm-3 control-label">Tanggal Awal *</label>
                    
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker"  id="tglAwal" name="tglAwal" class="form-control refresh" value="{!! old('tglAwal') !!}" autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="tglAwalError"></p></i></span>
                    </div>
                    {!! $errors->first('tglAwal', '<span class="help-block">:message</span>') !!}
                </div>      
                
                <div class="form-group">
                    <label for="tglAkhir" class="col-sm-3 control-label">Tanggal Berakhir *</label>
                    
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker"  id="tglAkhir" name="tglAkhir" class="form-control refresh" value="{!! old('tglAkhir') !!}" autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="tglAkhirError"></p></i></span>
                    </div>
                    {!! $errors->first('tglAkhir', '<span class="help-block">:message</span>') !!}
                </div>      

                <div class="form-group {{ $errors->first('dokumen', 'has-error') }}">
                    <label for="dokumen" class="col-sm-3 control-label">Dokumen (*.pdf  Max 2000 KB) *</label>
                        <div class="col-sm-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 500px; max-height: 200px;"></div>
                            <div>
                            <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select File</span>
                            <span class="fileinput-exists">Change</span>
                            <input id="dokumenKerma" name="dokumenKerma" type="file" class="form-control" accept="application/pdf" required/></span>
                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                        <span style="color: red;"><i><p id="dokumenKermaError"></p></i></span>
                        </div>
                        <span class="help-block">{{ $errors->first('dokumenKermaError', ':message') }}</span>
                    </div>
                </div>  

               
                <div class="form-group {{ $errors->first('country_id', 'has-error') }}">
                    <label for="country_id" class="col-sm-3 control-label">Negara *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Country..." name="country_id" id="country_id" style="width: 100%;" required>
                        <option value="">Select Country ... </option>
                           @foreach($countries as $country)
                            <option value="{{ $country->id }}"
                                    @if(old('country_id') === '{{ $country->id }}' ) selected="selected" @endif >{{ $country->name}}
                            </option>
                           @endforeach

                        </select>
                        <span style="color: red;"><i><p id="country_idError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('country_id', ':message') }}</span>
                </div>

                <div class="form-group {{ $errors->first('status_kerma', 'has-error') }}">
                    <label for="status_kerma" class="col-sm-3 control-label">Status Kerma *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Status Kerma..." name="status_kerma" id="status_kerma" style="width: 100%;" required>
                     
                            <option value="1"
                                    @if(old('1') === '1' ) selected="selected" @endif >Aktif
                            </option>
                            <option value="0"
                                    @if(old('0') === '0' ) selected="selected" @endif >Tidak Aktif
                            </option>
                            <!-- <option value="2"
                                    @if(old('2') === '2' ) selected="selected" @endif >Lain-lain
                            </option> -->
                        </select>
                        <span style="color: red;"><i><p id="status_kermaError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('status_kerma', ':message') }}</span>
                </div>

                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
					<button type="submit" class="btn btn-primary pull-right btnSave" id="btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
				</div>
            </div>
        </form>

        </div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
@include('admin.kerma.ajax')



@stop
