<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Kerma;

class nonAktifkanKerma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nonaktif:kerma';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Aktifkan Status Kerma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses non aktifkan Kerjasama.');

        $data = Kerma::select('id')->where('tglAkhir','<',date('Y-m-d'))->whereIn('status_kerma', [1,3])->get();

        foreach($data as $rows){
            Kerma::find($rows->id)->update(['status_kerma' => 2]);
        }
        $this->info('Selesai...');
    }
}
