<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RincianKerma extends Model
{
    protected $fillable = ['id','kerma_id','bidang_id', 'created_at'];

    protected $guarded = ['id'];

    public function bidang()
    {
        return $this->belongsTo('App\Bidang');
    } 
    
    public function kerma()
    {
        return $this->belongsTo('App\Kerma');
    } 
}
