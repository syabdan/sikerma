<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Permit;
use App\Mahasiswaln;
use App\Mail\NotifikasiPermit; 
use Illuminate\Support\Facades\Mail;


class sendNotifPermit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:NotifPermit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kirim Email Notifikasi Permit ke Mahasiswa Asing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses kirim notif ke email dan sms.');
        $start = microtime(true);

        // Send Notif Email
        $notif1 = Permit::whereMonth('tglBerakhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))->whereNull('status_notif')->get();
        
        if($notif1){
            foreach($notif1 as $data){
                // Kirim email
                Mail::to($data->mahasiswaln->email)
                ->send(new NotifikasiPermit($data));

                //Update Notif Status
                Permit::find($data->id)->update(['status_notif' => 1]);
            }
        }else{
            return 'Link has expired';
        }

        $notif2 = Permit::whereMonth('tglBerakhir', date('m', strtotime('+1 month', strtotime(date('Y-m-d')))))->whereNull('status_notif_2')->get();
        if($notif2){

            foreach($notif2 as $data){
                // Kirim email
                Mail::to($data->mahasiswaln->email)
                ->send(new NotifikasiPermit($data));
                //Update Notif Status
                Permit::find($data->id)->update(['status_notif_2' => 1]);
            }
            
        }else{
            return 'Link has expired';

        }
        // End Send Notif Email
        
        
        // Send Notif SMS
        $isi_sms = "The validity period of your permit / residence permit will soon end, please extend your Permit period, IO office UIR.";

        $notif1 = Permit::whereMonth('tglBerakhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))->whereNull('status_sms')->get();
        
        if($notif1){

            foreach($notif1 as $data){
                //Update Notif Status
                Permit::find($data->id)->update(['status_sms' => 1]);

                $no_hp      = $data->mahasiswaln->noHp;
                // Kirim SMS
                $client = new \GuzzleHttp\Client();
                $res = $client->request('GET', 'http://sms.webuir.com/sikerma.php?user=uir&pass=uirunggul2020&no='.$no_hp.'&isi='.$isi_sms);
                echo $res->getStatusCode();

            }
            
        }else{
            return 'Tidak ada data yang akan di kirim SMS';
        }

        $notif2 = Permit::whereMonth('tglBerakhir', date('m', strtotime('+1 month', strtotime(date('Y-m-d')))))->whereNull('status_sms_2')->get();
        if($notif2){

            foreach($notif2 as $data){
                $no_hp      = $data->mahasiswaln->noHp;

                // Kirim SMS
                $client = new \GuzzleHttp\Client();
                $res = $client->request('GET', 'http://sms.webuir.com/sikerma.php?user=uir&pass=uirunggul2020&no='.$no_hp.'&isi='.$isi_sms);
                echo $res->getStatusCode();

                 //Update Notif Status
                 Permit::find($data->id)->update(['status_sms_2' => 1]);
            }
            
        }else{
            return 'Tidak ada data yang akan di kirim SMS';
        }
        // End Send Notif SMS

        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Selesai... '.$time_elapsed_secs);
    }
}
