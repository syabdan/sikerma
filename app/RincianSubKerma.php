<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RincianSubKerma extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['id','rincian_kerma_id','sub_bidang_id','tglKegiatan', 'manfaat','dokumenKegiatan','bentuk_kegiatan', 'unit_id', 'created_at'];

    protected $guarded = ['id'];

    public function subBidang()
    {
        return $this->belongsTo('App\SubBidang');
    }
    
    public function rincianKerma()
    {
        return $this->belongsTo('App\RincianKerma');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    } 
}
