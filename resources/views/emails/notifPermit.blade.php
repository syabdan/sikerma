

@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
        Administrator Kantor Urusan Internasional dan Kerjasama (International Office) UIR
        @endcomponent
    @endslot

    {{-- Body --}}
# Hello  {!! $data->mahasiswaln->nama !!},<br>

The validity period of your permit / residence permit will soon end, 
please extend your Permit period. If You have read this information, please click on the following link bellow.
@component('mail::button', ['url' =>  URL::route('update-permit-confirm', [$data->npm, $data->noPermit]) ])
Send Confirmation
@endcomponent


Thanks, KUIK UIR

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
           &copy; {{ date('Y') }} All Copy right received
        @endcomponent
    @endslot
@endcomponent
