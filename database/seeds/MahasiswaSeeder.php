<?php

use Illuminate\Database\Seeder;
use App\Mahasiswaln;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mahasiswaln::truncate();

        $mahasiswa_asing_all = mahasiswa_asing_all();
        $sumber = $mahasiswa_asing_all;
        $list = json_decode($sumber, true);
        // print_r($list);
   
		for($a=0; $a < count($list); $a++){

            Mahasiswaln::updateOrCreate(['npm' => $list[$a]['npm']],
            [
                'npm'               => $list[$a]['npm'],
                'nama'              => $list[$a]['nama'],
                'fakultas'          => $list[$a]['nama_fakultas'],
                'prodi'             => $list[$a]['nama_prodi'],
                'kode_prodi'        => $list[$a]['kode_prodi'],
                'asalNegara'        => $list[$a]['kewarganegaraan'],
                'thnMasuk'          => $list[$a]['tahun_angkatan'],
                'agama'             => $list[$a]['agama'],
                'kelamin'           => $list[$a]['jenis_kelamin'],
                'noHp'              => $list[$a]['no_handphone1'],
                'email'             => $list[$a]['email'],
                'status_mahasiswa'  => $list[$a]['status_aktif'],
                'tgl_lulus'         => $list[$a]['tgl_wisuda']
            ]);
		}
    }
}
