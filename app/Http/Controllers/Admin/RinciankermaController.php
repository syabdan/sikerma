<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RinciankermaRequest;
use App\RincianKerma;
use App\RincianSubKerma;
use App\Kerma;
use App\Bidang;
use App\SubBidang;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class RinciankermaController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the bidang
        $rinciankermas  =   Rinciankerma::all();
        if(Sentinel::inRole('user')){
            $kermas         =   Kerma::where('status_kerma' , 1)->whereIn('user_id', [Sentinel::getUser()->id, 4])->get();
        }else{
            $kermas         =   Kerma::where('status_kerma' , 1)->get();

        }
        $bidangs        =   Bidang::all();
        $subbidangs     =   SubBidang::all();
        $title          =   "Ruang Lingkup Kerjasama";
        // Show the page
        return view('admin.rinciankerma.index', compact('rinciankermas', 'title','bidangs','kermas','subbidangs'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $rinciankermas = Rinciankerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','kerma_id','bidang_id','created_at'])->orderBy('id', 'DESC')->get();
        return DataTables::of($rinciankermas)
            ->editColumn('created_at',function(Rinciankerma $rinciankermas) {
                return $rinciankermas->created_at->diffForHumans();
            })
 
            ->addColumn('actions',function($rinciankermas) {
                $actions = '<a onclick="addSub_form('.$rinciankermas->id.')" class="btn btn-xs btn-warning clearCacheForm"><span class="glyphicon glyphicon-plus"></span>Tambah Sub</a>
                            <a onclick="detailFormSub('.$rinciankermas->id.')" class="btn btn-xs btn-success clearCacheForm"><span class="glyphicon glyphicon-search"></span>Detail Sub</a>
                            | <a onclick="editForm('.$rinciankermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update rincian kerjasama"></i></a>';
                $actions .= '<a onclick="deleteForm('.$rinciankermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete rincian kerjasama"></i></a>';
              
                return $actions;
            })
            ->addColumn('namaBidang',function($rinciankermas) {
                return $rinciankermas->bidang->namaBidang;
            })
            ->addColumn('namaMitra',function($rinciankermas) {
                return $rinciankermas->kerma->namaMitra;
            })
            ->rawColumns(['actions','namaBidang','namaMitra'])
            ->make(true);
    }

    public function data_sub($idRincian)
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $rinciansubkermas = RincianSubKerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','rincian_kerma_id','sub_bidang_id', 'tglKegiatan', 'manfaat','dokumenKegiatan', 'bentuk_kegiatan','created_at'])->where('rincian_kerma_id', $idRincian)->get();
        return DataTables::of($rinciansubkermas)
            ->editColumn('created_at',function(RincianSubKerma $rinciansubkermas) {
                return $rinciansubkermas->created_at->diffForHumans();
            })
 
            ->addColumn('namaSubBidang',function($rinciansubkermas) {
                return $rinciansubkermas->subBidang->namaSubBidang;
            })
            
            ->addColumn('namaMitra',function($rinciansubkermas) {
                return $rinciansubkermas->rinciankerma->kerma->namaMitra;
            })
            ->addColumn('file',function($rinciansubkermas) {
                if($rinciansubkermas->dokumenKegiatan == ''){
                    $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
                }else{
                    $file = '<a href='. url('admin/subkerma/ambilFile/'.$rinciansubkermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
                }
            return $file;
        })

        ->addColumn('tglKegiatan',function($data) {
            return tanggal_indonesia($data->tglKegiatan);
        })
            ->rawColumns(['namaSubBidang','file','namaMitra'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $rinciankermas = Rinciankerma::where('id',$id)->first();    
        return json_encode($rinciankermas);
    }
    
    public function ambil_sub($id_sub)
    {
        $SubBidang = SubBidang::select('sub_bidangs.id','namaSubBidang')
                        ->join('bidangs','bidangs.id','sub_bidangs.bidang_id')
                        ->join('rincian_kermas','rincian_kermas.bidang_id','bidangs.id')
                        ->where('rincian_kermas.id',$id_sub)->get();    
        return json_encode($SubBidang);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RinciankermaRequest $request)
    {

        $cekData        = Rinciankerma::where('kerma_id', $request->kerma_id)->where('bidang_id', $request->bidang_id)->count();

        if($cekData > 0){
            return 0;
        }else{
            $rinciankermas  = new Rinciankerma($request->all());
        }

        $rinciankermas->save();
        return json_encode($rinciankermas);
      
    }


    public function edit($rinciankermas)
    {
        
    }

    public function update(Request $request, $id)
    {

        $rinciankermas = Rinciankerma::find($id);
        if ($rinciankermas->update($request->all())) {
            return json_encode($rinciankermas);
        } else {
            return Redirect::route('admin/rinciankerma')->withInput()->with('error', trans('rinciankerma/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $rinciankermas = Rinciankerma::find($id);
        if ($rinciankermas->delete()) {
            return json_encode($rinciankermas);
        } else {
            return Redirect::route('admin/rinciankerma')->withInput()->with('error', trans('rinciankerma/message.error.delete'));
        }
    }
}
