<script>
var table;

$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: '{!! route('admin.mahasiswa.data') !!}',
        columns: [
            // { data: 'rownum', searchable: false },
            @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            @endif
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'noHp', name:'noHp'},
            { data: 'email', name:'email'},
            { data: 'benua'},
            { data: 'alamatDomisili'},
            { data: 'penanggungjawab'},
            { data: 'noHpPenanggungjawab'},
            { data: 'alamatPenanggungjawab'},
            { data: 'noPassport'},
            { data: 'passportBerakhir'},
            { data: 'status_mahasiswa'}
           
        ],

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10 
                }
                // title: 'Data Mahasiswa Asing'
            }
        ],
        // initComplete: function () {
        //     this.api().columns().every( function () {
        //         var column = this;
        //         var select = $('<select><option value=""></option></select>')
        //             .appendTo( $(column.header()).empty() )
        //             .on( 'change', function () {
        //                 var val = $.fn.dataTable.util.escapeRegex(
        //                     $(this).val()
        //                 );
 
        //                 column
        //                     .search( val ? '^'+val+'$' : '', true, false )
        //                     .draw();
        //             } );
 
        //         column.data().unique().sort().each( function ( d, j ) {
        //             select.append( '<option value="'+d+'">'+d+'</option>' )
        //         } );
        //     } );
        // }
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );

});



$(function() {

    //Menampilkan Data dengan DataTable
    table_wisuda=$('#table_wisuda').DataTable({
    "processing" : true,
    'responsive' : true,
    "ajax" : {
        "url" : "{{ route('admin.mahasiswa.data_wisuda') }}",
        "type" : "GET"
    }
    
    });

});

    
// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Mahasiswa');
    $('.btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

}

// OPEN MODAL Wisuda
function wisudaForm(){
    $('input[name=_method]').val('POST');
    $('#wisudaModal').modal('show');
    $('#wisudaModal .modal-title').text('Data Mahasiswa Asing yang telah Wisuda');

}

// OPEN MODAL Sex
function SexModal(sex){
    $('#SexModal').modal('show');
    $('#SexModal .modal-title').text('Data Mahasiswa Asing Perempuan');
    if(sex == 'L'){
        $('#SexModal .modal-title').text('Data Mahasiswa Asing Laki-laki');
    }

    $(function() {
        // Datatable untuk Kelamin
     $('#table_sex').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        bDestroy: true,
        ajax: '{!! url('admin/mahasiswa/data_sex') !!}'+"/"+sex,
        columns: [
            // { data: 'rownum',name:'rownum' },
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'benua', name:'benua'},
            { data: 'alamatDomisili', name:'alamatDomisili'},
            { data: 'penanggungjawab', name:'penanggungjawab'},
            { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
            { data: 'alamatPenanggungjawab', name:'alamatPenanggungjawab'},
            { data: 'noPassport', name:'noPassport'},
            { data: 'passportBerakhir', name:'passportBerakhir'},
           
        ],

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                download: 'open'
            }
        ],
    });
        
    });

}

// OPEN MODAL Sex
function StatusModal(status){
    $('#StatusModal').modal('show');
    $('#StatusModal .modal-title').text('Data Mahasiswa Asing Aktif');
    if(status == 'N'){
        $('#StatusModal .modal-title').text('Data Mahasiswa Asing Tidak Aktif');
    }

    $(function() {
        // Datatable untuk Kelamin
     $('#table_status').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        bDestroy: true,
        ajax: '{!! url('admin/mahasiswa/data_status') !!}'+"/"+status,
        columns: [
            // { data: 'rownum' },
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'benua', name:'benua'},
            { data: 'alamatDomisili', name:'alamatDomisili'},
            { data: 'penanggungjawab', name:'penanggungjawab'},
            { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
            { data: 'alamatPenanggungjawab', name:'alamatPenanggungjawab'},
            { data: 'noPassport', name:'noPassport'},
            { data: 'passportBerakhir', name:'passportBerakhir'},
           
        ],

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                download: 'open'
            }
        ],
    });
        
    });

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Mahasiswa');
    $('.btnSave').removeClass('hide');
    // $("body").on("click",".clearCacheForm",function(){ 
    //         $(".refresh").empty(); 
    //         $(".refresh").attr('readonly',false); 
    // });
    $.ajax({
        url: "{{ url('admin/mahasiswa') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

                $('#id').val(data.id);
                $("#npm").val(data.npm);
                $("#nama").val(data.nama);
                $("#fakultas").val(data.fakultas);
                $("#prodi").val(data.prodi);
                $("#asalNegara").val(data.asalNegara);
                $("#thnMasuk").val(data.thnMasuk);
                $("#agama").val(data.agama);
                $("#kelamin").val(data.kelamin);
                $("#benua").val(data.benua);
                $("#alamatDomisili").val(data.alamatDomisili);
                $("#penanggungjawab").val(data.penanggungjawab);
                $("#noHpPenanggungjawab").val(data.noHpPenanggungjawab);
                $("#alamatPenanggungjawab").val(data.alamatPenanggungjawab);
                $("#noPassport").val(data.noPassport);
                $("#passportBerakhir").val(data.passportBerakhir);
                $("#periodeWisuda").val(data.periodeWisuda);
                $("#noHp").val(data.noHp);
                $("#email").val(data.email);

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show mahasiswa');
    $('.btnSave').addClass('hide');

    $.ajax({
        url: "{{ url('admin/mahasiswa') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.id);
                $('#id').val(data.id);
                $("#npm").val(data.npm);
                $("#nama").val(data.nama);
                $("#fakultas").val(data.fakultas);
                $("#prodi").val(data.prodi);
                $("#asalNegara").val(data.asalNegara);
                $("#thnMasuk").val(data.thnMasuk);
                $("#agama").val(data.agama);
                $("#kelamin").val(data.kelamin);
                $("#benua").val(data.benua);
                $("#alamatDomisili").val(data.alamatDomisili);
                $("#penanggungJawab").val(data.penanggungJawab);
                $("#noHpPenanggungJawab").val(data.noHpPenanggungJawab);
                $("#alamatPenanggungJawab").val(data.alamatPenanggungJawab);
                $("#noPassport").val(data.noPassport);
                $("#passportBerakhir").val(data.passportBerakhir);
                $("#periodeWisuda").val(data.periodeWisuda);
                $("#noHp").val(data.noHp);
                $("#email").val(data.email);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete Mahasiswa');

    $.ajax({
        url: "{{ url('admin/mahasiswa') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#pesan_hapus").text(data.namaMitra);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();

            url = "{{ url('admin/mahasiswa/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.mahasiswa.store') }}";
            } 
            else {
            url = "{{ url('admin/mahasiswa') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                        $('.btnSave').attr('disabled', true);
                        $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#addModal').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                    if(syabdan['responseJSON']['errors']) {
                            if(syabdan['responseJSON']['errors']['benua']){
                                $( '#benuaError' ).html(syabdan['responseJSON']['errors']['benua'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['alamatDomisili']){
                                $( '#alamatDomisiliError' ).html(syabdan['responseJSON']['errors']['alamatDomisili'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['penanggungJawab']){
                                $( '#penanggungJawabError' ).html(syabdan['responseJSON']['errors']['penanggungJawab'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['noHpPenanggungJawab']){
                                $( '#noHpPenanggungJawab' ).html(syabdan['responseJSON']['errors']['noHpPenanggungJawab'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['noPassport']){
                                $( '#noPassportrError' ).html(syabdan['responseJSON']['errors']['noPassport'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['passportBerakhir']){
                                $( '#passportBerakhirError' ).html(syabdan['responseJSON']['errors']['passportBerakhir'][0]);
                            }
                            if(syabdan['responseJSON']['errors']['periodeWisuda']){
                                $( '#periodeWisudaError' ).html(syabdan['responseJSON']['errors']['periodeWisuda'][0]);
                            }
                           
                
                    }
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});

$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});
$(".select2").select2();

</script>
