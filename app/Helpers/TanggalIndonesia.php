<?php
function tanggal_indonesia($tgl, $tampil_hari=true){
	$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);
	
	$text="";
	
	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));
		
		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}
	
	$text .=$tanggal." ".$bulan." ".$tahun;
	
	return $text;
}

// function getBulan($tgl){
// 	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	
// 	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	
// 	return $bulan;
// }

function tanggal_indonesia2($tgl, $tampil_hari=true){
	$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
	$nama_bulan=array(1=>"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des");
	
	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);
	
	$text="";
	
	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));
		
		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}
	
	$text .=$tanggal." ".$bulan." ".$tahun;
	
	return $text;
}

function getBulanRomawi ($bln){
	switch($bln){
		case 1:
			return "I";
		break;
		case 2:
			return "II";
		break;
		case 3:
			return "III";
		break;
		case 4:
			return "IV";
		break;
		case 5:
			return "V";
		break;
		case 6:
			return "VI";
		break;
		case 7:
			return "VII";
		break;
		case 8:
			return "VIII";
		break;
		case 9:
			return "IX";
		break;
		case 10:
			return "X";
		break;
		case 11:
			return "XI";
		break;
		case 12:
			return "XII";
		break;
	}
}

function hariInd($tgl){
	$timestamp = strtotime($tgl);
	$day = date('w', $timestamp);
	
	switch($day){
		case 1:
			$hari='Senin';
		break;
		case 2:
			$hari='Selasa';
		break;
		case 3:
			$hari='Rabu';
		break;
		case 4:
			$hari='Kamis';
		break;
		case 5:
			$hari='Jumat';
		break;
		case 6:
			$hari='Sabtu';
		break;
		case 7:
			$hari='Minggu';
		break;
	}
	return $hari;
}

function terbilang ($nilai){
	$nilai = abs($nilai);
	$huruf = array("", " satu", " dua", " tiga", " empat", " lima", " enam", " tujuh", " delapan", " sembilan", " sepuluh", " sebelas");
	$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = terbilang($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = terbilang($nilai/10)." puluh". terbilang($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . terbilang($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = terbilang($nilai/100) . " ratus" . terbilang($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . terbilang($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = terbilang($nilai/1000) . " ribu" . terbilang($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = terbilang($nilai/1000000) . " juta" . terbilang($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = terbilang($nilai/1000000000) . " milyar" . terbilang(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = terbilang($nilai/1000000000000) . " trilyun" . terbilang(fmod($nilai,1000000000000));
		}     
		return trim($temp);
}

function siswa_waktu_permit ($tglBerakhir){
	// mengatur time zone untuk WIB.
	date_default_timezone_set("Asia/Jakarta");
	
	$tahun	=	substr($tglBerakhir,0,4);
	$bulan	=	substr($tglBerakhir,5,2);
	$hari	=	substr($tglBerakhir,8,2);

	// mencari mktime untuk tanggal 1 Januari 2011 00:00:00 WIB
	$selisih1 =  mktime(0, 0, 0, $bulan, $hari, $tahun);
	
	// mencari mktime untuk current time
	$selisih2 = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
	
	// mencari selisih detik antara kedua waktu
	$delta = $selisih1 - $selisih2;
	
	// proses mencari jumlah hari
	$a = floor($delta / 86400);
	
	// proses mencari jumlah jam
	$sisa = $delta % 86400;
	$b  = floor($sisa / 3600);
	
	// proses mencari jumlah menit
	$sisa = $sisa % 3600;
	$c = floor($sisa / 60);
	
	// proses mencari jumlah detik
	$sisa = $sisa % 60;
	$d = floor($sisa / 1);
	
	// echo "Waktu saat ini: ".date("d-m-Y H:i:s")."<br>";
	return "<b style='color:red' >".$a." hari </b>";
}
?>