<?php

use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends DatabaseSeeder {

	public function run()
	{
		DB::table('users')->truncate(); // Using truncate function so all info will be cleared when re-seeding.
		DB::table('roles')->truncate();
		DB::table('role_users')->truncate();
		DB::table('activations')->truncate();

		$admin = Sentinel::registerAndActivate(array(
			'email'       => 'admin@admin.com',
			'password'    => "admin",
			'first_name'  => 'Syabdan',
			'last_name'   => 'Dalimunthe',
		));

		$superadminRole = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Superadmin',
			'slug' => 'superadmin',
			'permissions' => array('superadmin' => 1),
		]);
		
		$adminRole = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'admin',
			'slug' => 'admin',
			'permissions' => array('admin' => 2),
		]);

        $userRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'User',
			'slug'  => 'user',
		]);


		$admin->roles()->attach($adminRole);

		$this->command->info('Admin User created with username admin@admin.com and password admin');
	}

}