<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! route('admin.laporan_kerma.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'}
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

    $("#negara").change(function() {
     
    var selectValues = $("#negara").val();
    if (selectValues ==""){
    var msg = "";
    $('#tabel').html(msg);
    }else{
        table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! url("admin/laporan_kerma/data_laporan") !!}'+'/null/null/'+selectValues,
        columns: [
            { data: 'rownum' },
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'}
        ]
    });
    }
    });

    $("#tahunan").change(function() {
    var tahun = $("#tahunan").val();
    // var bulan = $("#bulan").val();
    var selectValues = $("#tahunan").val();
    if (selectValues ==""){
    var msg = "";
    $('#tabel').html(msg);
    }else{
        table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! url("admin/laporan_kerma/data_laporan") !!}'+'/'+tahun+'/'+'null/null',
        columns: [
            { data: 'rownum' },
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'}
        ]
    });
    }
    });
    
    $("#status_kerma").change(function() {
    var status = $("#status_kerma").val();
    // var bulan = $("#bulan").val();
    var selectValues = $("#status_kerma").val();
    if (selectValues ==""){
    var msg = "";
    $('#tabel').html(msg);
    }else{
        table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! url("admin/laporan_kerma/data_laporan") !!}'+'/'+status+'/'+'null/null',
        columns: [
            { data: 'rownum' },
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'}
        ]
    });
    }
    });

    $("#thn, #bln").change(function() {
        var thn = $("#thn").val();
        var bln = $("#bln").val();
        var selectValues = $("#bln").val();
    if (selectValues ==""){
    var msg = "";
    $('#tabel').html(msg);
    }else{
        table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! url("admin/laporan_kerma/data_laporan") !!}'+'/'+thn+'/'+bln+'/'+'null',
        columns: [
            { data: 'rownum' },
            { data: 'kategori', name:'kategori'},
            { data: 'namaUnit'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'}
        ]
    });
    }
    });


//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Kerma');
    $('.btnSave').addClass('hide');

    $.ajax({
        url: "{{ url('admin/kerma') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.id);
                $("#kategori option[value='" + data.kategori + "']").prop("selected", true).change();
                $("#country_id option[value='" + data.country_id + "']").prop("selected", true).change();
                $("#user_id option[value='" + data.user_id + "']").prop("selected", true).change();
                $("#jenisKerma option[value='" + data.jenis_kerma_id + "']").prop("selected", true).change();
                $("#jenisPartner option[value='" + data.jenis_partner_id + "']").prop("selected", true).change();
                $("#noHp").val(data.noHp);
                $("#tingkat option[value='" + data.tingkat + "']").prop("selected", true).change();
                $("#email").val(data.email);
                $("#namaMitra").val(data.namaMitra);
                $("#tglPengesahan").val(data.tglPengesahan);
                $("#tglAwal").val(data.tglAwal);
                $("#tglAkhir").val(data.tglAkhir);
                
        
                if(data.status_kerma == 1){

                    // document.getElementById('status_kerma').attr("checked", true);
                    document.getElementById('status_kerma').checked = true;
                }else{
                    document.getElementById('status_kerma').attr("checked", false);
                    // document.getElementById('status_kerma').checked = false;

                }

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


$('#lap_kerma').on('change', function(){
     if(this.value == 'lap_status'){
        $('#lap_status').show();
        $('#lap_negara').hide();
    }else{
        $('#lap_status').hide();
        $('#lap_negara').show();
    }
});

$(".select2").select2();

</script>
