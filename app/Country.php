<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Country extends Model
{
    use Eloquence;


    protected $table = 'countries';
    protected $guarded  = ['id'];
    protected $searchableColumns = ['name'];

    public function kerma()
    {
        return $this->hasMany('App\Kerma');
    }
    
    public function tamu()
    {
        return $this->hasMany('App\Tamu');
    }
    
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
