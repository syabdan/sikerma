<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mahasiswaln;

class insert_data_api_mahasiswa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:mahasiswa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert data mahasiswa asing from API to database sikerma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mahasiswa_asing_all = mahasiswa_asing_all();
        $sumber = $mahasiswa_asing_all;
        $list = json_decode($sumber, true);
        
		for($a=0; $a < count($list); $a++){
                Mahasiswaln::updateOrCreate(['npm'  => $list[$a]['npm']],[
                    'npm'               => $list[$a]['npm'],
                    'nama'              => $list[$a]['nama'],
                    'fakultas'          => $list[$a]['nama_fakultas'],
                    'prodi'             => $list[$a]['nama_prodi'],
                    'kode_prodi'        => $list[$a]['kode_prodi'],
                    'kode_fakultas'     => $list[$a]['kode_fakultas'],
                    'asalNegara'        => $list[$a]['kewarganegaraan'],
                    'thnMasuk'          => $list[$a]['tahun_angkatan'],
                    'agama'             => $list[$a]['agama'],
                    'kelamin'           => $list[$a]['jenis_kelamin'],
                    'noHp'              => $list[$a]['no_handphone1'],
                    'email'             => $list[$a]['email'],
                    'status_mahasiswa'  => $list[$a]['status_aktif'],
                    'tgl_lulus'         => $list[$a]['tgl_wisuda']
                ]);
		}
    }
}
