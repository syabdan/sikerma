<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJenispartnerToTableKermas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kermas', function (Blueprint $table) {
            $table->string('jenis_partner_id')->unsigned();
            $table->foreign('jenis_partner_id')->references('id')->on('jenis_partners');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kermas', function (Blueprint $table) {
            $table->dropColumn('jenis_partner_id');
            
        });
    }
}
