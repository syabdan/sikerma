<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswaln extends Model
{
    protected $fillable = ['id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at','email','noHp','status_mahasiswa','tgl_lulus','kode_prodi','kode_fakultas'];

    protected $guarded = ['id'];

    public function permit()
    {
        return $this->hasMany('App\Permit');
    }
    
}