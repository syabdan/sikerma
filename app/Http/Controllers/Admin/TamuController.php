<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TamuRequest;
use App\Tamu;
use App\Unit;
use App\Country;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;

class TamuController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the bidang
        $guests        =   Tamu::all();
        $countries     =   Country::all();
        if(Sentinel::inRole('user')){
            $units         =   Unit::where('id', Sentinel::getUser()->unit_id)->get();

        }else{
            $units         =   Unit::all();
        }
        $title         =   "Kunjungan Tamu / Narasumber Luar Negeri";
        // Show the page
        return view('admin.tamu.index', compact('guests', 'title','countries','units'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));   
        if(Sentinel::inRole('user')){
            $guest = Tamu::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'tamus.id','tamus.country_id','tamus.unit_id', 'tamus.nama','noHp','tamus.email','tglKedatangan', 'tujuan','keterangan','dokumentasiTamu', 'tamus.created_at'])
            ->join('users','users.unit_id','tamus.unit_id')
            ->where('users.id', Sentinel::getUser()->id)
            ->orderBy('tamus.id', 'DESC')->get();

        }else{
            $guest = Tamu::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','country_id','unit_id', 'nama','noHp','email','tglKedatangan', 'tujuan','keterangan','dokumentasiTamu', 'created_at'])->orderBy('id', 'DESC')->get();
        }
        return DataTables::of($guest)
            ->editColumn('created_at',function(Tamu $guest) {
                return $guest->created_at->diffForHumans();
            })
 
            ->addColumn('actions',function($guest) {
                $actions = '<a onclick="showForm('.$guest->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view tamu"></i></a>
                            <a onclick="editForm('.$guest->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update tamu"></i></a>';
                $actions .= '<a onclick="deleteForm('.$guest->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete tamu"></i></a>';
              
                return $actions;
            })
            ->addColumn('negara',function($guest) {
                return $guest->country->name;
            })
            ->addColumn('unit',function($guest) {
                return $guest->unit->namaUnit;
            })
            ->addColumn('file',function($guest) {
                $file = '<a href='. url('admin/tamu/ambilFile/'.$guest->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
            return $file;
        })
            ->rawColumns(['actions','negara','unit','file'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ambilFile($id)
    {
        $model = Tamu::where('id',$id)->first(); 
        return response()->file(storage_path('/uploads/tamu/'.$model->dokumentasiTamu));   
    }

    public function show($id)
    {
        $model = Tamu::where('id',$id)->first();    
        return json_encode($model);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(TamuRequest $request)
    {

        $model      =   new Tamu();
        //upload dokumen Permit
        if ($file = $request->file('dokumentasiTamu')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/tamu/'))
            {
                File::makeDirectory( storage_path() . '/uploads/tamu/');
            }
            $destinationPath = storage_path() . '/uploads/tamu/';

            $file->move($destinationPath, $gambarName);
            $model->dokumentasiTamu = $gambarName;
        }
        
  

        $model->tujuan                  = $request->tujuan;
        $model->nama                    = $request->nama;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->keterangan              = $request->keterangan;
        $model->unit_id                 = $request->unit_id;
        $model->country_id              = $request->country_id;
        $model->tglKedatangan           = date('Y-m-d', strtotime($request->tglKedatangan));
        $model->save();

        return json_encode($model);
    }


    public function edit($rinciankermas)
    {
        
    }

    public function update(Request $request, $id)
    {
        $model      =   Tamu::find($id);
        //upload dokumen Permit
        if ($file = $request->file('dokumentasiTamu')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/tamu/'))
            {
                File::makeDirectory( storage_path() . '/uploads/tamu/');
            }
            $destinationPath = storage_path() . '/uploads/tamu/';

            $file->move($destinationPath, $gambarName);
            $model->dokumentasiTamu = $gambarName;
        }

        $model->tujuan                  = $request->tujuan;
        $model->nama                    = $request->nama;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->keterangan              = $request->keterangan;
        $model->unit_id                 = $request->unit_id;
        $model->country_id              = $request->country_id;
        $model->tglKedatangan           = date('Y-m-d', strtotime($request->tglKedatangan));
        $model->save();

        return json_encode($model);
    }

    public function destroy($id)
    {
        $model = Tamu::find($id);
        if ($model->delete()) {
            return json_encode($model);
        } else {
            return Redirect::route('admin/tamu')->withInput()->with('error', trans('tamu/message.error.delete'));
        }
    }
}
