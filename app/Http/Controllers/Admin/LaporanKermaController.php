<?php

namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\KermaRequest;
use App\Kerma;
use App\Country;
use App\JenisKerma;
use App\JenisPartner;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;
use Auth;
use PDF;

class LaporanKermaController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $kermas = Kerma::all();
        $countries = Country::all();
        $jenisKermas = JenisKerma::all();
        $jenisPartners = JenisPartner::all();
        $title          = "Laporan Kerja Sama";
        // Show the page
        return view('admin.laporan_kerma.index', compact('kermas','countries','jenisKermas','jenisPartners','title'));
    }
    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->orderBy('kermas.id', 'DESC')
            ->get();
        return DataTables::of($kermas)

            ->addColumn('actions',function($kermas) {
                $actions = '<a onclick="showForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                            <a onclick="editForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$kermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
              
                return $actions;
            })
            ->addColumn('status',function($kermas) {
                if($kermas->status_kerma == 1){
                    $status_kerma = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else if($kermas->status_kerma == 0){
                    $status_kerma = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }else{
                    $status_kerma = '<span class="btn btn-warning btn-xs btn-xs">Lain-lain</span>';
                }
              
                return $status_kerma;
            })
            ->addColumn('negara',function($kermas) {
                return $kermas->country->name;
            })
            ->addColumn('user',function($kermas) {
                return $kermas->user->first_name;
            })
          
    
            ->addColumn('jenisPartner',function($kermas) {
                return $kermas->jenisPartner->namaJenisPartner;
            })

            ->addColumn('file',function($kermas) {
              
                    $file = '<a href='. url('admin/kerma/ambilFile/'.$kermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
                return $file;
            })

            ->addColumn('tglPengesahan',function($data) {
                return tanggal_indonesia($data->tglPengesahan);
            })
            
            ->addColumn('tglAwal',function($data) {
                return tanggal_indonesia($data->tglAwal);
            })
            
            ->addColumn('tglAkhir',function($data) {
                return tanggal_indonesia($data->tglAkhir);
            })  

            ->rawColumns(['actions','status','file','negara','user','jenisPartner'])
            ->make(true);
    }
    
    public function dataLaporan($status,$bulan,$negara)
    {
       
        DB::statement(DB::raw('set @rownum=0'));    
        if($negara != 'null'){
            $kermas =    Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->where('country_id', $negara)->orderBy('kermas.id', 'DESC')->get();

        }else if($bulan != 'null'){
            $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->whereMonth('kermas.created_at', $bulan)
            ->whereYear('kermas.created_at', $tahun)->orderBy('kermas.id', 'DESC')->get();
        }else{
            $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->where('status_kerma', $status)->orderBy('kermas.id', 'DESC')->get();
        }
        return DataTables::of($kermas)

            ->addColumn('actions',function($kermas) {
                $actions = '<a onclick="showForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                            <a onclick="editForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$kermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
              
                return $actions;
            })
            ->addColumn('status',function($kermas) {
                if($kermas->status_kerma == 1){
                    $status_kerma = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else if($kermas->status_kerma == 2){
                    $status_kerma = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }else{
                    $status_kerma = '<span class="btn btn-warning btn-xs btn-xs">Lain-lain</span>';
                }
              
                return $status_kerma;
            })
            ->addColumn('negara',function($kermas) {
       
                return $kermas->country->name;
            })
            ->addColumn('user',function($kermas) {
       
                return $kermas->user->first_name;
            })
            ->addColumn('jenisPartner',function($kermas) {
       
                return $kermas->jenisPartner->namaJenisPartner;
            })

            ->addColumn('file',function($kermas) {
              
                    $file = '<a href='. url('admin/kerma/ambilFile/'.$kermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
                return $file;
            })

            ->addColumn('tglPengesahan',function($data) {
                return tanggal_indonesia($data->tglPengesahan);
            })
            
            ->addColumn('tglAwal',function($data) {
                return tanggal_indonesia($data->tglAwal);
            })
            
            ->addColumn('tglAkhir',function($data) {
                return tanggal_indonesia($data->tglAkhir);
            })
            ->rawColumns(['actions','status','file','negara','user','jenisPartner'])
            ->make(true);
    }
    
    public function cetakLaporan(Request $request){

        $jenisLaporan = $request->jenisLaporan;

        DB::statement(DB::raw('set @rownum=0'));
        if($jenisLaporan == 'negara'){
            $negara = $request->negara;

            $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->where('country_id', $negara)->orderBy('kermas.id', 'DESC')->get();

        }else if($jenisLaporan == 'bulanan'){
            $tahun = $request->thn;
            $bulan = $request->bln;
            
            $kermas =Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->whereMonth('kermas.created_at', $bulan)
            ->whereYear('kermas.created_at', $tahun)->orderBy('kermas.id', 'DESC')->get();
        }else if($jenisLaporan == 'status_kerma'){
            $status_kerma = $request->status_kerma;

            $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('users','users.id','kermas.user_id')
            ->leftjoin('units','units.id','users.unit_id')
            ->where('status_kerma', $status_kerma)->orderBy('kermas.id', 'DESC')->get();
        }
        $kepala_kantor = "Dr. Husnul Kausarian, M.Sc.";

        $pdf = PDF::loadView('admin.laporan_kerma.pdf', compact('kermas','kepala_kantor'))->setPaper('a4', 'landscape');
        // return PDF::loadView('frontend.pdf', compact('cetakSkpd'))->save('app/lapor/my_stored_file.pdf')->stream('download.pdf');
        return $pdf->stream('laporan_kerjasama.pdf');
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $kermas = Kerma::where('id',$id)->first();    
        return json_encode($kermas);
    }
    
    public function ambilFile($id)
    {
        $kermas = Kerma::where('id',$id)->first(); 
        return response()->file(storage_path('/uploads/kerma/'.$kermas->dokumenKerma));   
    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(KermaRequest $request)
    {
        $model      =   new Kerma();
        //upload dokumen Permit
        if ($file = $request->file('dokumenKerma')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/kerma/'))
            {
                File::makeDirectory( storage_path() . '/uploads/kerma/');
            }
            $destinationPath = storage_path() . '/uploads/kerma/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenKerma = $gambarName;
        }
        
        $model->kategori                = $request->kategori;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->country_id              = $request->country_id;
        $model->user_id                 = Sentinel::getUser()->id;
        $model->jenis_kerma_id          = $request->jenisKerma;
        $model->jenis_partner_id        = $request->jenisPartner;
        $model->tingkat                 = $request->tingkat;
        $model->namaMitra               = $request->namaMitra;
        $model->tglPengesahan           = date('Y-m-d', strtotime($request->tglPengesahan));
        $model->tglAwal                 = date('Y-m-d', strtotime($request->tglAwal));
        $model->tglAkhir                = date('Y-m-d', strtotime($request->tglAkhir));
        $model->status_kerma            = $request->status_kerma;

        $model->save();

        return json_encode($model);
     
    }


    public function edit($kermas)
    {
        
    }

    public function update(Request $request, $id)
    {

        $model = Kerma::find($id);

        //upload dokumen Permit
        if ($file = $request->file('dokumenKerma')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/kerma/'))
            {
                File::makeDirectory( storage_path() . '/uploads/kerma/');
            }
            $destinationPath = storage_path() . '/uploads/kerma/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenKerma = $gambarName;
        }
        
  

        $model->kategori                = $request->kategori;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->country_id              = $request->country_id;
        $model->user_id                 = Sentinel::getUser()->id;
        $model->jenis_kerma_id          = $request->jenisKerma;
        $model->jenis_partner_id        = $request->jenisPartner;
        $model->tingkat                 = $request->tingkat;
        $model->namaMitra               = $request->namaMitra;
        $model->tglPengesahan           = date('Y-m-d', strtotime($request->tglPengesahan));
        $model->tglAwal                 = date('Y-m-d', strtotime($request->tglAwal));
        $model->tglAkhir                = date('Y-m-d', strtotime($request->tglAkhir));
        $model->status_kerma            = $request->status_kerma;

        $model->update();

        return json_encode($model);

    }

    public function destroy($id)
    {
        $kermas = Kerma::find($id);
        if ($kermas->delete()) {
            return json_encode($kermas);
        } else {
            return Redirect::route('admin/kerma')->withInput()->with('error', trans('kerma/message.error.delete'));
        }
    }
}
