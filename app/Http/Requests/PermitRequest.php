<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'noPermit' => 'required|unique:permits',
            'tglDikeluarkan' => 'required|before_or_equal:tglBerakhir',
            'tglBerakhir' => 'required|after_or_equal:tglDikeluarkan',
            'dokumenPermit' => 'required|mimes:pdf|max:2000',
            
        ];
    }
}
