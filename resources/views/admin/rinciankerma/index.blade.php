@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$title}} List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> {{$title}}</a></li>
        <li class="active">{{$title}}</li>
    </ol>
</section>


<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    {{$title}}
                </h4>
                <div class="pull-right">
                    <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>Nama Mitra</th>
                            <th>Bidang</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus data ini ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" ><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
  </div>
</div>
<div class="modal fade" id="addModal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />
        <input type="hidden" name="id" id="id" />

            <div class="modal-body">
                <div class="form-group {{ $errors->first('kerma_id', 'has-error') }}">
                    <label for="email" class="col-sm-3 control-label">Kerja Sama *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Kerja Sama..." name="kerma_id" id="kerma_id" style="width: 100%;" required>
                        <option value="">Select Kerja Sama ... </option>
                           @foreach($kermas as $kerma)
                            <option value="{{ $kerma->id }}"
                                    @if(old('kerma_id') === '{{ $kerma->id }}' ) selected="selected" @endif >{{$kerma->id}} - {{ $kerma->namaMitra}}
                            </option>
                           @endforeach

                        </select>
                    </div>
                    <span class="help-block">{{ $errors->first('bidang_id', ':message') }}</span>
                </div>
                <div class="form-group {{ $errors->first('bidang_id', 'has-error') }}">
                    <label for="email" class="col-sm-3 control-label">Bidang *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Bidang..." name="bidang_id" id="bidang_id" style="width: 100%;" required>
                        <option value="">Select Bidang ... </option>
                           @foreach($bidangs as $bidang)
                            <option value="{{ $bidang->id }}"
                                    @if(old('bidang_id') === '{{ $bidang->id }}' ) selected="selected" @endif >{{ $bidang->namaBidang}}
                            </option>
                           @endforeach

                        </select>
                    </div>
                    <span class="help-block">{{ $errors->first('bidang_id', ':message') }}</span>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
					<button type="submit" class="btn btn-primary pull-right btnSave" id="btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
				</div>
                
            </div>
        </form>

        </div>
  </div>
</div>

<!-- Modal Rincian sub  -->
<div class="modal fade" id="addSub_modal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="_method"  />
            <input type="hidden" name="idSub" id="idSub" />
            <!-- <input type="hidden" name="idRincian" id="idRincian" /> -->

                <div class="modal-body">
                    <div class="form-group {{ $errors->first('kerma_id', 'has-error') }}">
                        <label for="email" class="col-sm-3 control-label">Rincian Kerja Sama *</label>
                        <div class="col-sm-9">
                            <select class="form-control select2" title="Select Rincian Kerja Sama..." name="rincian_kerma_id" id="rincian_kerma_id" required>
                            <option value="">Select Rincian Kerja Sama ... </option>
                            @foreach($rinciankermas as $rincian_kerma)
                                <option value="{{ $rincian_kerma->id }}"
                                        @if(old('rincian_kerma_id') === '{{ $rincian_kerma->id }}' ) selected="selected" @endif >{{$rincian_kerma->id}} - {{ $rincian_kerma->kerma->namaMitra}}
                                </option>
                            @endforeach
                            </select>
                        </div>
                        <span class="help-block">{{ $errors->first('rincian_kerma', ':message') }}</span>
                    </div>
                    <div class="form-group {{ $errors->first('sub_bidang_id', 'has-error') }}">
                        <label for="email" class="col-sm-3 control-label">Sub Bidang *</label>
                        <div class="col-sm-9">
                            <select class="form-control select2" title="Select Sub Bidang..." name="sub_bidang_id" id="sub_bidang_id" required>
                            <option value="">Select Sub Bidang ... </option>
                          

                            </select>
                        </div>
                        <span class="help-block">{{ $errors->first('sub_bidang_id', ':message') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="manfaat" class="col-sm-3 control-label">Manfaat </label>
                        <div class="col-sm-9">
                            <input type="text" id="manfaat" name="manfaat" class="form-control refresh" placeholder="Manfaat" value="{!! old('manfaat') !!}">
                            <span style="color: red;"><i><p id="manfaatError"></p></i></span>
                        </div>
                        {!! $errors->first('manfaat', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <div class="form-group">
                        <label for="manfaat" class="col-sm-3 control-label">Bentuk Kegiatan </label>
                        <div class="col-sm-9">
                            <textarea id="bentuk_kegiatan" name="bentuk_kegiatan" class="form-control refresh" placeholder="Bentuk Kegiatan" value="{!! old('bentuk_kegiatan') !!}" ></textarea>
                            <span style="color: red;"><i><p id="bentuk_kegiatanError"></p></i></span>
                        </div>
                        {!! $errors->first('bentuk_kegiatan', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group">
                        <label for="tglKegiatan" class="col-sm-3 control-label">Tanggal Kegiatan </label>
                        
                        <div class="col-sm-9">
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                        </div>
                            <input type="text" class="form-control datepicker"  id="tglKegiatan" name="tglKegiatan" class="form-control refresh" value="{!! old('tglKegiatan') !!}" autocomplete="off"/>
                        </div>
                            
                            <span style="color: red;"><i><p id="tglKegiatanError"></p></i></span>
                        </div>
                        {!! $errors->first('tglKegiatan', '<span class="help-block">:message</span>') !!}
                    </div>      

                    <div class="form-group {{ $errors->first('dokumen', 'has-error') }}">
                        <label for="dokumen" class="col-sm-3 control-label">Dokumen (.PDF) *</label>
                            <div class="col-sm-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 500px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select File</span>
                                <span class="fileinput-exists">Change</span>
                                <input id="dokumenSubKerma" name="dokumenSubKerma" type="file" class="form-control" required/></span>
                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <span style="color: red;"><i><p id="dokumenKermaError"></p></i></span>
                            </div>
                            <span class="help-block">{{ $errors->first('dokumen', ':message') }}</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
                        <button type="submit" class="btn btn-primary pull-right btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
                    </div>
                    
                </div>
            </form>


        </div>
  </div>
</div>


<div class="modal fade" id="modalTableSub"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content modal-lg" >
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <section class="content paddingleft_right15">
                <div class="row">
                    <div class="panel panel-primary ">
                        
                        <br />
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table table-bordered " id="tableSub">
                                <thead>
                                    <tr class="filters">
                                        <th>No</th>
                                        <th>Bentuk Kegiatan</th>
                                        <th>Manfaat</th>
                                        <th>Nama Mitra</th>
                                        <th>Bidang Kerjasama</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Dokumen</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>    <!-- row-->
            </section>
        </div>
    </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>

@include('admin.rinciankerma.ajax')
@include('admin.rinciankerma.ajax_sub')



@stop
