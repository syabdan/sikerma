@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$title}} List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>

@stop


{{-- Page content --}}
@section('content')
<?php
    $tanggal=gmdate("d",time()+60*60*7);
    $bulan=gmdate("m",time()+60*60*7);
    $tahun=gmdate("Y",time()+60*60*7);

    function getBulan($bln){
                switch ($bln){
                    case 1:
                        return "Januari";
                        break;
                    case 2:
                        return "Februari";
                        break;
                    case 3:
                        return "Maret";
                        break;
                    case 4:
                        return "April";
                        break;
                    case 5:
                        return "Mei";
                        break;
                    case 6:
                        return "Juni";
                        break;
                    case 7:
                        return "Juli";
                        break;
                    case 8:
                        return "Agustus";
                        break;
                    case 9:
                        return "September";
                        break;
                    case 10:
                        return "Oktober";
                        break;
                    case 11:
                        return "November";
                        break;
                    case 12:
                        return "Desember";
                        break;
                }
            }
    ?>
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">{{$title}}</a></li>
        <li class="active">{{$title}} List</li>
    </ol>
</section>


<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                {{$title}} List
                </h4>
            </div>
            <br />
            
            <div class="panel-body">

            <div class="form-group">
                    <h5 class="m-t-30 m-b-10">Laporan</h5>
                    <select class="form-control select2" data-style="form-control" name="lap_kerma" id="lap_kerma">
                    <option value="">-- Pilih Laporan --</option>
                    <option value="lap_status">Per Status</option>
                    <option value="lap_negara">Per Negara</option>
                    </select>
            </div>
       
            <div class="form-group" style="display:none" id="lap_status">
                <form action="{{ url('admin/laporan_kerma/data_laporan')}}" method="post" target="_blank">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                <input name="jenisLaporan" type="hidden" value="status_kerma">

                    <h5 class="m-t-30 m-b-10" >Pilih Status Kerjasama</h5>
                    <select class="form-control select2" data-style="form-control" name="status_kerma" id="status_kerma">
                    <option value="">--- Pilih Status Kerjasama ---</option>    
                    <option value="1">Aktif</option>    
                    <option value="2">Tidak Aktif</option>    
                    <option value="3">Lain-lain</option>    
                    </select>
                    <button class="btn btn-success" type="submit" name="print" id="print" value="Print"><i class="glyphicon glyphicon-download-alt" > Download</i>
                </form>
                </div>

                <div class="form-group" style="display:none;" id="lap_bulanan">
                    <form action="{{ url('admin/laporan_kerma/data_laporan')}}" method="post" target="_blank">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <input name="jenisLaporan" type="hidden" value="bulanan">

                    <h5 class="m-t-30 m-b-10">Pilih Bulan</h5>
                    <select class="form-control select2" name="bln" id="bln" class="span6">
                        <?php echo '<option value="" ></option>';
                            for($i=1;$i<=12;$i++){
                            if($bulan==$i){
                                echo '<option value="'.$i.'" >'.getBulan($i).'</option>';
                            }else{
                                echo '<option value="'.$i.'">'.getBulan($i).'</option>';
                            }
                            }?>
                    </select>

                    <select class="form-control select2" name="thn" id="thn" class="span8">
                        <?php $year=date('Y');$awal=$year-80;$akhir=$year+2;
                            echo '<option value="" ></option>';
                            for($i=$awal;$i<=$akhir;$i++){
                            if($tahun==$i){
                                echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
                                }else{
                                echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                            }?>
                        </select>
                        <button class="btn btn-success" type="submit" name="print" id="print" value="Print"><i class="glyphicon glyphicon-download-alt" > Download</i>
                    </form>
                </div>

                <div class="form-group" style="display:none;" id="lap_negara">
                <form action="{{ url('admin/laporan_kerma/data_laporan')}}" method="post" target="_blank">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <input name="jenisLaporan" type="hidden" value="negara">
                    <h5 class="m-t-30 m-b-10">Pilih Negara</h5>

                        <select class="form-control select2" title="Select Country..." name="negara" id="negara" style="width:100%" required>
                        <option value="">Select Country ... </option>
                           @foreach($countries as $country)
                            <option value="{{ $country->id }}"
                                    @if(old('negara') === '{{ $country->id }}' ) selected="selected" @endif >{{ $country->name}}
                            </option>
                           @endforeach

                        </select>
                    

               
                    <button class="btn btn-success" type="submit" name="print" id="print" value="Print"><i class="glyphicon glyphicon-download-alt" > Download</i>
                    </form>
                </div>

                <br>
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Pihak UIR</th>
                            <th>Nama Mitra</th>
                            <th>Negara</th>
                            <th>Jenis Partner</th>
                            <th>Tingkat</th>
                            <th>Tgl Pengesahan</th>
                            <th>Tgl Awal</th>
                            <th>Tgl Akhir</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus <b id="pesan_hapus"></b> ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" id="btnSave"><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
  </div>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>

@include('admin.laporan_kerma.ajax')



@stop
