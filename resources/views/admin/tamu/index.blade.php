@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$title}} List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> {{$title}}</a></li>
        <li class="active">{{$title}}</li>
    </ol>
</section>


<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    {{$title}}
                </h4>
                <div class="pull-right">
                    <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>Nama</th>
                            <th>Negara</th>
                            <th>Unit/Prodi/Fakultas</th>
                            <th>Tujuan</th>
                            <th>Dokumentasi</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus <b id="pesan_hapus"></b> ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" ><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
  </div>
</div>
<div class="modal fade" id="addModal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />
        <input type="hidden" name="id" id="id" />

            <div class="modal-body">
                <div class="form-group">
                    <label for="nama" class="col-sm-3 control-label">Nama *</label>
                    <div class="col-sm-9">
                        <input type="text" id="nama" name="nama" class="form-control refresh" placeholder="Nama" value="{!! old('nama') !!}">
                        <span style="color: red;"><i><p id="namaError"></p></i></span>
                    </div>
                    {!! $errors->first('nama', '<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="noHp" class="col-sm-3 control-label">No. Hp *</label>
                    <div class="col-sm-9">
                        <input type="text" id="noHp" name="noHp" class="form-control refresh" placeholder="No HP" value="{!! old('noHp') !!}">
                        <span style="color: red;"><i><p id="noHpError"></p></i></span>
                    </div>
                    {!! $errors->first('noHp', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email </label>
                    <div class="col-sm-9">
                        <input type="text" id="email" name="email" class="form-control refresh" placeholder="Email" value="{!! old('email') !!}">
                        <span style="color: red;"><i><p id="emailError"></p></i></span>
                    </div>
                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->first('country_id', 'has-error') }}">
                    <label for="country_id" class="col-sm-3 control-label">Negara *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Country..." name="country_id" id="country_id" style="width: 100%;" required>
                        <option value="">Select Country ... </option>
                           @foreach($countries as $country)
                            <option value="{{ $country->id }}"
                                    @if(old('country_id') === '{{ $country->id }}' ) selected="selected" @endif >{{ $country->name}}
                            </option>
                           @endforeach

                        </select>
                        <span style="color: red;"><i><p id="country_idError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('country_id', ':message') }}</span>
                </div>

                <div class="form-group {{ $errors->first('unit_id', 'has-error') }}">
                    <label for="unit_id" class="col-sm-3 control-label">Unit/Prodi/Fakultas Tujuan *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Unit..." name="unit_id" id="unit_id" style="width: 100%;" required >
                        <option value="">Select Unit ... </option>
                           @foreach($units as $unit)
                            <option value="{{ $unit->id }}"
                                    @if(old('unit_id') === '{{ $unit->id }}' ) selected="selected" @endif >{{ $unit->namaUnit}}
                            </option>
                           @endforeach

                        </select>
                        <span style="color: red;"><i><p id="unit_idError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('unit_id', ':message') }}</span>
                </div>

                <div class="form-group">
                    <label for="tujuan" class="col-sm-3 control-label">Tujuan </label>
                    <div class="col-sm-9">
                        <input type="text" id="tujuan" name="tujuan" class="form-control refresh" placeholder="Tujuan" value="{!! old('tujuan') !!}">
                        <span style="color: red;"><i><p id="tujuanError"></p></i></span>
                    </div>
                    {!! $errors->first('tujuan', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group">
                    <label for="tglKedatangan" class="col-sm-3 control-label">Tanggal Kedatangan </label>
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker"  id="tglKedatangan" name="tglKedatangan" class="form-control refresh" value="{!! old('tglKedatangan') !!}" required autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="tglKedatanganError"></p></i></span>
                    </div>
                    {!! $errors->first('tglKedatangan', '<span class="help-block">:message</span>') !!}
                </div>     

                <div class="form-group">
                    <label for="keterangan" class="col-sm-3 control-label">Keterangan </label>
                    <div class="col-sm-9">
                        <textarea id="keterangan" name="keterangan" class="form-control refresh" placeholder="Keterangan" value="{!! old('keterangan') !!}" > </textarea>
                        <span style="color: red;"><i><p id="keteranganError"></p></i></span>
                    </div>
                    {!! $errors->first('keterangan', '<span class="help-block">:message</span>') !!}
                </div> 

                <div class="form-group {{ $errors->first('dokumen', 'has-error') }}">
                    <label for="dokumen" class="col-sm-3 control-label">Dokumentasi (*.pdf Max 2000 KB) *</label>
                        <div class="col-sm-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 500px; max-height: 200px;"></div>
                            <div>
                            <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select File</span>
                            <span class="fileinput-exists">Change</span>
                            <input id="dokumentasiTamu" name="dokumentasiTamu" type="file" class="form-control" accept="application/pdf" required/></span>
                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                        <span style="color: red;"><i><p id="dokumenTamuError"></p></i></span>
                        <span style="color: red;"><i> Passport, Halaman Visa, CV</i></span>
                        </div>
                        <span class="help-block">{{ $errors->first('dokumen', ':message') }}</span>
                    </div>
                </div>

                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
					<button type="submit" class="btn btn-primary pull-right btnSave" id="btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
				</div>
                
            </div>
        </form>

        </div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>


@include('admin.tamu.ajax')



@stop
