<!doctype html>
<html lang="en">
  <head>
    <title>SIKERMA | Sistem Informasi Kerjasama dan Mahasiswa Asing UIR</title>

  <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Sistem Informasi Kerja Sama dan Mahasiswa Asing Universitas Islam Riau" />
	<meta name="author" content="Syabdan Dalimunthe" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('login') }}/fonts/icomoon/style.css">

    <link rel="stylesheet" href="{{ asset('login') }}/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('login') }}/css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('login') }}/css/style.css">
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
  </head>
  <body >

  <div class="content" style="background-color: #fff">
    <div class="container">
      <div class="row ">
        <div class="col-md-6 hidden-sm">
            <img src="{{ asset('login') }}/images/sikerma.jpg" alt="Image" class="img-fluid">
        </div>
        <div class="col-md-6 contents">
            <div class="row justify-content-center">
            
              <div class="col-md-8">
                <div class="mb-4">
                    <h3 style="font-weight: bold">SIKERMA</h3>
                    {{-- <img src="{{ asset('/') }}/frontend/images/siami6.png" alt="alternative" width="50%"> --}}
                    <p class="mb-4" style="color:#F50057">Sistem Informasi Kerjasama dan Mahasiswa Asing Universitas Islam Riau</p>
              </div>


              <!-- Notifications -->
                <div id="notific">
                    @include('notifications')
                </div>

              <form action="{{ route('signin') }}" autocomplete="on" method="post" role="form" id="login_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="form-group first {{ $errors->first('email', 'has-error') }}">
                  <label for="email">Email</label>
                  <input id="email" type="email" class="input100 form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                  
                    <span class="invalid-feedback" role="alert">
                        <strong style="color:red">{!! $errors->first('email', '<span class="help-block">:message</span>') !!}</strong>
                    </span>
                    
                </div>

                <div class="form-group last mb-2 {{ $errors->first('password', 'has-error') }}">
                  <label for="password">Password</label>
                  <input  id="password" type="password" class="input100 form-control" name="password" required autocomplete="current-password">
                
                    <span class="invalid-feedback" role="alert">
                        <strong style="color:red">{!! $errors->first('password', '<span class="help-block">:message</span>') !!}</strong>
                    </span>
                
                </div>

                <div class="d-flex mb-3 align-items-center" >
                <label class="control control--checkbox mb-0"><span class="caption">Remember me</span>
                    <input type="checkbox"  style="background-color:#F50057;" />
                    <div class="control__indicator"></div>
                </label>
                {{-- <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span> --}}
                </div>

                <button type="submit" class="btn btn-block" style="background-color:#F50057; color:#fff">
                    Masuk
                </button>
                <br>
                <a href="{{ url('/') }}" style="text-decoration: none; color: #F50057; margin-top:50px"><img src="{{ asset('login/images/arrow_back-24px.svg') }}" alt=""> Kembali</a>
  
              </form>
             
              </div>

            </div>
        </div>
    
      </div>
    </div>
  </div>


    <script src="{{ asset('login') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('login') }}/js/popper.min.js"></script>
    <script src="{{ asset('login') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('login') }}/js/main.js"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
    </script>
    <!--livicons-->
    <script src="{{ asset('assets/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/login.js') }}" type="text/javascript"></script>
    <!-- end of global js -->
    {{-- <script type="text/javascript">var _Hasync= _Hasync|| [];
        _Hasync.push(['Histats.start', '1,4438908,4,0,0,0,00010000']);
        _Hasync.push(['Histats.fasi', '1']);
        _Hasync.push(['Histats.track_hits', '']);
        (function() {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
        })();</script> --}}
  </body>
</html>
