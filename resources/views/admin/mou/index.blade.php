@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$title}} List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/Buttons/css/buttons.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/pages/advbuttons.css') }}" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">{{$title}}</a></li>
        <li class="active">{{$title}} List</li>
    </ol>
</section>
<!-- 
<section class="content">
  
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        </h3>
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="flatbuttons">
                            <div class="text-left">
                                
                            </div>
                        </div>


                </div>
            </div>
        </div>
    </div>
</section> -->

<div class="panel-body">
    <div class="row">
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a href="#" class="list-group-item visitor">
                    <p class="pull-right">
                        <i class="fa fa-university f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_Mou}}</h4>
                    <p class="list-group-item-text">Semua MoU</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a href="#" class="list-group-item tumblr">
                    <p class="pull-right">
                        <i class="fa fa-check f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_Mou_aktif}}</h4>
                    <p class="list-group-item-text">MoU AKtif</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                
                <a href="#" class="list-group-item google-plus">
                    <p class="pull-right">
                        <i class="fa fa-times-circle f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_Mou_habis}}</h4>
                    <p class="list-group-item-text">MoU Berakhir</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                
                <a href="#" class="list-group-item vimeo">
                    <p class="pull-right">
                        <i class="fa fa-hourglass-half f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_Mou_akan_habis}}</h4>
                    <p class="list-group-item-text">MoU Akan Berakhir</p>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                {{$title}} List
                </h4>
                <div class="pull-right">
                    <!-- <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a> -->
                    <!-- <a onclick="mouForm()"" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-pencil"></span> MoU</a> -->
                    <a onclick="mou_by_countryForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-flag"></span> MoU by Country</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>Kategori</th>
                            <th>User</th>
                            <th>Nama Mitra</th>
                            <th>Negara</th>
                            <th>Tgl Pengesahan</th>
                            <th>Tgl Awal</th>
                            <th>Tgl Akhir</th>
                            <th>Jenis Kerma</th>
                            <th>Jenis Partner</th>
                            <th>Tingkat</th>
                            <th>Dokumen</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>   
</section>

<div class="modal fade" id="mouModal"  role="dialog"  aria-hidden="true " aria-labelledby="Modallabel3dflip"> 
	<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header bg-blue">
        <div class="panel-heading clearfix">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        </div>
            <div class="modal-body">
                <div class="row">
              
                    <div class="table-responsive">
                    <table class="table table-bordered " id="table2">
                        <thead>
                            <tr class="filters">
                                <th>No</th>
                                <th>Nama Mitra</th>
                                <th>Negara</th>
                                <th>Tgl Pengesahan</th>
                                <th>Tgl Awal</th>
                                <th>Tgl Akhir</th>
                                <th>Jenis Kerma</th>
                                <th>Jenis Partner</th>
                                <th>Tingkat</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/Buttons/js/scrollto.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/Buttons/js/buttons.js') }}" ></script>
@include('admin.mou.ajax');

@stop
