<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! route('admin.laporan_mahasiswa.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'benua', name:'benua'},
            { data: 'alamatDomisili', name:'alamatDomisili'},
            { data: 'penanggungjawab', name:'penanggungjawab'},
            { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
            { data: 'noPassport', name:'noPassport'},
            { data: 'passportBerakhir', name:'passportBerakhir'},
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

$("#status_permit").change(function() {
    var selectValues = $("#status_permit").val();
    if (selectValues ==""){
    var msg = "";
    $('#tabel').html(msg);
        }else{
        table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! url("admin/laporan_mahasiswa/data_laporan") !!}'+'/null/null/'+selectValues,
        columns: [
            { data: 'rownum' },
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'benua', name:'benua'},
            { data: 'alamatDomisili', name:'alamatDomisili'},
            { data: 'penanggungjawab', name:'penanggungjawab'},
            { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
            { data: 'noPassport', name:'noPassport'},
            { data: 'passportBerakhir', name:'passportBerakhir'}
            ]

        });
    }
 });

$("#negara").change(function() {
    var selectValues = $("#negara").val();
    if (selectValues ==""){
    var msg = "";
    $('#tabel').html(msg);
        }else{
        table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '{!! url("admin/laporan_mahasiswa/data_laporan") !!}'+'/null/null/'+selectValues,
        columns: [
            { data: 'rownum' },
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'benua', name:'benua'},
            { data: 'alamatDomisili', name:'alamatDomisili'},
            { data: 'penanggungjawab', name:'penanggungjawab'},
            { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
            { data: 'noPassport', name:'noPassport'},
            { data: 'passportBerakhir', name:'passportBerakhir'}
            ]

        });
    }
 });

 $("#tahunan").change(function() {
 var tahun = $("#tahunan").val();
 // var bulan = $("#bulan").val();
 var selectValues = $("#tahunan").val();
 if (selectValues ==""){
 var msg = "";
 $('#tabel').html(msg);
 }else{
     table = $('#table').DataTable({
     processing: true,
     serverSide: true,
     bDestroy: true,
     ajax: '{!! url("admin/laporan_mahasiswa/data_laporan") !!}'+'/'+tahun+'/'+'null/null',
     columns: [
        { data: 'rownum' },
        { data: 'npm', name:'npm'},
        { data: 'nama', name: 'nama' },
        { data: 'fakultas', name: 'fakultas'},
        { data: 'prodi', name:'prodi'},
        { data: 'asalNegara', name:'asalNegara'},
        { data: 'thnMasuk', name:'thnMasuk'},
        { data: 'agama', name:'agama'},
        { data: 'kelamin', name:'kelamin'},
        { data: 'benua', name:'benua'},
        { data: 'alamatDomisili', name:'alamatDomisili'},
        { data: 'penanggungjawab', name:'penanggungjawab'},
        { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
        { data: 'noPassport', name:'noPassport'},
        { data: 'passportBerakhir', name:'passportBerakhir'}
     ]
 });
 }
 });

 $("#thn, #bln").change(function() {
     var thn = $("#thn").val();
     var bln = $("#bln").val();
     var selectValues = $("#bln").val();
 if (selectValues ==""){
 var msg = "";
 $('#tabel').html(msg);
 }else{
     table = $('#table').DataTable({
     processing: true,
     serverSide: true,
     bDestroy: true,
     ajax: '{!! url("admin/laporan_mahasiswa/data_laporan") !!}'+'/'+thn+'/'+bln+'/'+'null',
     columns: [
            { data: 'rownum' },
            { data: 'npm', name:'npm'},
            { data: 'nama', name: 'nama' },
            { data: 'fakultas', name: 'fakultas'},
            { data: 'prodi', name:'prodi'},
            { data: 'asalNegara', name:'asalNegara'},
            { data: 'thnMasuk', name:'thnMasuk'},
            { data: 'agama', name:'agama'},
            { data: 'kelamin', name:'kelamin'},
            { data: 'benua', name:'benua'},
            { data: 'alamatDomisili', name:'alamatDomisili'},
            { data: 'penanggungjawab', name:'penanggungjawab'},
            { data: 'noHpPenanggungjawab', name:'noHpPenanggungjawab'},
            { data: 'noPassport', name:'noPassport'},
            { data: 'passportBerakhir', name:'passportBerakhir'}
     ]
 });
 }
 });



$('#lap_mahasiswa').on('change', function(){
    if(this.value == 'lap_bulanan'){
        $('#lap_bulanan').show();
        $('#lap_tahunan').hide();
        $('#lap_status').hide();
    }else if(this.value == 'lap_tahunan'){
        $('#lap_tahunan').show();
        $('#lap_bulanan').hide();
        $('#lap_status').hide();
    }else{
        $('#lap_tahunan').hide();
        $('#lap_bulanan').hide();
        $('#lap_status').show();
    }
});

$(".select2").select2();

</script>
