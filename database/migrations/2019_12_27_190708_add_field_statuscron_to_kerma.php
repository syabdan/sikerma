<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatuscronToKerma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kermas', function (Blueprint $table) {
            $table->datetime('status_cron_kerma')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kermas', function (Blueprint $table) {
            $table->dropColumn('status_cron_kerma');
        });
    }
}
