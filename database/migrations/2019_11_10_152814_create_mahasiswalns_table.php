<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswalnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswalns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('npm');
            $table->string('nama');
            $table->string('fakultas');
            $table->string('prodi');
            $table->string('asalNegara');
            $table->string('thnMasuk');
            $table->string('agama');
            $table->string('kelamin');
            $table->string('benua')->nullable();
            $table->string('alamatDomisili')->nullable();
            $table->string('penanggungjawab')->nullable();
            $table->string('noHpPenanggungjawab')->nullable();
            $table->string('alamatPenanggungjawab')->nullable();
            $table->string('noPassport')->nullable();
            $table->date('passportBerakhir')->nullable();
            $table->string('status_mahasiswa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswalns');
    }
}
