<?php

namespace App\Http\Controllers\Admin;

use\App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermitRequest;
use App\Permit;
use App\Unit;
use App\Mahasiswaln;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;
use App\Mail\NotifikasiPermit; 

class PermitController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
            ->where('users.id', Sentinel::getUser()->id)
            ->first();

        if(Sentinel::inRole('user')){
            // $mahasiswa_asings           = Permit::select(['mahasiswalns.id as id','mahasiswalns.npm','nama'])
            // ->join('users','users.id','permits.user_id')
            // ->join('mahasiswalns','mahasiswalns.id','permits.mahasiswaln_id')
            // ->where('users.id', Sentinel::getUser()->id)
            // ->whereMonth('tglBerakhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))
            // ->where('users.id', Sentinel::getUser()->id)->get();
            $mahasiswa_asings           = Mahasiswaln::select(['mahasiswalns.id as id','npm','nama'])
                                            ->join('users','unit_id','mahasiswalns.kode_prodi')
                                            ->where('users.id', Sentinel::getUser()->id)->get();
            if($fakultas){
                $permits                    = Permit::join('mahasiswalns','mahasiswalns.npm','permits.npm')
                                            ->join('units','units.id','mahasiswalns.kode_prodi')
                                            ->where('mahasiswalns.kode_fakultas', Sentinel::getUser()->unit_id)
                                            ->where('status_permit',1)->count();

                $tot_permit_akan_habis      = Permit::join('mahasiswalns','mahasiswalns.npm','permits.npm')
                                            ->join('units','units.id','mahasiswalns.kode_prodi')
                                            ->where('mahasiswalns.kode_fakultas', Sentinel::getUser()->unit_id)
                                            ->whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))))->count();
                                    
            }
            else{
                $permits                    = Permit::join('mahasiswalns','mahasiswalns.npm','permits.npm')
                                            ->join('units','units.id','mahasiswalns.kode_prodi')
                                            ->where('mahasiswalns.kode_prodi', Sentinel::getUser()->unit_id)
                                            ->where('status_permit',1)->count();
                                            
                $tot_permit_akan_habis      = Permit::join('mahasiswalns','mahasiswalns.npm','permits.npm')
                                            ->join('units','units.id','mahasiswalns.kode_prodi')
                                            ->where('mahasiswalns.kode_prodi', Sentinel::getUser()->unit_id)
                                            ->whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))))->count();
                                            
            }
            
                                            
            $units                      = Unit::all();

            if(Sentinel::getUser()->unit_id == 111){
            $mahasiswa_asings           = Mahasiswaln::all();
            $permits                    = Permit::where('status_permit',1)->count();
            $tot_permit_akan_habis      = Permit::whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))))->count();


            }
        }else{
            // $mahasiswa_asings           = Permit::select(['mahasiswalns.id as id','mahasiswalns.npm','nama'])
            //                             ->join('users','users.id','permits.user_id')
            //                             ->join('mahasiswalns','mahasiswalns.id','permits.mahasiswaln_id')
            //                             ->where('users.id', Sentinel::getUser()->id)
            //                             ->whereMonth('tglBerakhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))->get();
            $mahasiswa_asings           = Mahasiswaln::all();
            $permits                    = Permit::where('status_permit',1)->count();
            $units                      = Unit::all();
            // $tot_permit_habis           = Permit::where('status_permit',2)->count();
            $tot_permit_akan_habis      = Permit::whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))))->count();
        }
        
        // Show the page
        return view('admin.permit.index', compact('permits','mahasiswa_asings','units','tot_permit_akan_habis'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));             
        if(Sentinel::inRole('user')){
            $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
            ->where('users.id', Sentinel::getUser()->id)
            ->first();

            $permits = Permit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'permits.id as id','permits.npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit','nama','namaUnit'])
            // ->join('users','users.id','permits.user_id')
            ->join('mahasiswalns','mahasiswalns.npm','permits.npm')
            ->join('units','units.id','mahasiswalns.kode_prodi');
            if($fakultas){
            $permits->where('mahasiswalns.kode_fakultas', Sentinel::getUser()->unit_id);
            }else{
            $permits->where('mahasiswalns.kode_prodi', Sentinel::getUser()->unit_id);
            }
            
            $permits->orderBy('permits.id', 'DESC')->get();

            if(Sentinel::getUser()->unit_id == 111){
                $permits = Permit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'permits.id as id','permits.npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit','nama','namaUnit'])
            ->join('mahasiswalns','mahasiswalns.id','permits.mahasiswaln_id')
            ->join('units','units.id','mahasiswalns.kode_prodi')
            ->orderBy('permits.id', 'DESC')->get();
            }
        }else{
            $permits = Permit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'permits.id as id','permits.npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit','nama','namaUnit'])
            ->join('mahasiswalns','mahasiswalns.id','permits.mahasiswaln_id')
            ->join('units','units.id','mahasiswalns.kode_prodi')
            ->orderBy('permits.id', 'DESC')->get();
        }
        return DataTables::of($permits)

            ->addColumn('actions',function($permits) {
                if($permits->status_permit != 0){
                    $actions = '<a onclick="showForm('.$permits->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                    <a onclick="editForm('.$permits->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$permits->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
                }else{
                    $actions = '-';
                }
                
              
                return $actions;
            })
            ->addColumn('status_permit',function($permits) {
                if($permits->status_permit == 1){
                    $status_permit = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else if($permits->status_permit == 0){
                    $status_permit = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }else{
                    $status_permit = '<span class="btn btn-warning btn-xs btn-xs">Dalam Proses Perpanjangan</span>';
                }
              
                return $status_permit;
            })

            ->addColumn('file',function($permits) {
                if($permits->dokumenPermit == ''){
                    $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
                  }else{
                    $file = '<a href='. url('admin/permit/ambilFile/'.$permits->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';

                  }
                return $file;
            })

            ->addColumn('tglDikeluarkan',function($data) {
                return tanggal_indonesia($data->tglDikeluarkan);
            })
            
            ->addColumn('tglBerakhir',function($data) {
                return tanggal_indonesia($data->tglBerakhir);
            })
            
            ->addColumn('mahasiswa',function($data) {
                return $data->nama;
            })
            ->addColumn('unit',function($data) {
                return $data->namaUnit;
            })
            ->addColumn('sisa_waktu',function($data) {
                if($data->status_permit != 0){
                    $sisa = siswa_waktu_permit($data->tglBerakhir);
                }else{
                    $sisa = '-';
                }
                return $sisa;
            })

            ->rawColumns(['actions','status_permit','file','mahasiswa','unit','sisa_waktu'])
            ->make(true);
    }
    
    public function data_status($status)
    {
       
        DB::statement(DB::raw('set @rownum=0'));             
        if(Sentinel::inRole('user')){
            $fakultas = Mahasiswaln::select('*')->join('users','unit_id','mahasiswalns.kode_fakultas')
            ->where('users.id', Sentinel::getUser()->id)
            ->first();

            $permits = Permit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'permits.id as id','permits.npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit','nama','namaUnit'])
            // ->join('users','users.id','permits.user_id')
            ->join('mahasiswalns','mahasiswalns.npm','permits.npm')
            ->join('units','units.id','mahasiswalns.kode_prodi');
            if($fakultas){
            $permits->where('mahasiswalns.kode_fakultas', Sentinel::getUser()->unit_id);
            }else{
            $permits->where('mahasiswalns.kode_prodi', Sentinel::getUser()->unit_id);
            }
            if($status == 'H'){
                $permits->whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))));
            }
            if($status == 'F'){
                $permits->where('status_permit',1);
            }
            $permits->orderBy('permits.id', 'DESC')->get();

            if(Sentinel::getUser()->unit_id == 111){
                $permits = Permit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'permits.id as id','permits.npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit','nama','namaUnit'])
            ->join('mahasiswalns','mahasiswalns.id','permits.mahasiswaln_id')
            ->join('units','units.id','mahasiswalns.kode_prodi');
            if($status == 'H'){
                $permits->whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))));
            }
            if($status == 'F'){
                $permits->where('status_permit',1);
            }
            $permits->orderBy('permits.id', 'DESC')->get();
            }
        }else{
            $permits = Permit::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'permits.id as id','permits.npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit','nama','namaUnit'])
            ->join('mahasiswalns','mahasiswalns.id','permits.mahasiswaln_id')
            ->join('units','units.id','mahasiswalns.kode_prodi');
            if($status == 'H'){
                $permits->whereMonth('tglBerakhir', date('m', strtotime('+2 month', strtotime(date('Y-m-d')))));
            }
            if($status == 'F'){
                $permits->where('status_permit',1);
            }
            $permits->orderBy('permits.id', 'DESC')->get();
        }
        return DataTables::of($permits)

            ->addColumn('actions',function($permits) {
                if($permits->status_permit != 0){
                    $actions = '<a onclick="showForm('.$permits->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                    <a onclick="editForm('.$permits->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$permits->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
                }else{
                    $actions = '-';
                }
                
              
                return $actions;
            })
            ->addColumn('status_permit',function($permits) {
                if($permits->status_permit == 1){
                    $status_permit = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else if($permits->status_permit == 0){
                    $status_permit = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }else{
                    $status_permit = '<span class="btn btn-warning btn-xs btn-xs">Dalam Proses Perpanjangan</span>';
                }
              
                return $status_permit;
            })

            ->addColumn('file',function($permits) {
                if($permits->dokumenPermit == ''){
                    $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
                  }else{
                    $file = '<a href='. url('admin/permit/ambilFile/'.$permits->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';

                  }
                return $file;
            })

            ->addColumn('tglDikeluarkan',function($data) {
                return tanggal_indonesia($data->tglDikeluarkan);
            })
            
            ->addColumn('tglBerakhir',function($data) {
                return tanggal_indonesia($data->tglBerakhir);
            })
            
            ->addColumn('mahasiswa',function($data) {
                return $data->nama;
            })
            ->addColumn('unit',function($data) {
                return $data->namaUnit;
            })
            ->addColumn('sisa_waktu',function($data) {
                if($data->status_permit != 0){
                    $sisa = siswa_waktu_permit($data->tglBerakhir);
                }else{
                    $sisa = '-';
                }
                return $sisa;
            })

            ->rawColumns(['actions','status_permit','file','mahasiswa','unit','sisa_waktu'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $permits = Permit::where('id',$id)->first();    
        return json_encode($permits);
    }
    
    public function ambilFile($id)
    {
        $permits = Permit::where('id',$id)->first(); 

        return response()->file(storage_path('/uploads/permit/'.$permits->dokumenPermit));   
    
    }

    
    public function sendNotif()
    {
    
        $notif1 = Permit::whereMonth('tglBerakhir', date('m', strtotime('+1 month', strtotime(date('Y-m-d')))))->whereNull('status_notif')->get();
        
        if($notif1){

            foreach($notif1 as $data){
                //Update Notif Status
                Permit::find($data->id)->update(['status_notif' => 1]);

                // Kirim email
                Mail::to($data->mahasiswaln->email)
                ->send(new NotifikasiPermit($data));

            }
            
        }else{
            return 'Link has expired';

        }
    }
    
    public function getUpdatePermitConfirm($npm,$noPermitCode = null)
    {

            $data = Permit::where('npm',$npm)->first();
 
     
            if($noPermitCode == $data->noPermit && $data->token_notif == NULL) {
                // Update Status Permit menjadi dalam proses perpanjangan 
                Permit::find($data->id)->update(['status_permit' => 2,'token_notif' => rand()]);
                return 'Sukses Konfirmasi Permit dengan NPM = '.$npm.'. Silahkan lengkapi persyaratan dan laporkan ke Kantor Urusan Internasional & Kerjasama Universitas Islam Riau';
            } else{
                return 'code does not match';
            }

    }
    
    public function getMahasiswa($npm)
    {

        $data = Mahasiswaln::where('npm',$npm)->first();
        
        return json_encode($data);
    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PermitRequest $request)
    {
        $model      =   new Permit();
        //upload dokumen Permit
        if ($file = $request->file('dokumenPermit')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/permit/'))
            {
                File::makeDirectory( storage_path() . '/uploads/permit/');
            }
            $destinationPath = storage_path() . '/uploads/permit/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenPermit = $gambarName;
        }
        
        if($request['my-checkbox'] == 'on'){
            $stat_permit    =   1;
        }else{
            $stat_permit    =   0;

        }

        $model->npm                     = $request->npm;
        $model->mahasiswaln_id          = $request->mahasiswaln_id;
        $model->noPermit                = $request->noPermit;
        $model->tglDikeluarkan          = date('Y-m-d', strtotime($request->tglDikeluarkan));
        $model->tglBerakhir             = date('Y-m-d', strtotime($request->tglBerakhir));
        $model->status_permit           = $stat_permit;
        $model->user_id                 = Sentinel::getUser()->id;
        $model->unit_id                 = $request->unit_id;
        $model->save();

        return json_encode($model);
     
    }


    public function edit($permits)
    {
        
    }

    public function update(Request $request, $id)
    {

        $model      =   Permit::find($id);
        $permit     =   Permit::select('*')->where('id',$id)->first();

        //upload dokumen Permit
        if ($file = $request->file('dokumenPermit')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/permit/'))
            {
                File::makeDirectory( storage_path() . '/uploads/permit/');
            }
            $destinationPath = storage_path() . '/uploads/permit/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenPermit = $gambarName;
        }
        
        if($request['my-checkbox'] == 'on'){
            $stat_permit    =   1;
            $model->status_notif           = NULL;
            $model->status_notif_2         = NULL;
            $model->status_cron_permit     = NULL;

        }else{
            $stat_permit    =   0;

        }

        if($request->noPermit != $permit->noPermit ){
            $this->validate($request, [
                'noPermit' => 'required|unique:permits'
                ]);
            $model->noPermit                = $request->noPermit;
        }

        $model->npm                     = $request->npm;
        $model->tglDikeluarkan          = date('Y-m-d', strtotime($request->tglDikeluarkan));
        $model->tglBerakhir             = date('Y-m-d', strtotime($request->tglBerakhir));
        $model->status_permit           = $stat_permit;
        $model->user_id                 = Sentinel::getUser()->id;
        $model->unit_id                 = $request->unit_id;

        $model->update();

        return json_encode($model);
    }

    public function destroy($id)
    {
        $permits = Permit::find($id);
        if ($permits->delete()) {
            return json_encode($permits);
        } else {
            return Redirect::route('admin/permit')->withInput()->with('error', trans('permit/message.error.delete'));
        }
    }
}
