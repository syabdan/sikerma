<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Bidang extends Model
{
   
	protected $fillable = ['id','namaBidang', 'created_at'];

    protected $guarded = ['id'];

    public function subBidang()
    {
        return $this->hasMany('App\SubBidang');
    }
    
    public function rincianKerma()
    {
        return $this->hasMany('App\RincianKerma');
    }
}
