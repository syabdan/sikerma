<?php

namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\KermaRequest;
use App\Kerma;
use App\Country;
use App\JenisKerma;
use App\JenisPartner;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;
use Auth;

class MoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tot_Moa                = Kerma::where('kategori','MoA')->count();
        $tot_Moa_aktif          = Kerma::where([['kategori','MoA'],['status_kerma',1]])->count();
        $tot_Moa_habis          = Kerma::where([['kategori','MoA'],['status_kerma',2]])->count();
        $tot_Moa_akan_habis     = Kerma::where('kategori','MoA')->whereMonth('tglAkhir', date('m', strtotime('+1 month', strtotime(date('Y-m-d')))))->count();

        $title                  = "Memorandum of Agreement";
        // Show the page
        return view('admin.moa.index', compact('tot_Moa','tot_Moa_aktif','tot_Moa_habis','tot_Moa_akan_habis','title'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','user_id','country_id','no_kerma','kategori', 'email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','created_at'])
                ->where('kategori','MoA')->get();
        return DataTables::of($kermas)

            ->addColumn('actions',function($kermas) {
                $actions = '<a onclick="showForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                            <a onclick="editForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$kermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
              
                return $actions;
            })
            ->addColumn('status',function($kermas) {
                if($kermas->status_kerma == 1){
                    $status_kerma = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else if($kermas->status_kerma == 2){
                    $status_kerma = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }else{
                    $status_kerma = '<span class="btn btn-warning btn-xs btn-xs">Lain-lain</span>';
                }
              
                return $status_kerma;
            })
            ->addColumn('negara',function($kermas) {
       
                return $kermas->country->name;
            })
            ->addColumn('user',function($kermas) {
       
                return $kermas->user->first_name;
            })
            ->addColumn('jenisKerma',function($kermas) {
       
                return $kermas->jenisKerma->namaJenisKerma;
            })
            ->addColumn('jenisPartner',function($kermas) {
       
                return $kermas->jenisPartner->namaJenisPartner;
            })

            ->addColumn('file',function($kermas) {
              
                if($kermas->dokumenKerma == ''){
                    $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
                  }else{
                    $file = '<a href='. url('admin/kerma/ambilFile/'.$kermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
                  }
                return $file;
            })

            ->rawColumns(['actions','status','file','negara','user','jenisKerma','jenisPartner'])
            ->make(true);
    }
    public function dataCountry()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','user_id','country_id','no_kerma','kategori', 'email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','created_at'])
                ->where('kategori','MoA')->groupBy('country_id')->get();
        return DataTables::of($kermas)

            ->addColumn('actions',function($kermas) {
                $actions = '<a onclick="showForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                            <a onclick="editForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$kermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
              
                return $actions;
            })
            ->addColumn('status',function($kermas) {
                if($kermas->status_kerma == 1){
                    $status_kerma = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else if($kermas->status_kerma == 2){
                    $status_kerma = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }else{
                    $status_kerma = '<span class="btn btn-warning btn-xs btn-xs">Lain-lain</span>';
                }
              
                return $status_kerma;
            })
            ->addColumn('negara',function($kermas) {
       
                return $kermas->country->name;
            })
            ->addColumn('user',function($kermas) {
       
                return $kermas->user->first_name;
            })
            ->addColumn('jenisKerma',function($kermas) {
       
                return $kermas->jenisKerma->namaJenisKerma;
            })
            ->addColumn('jenisPartner',function($kermas) {
       
                return $kermas->jenisPartner->namaJenisPartner;
            })

            ->addColumn('file',function($kermas) {
                if($kermas->dokumenKerma != ''){
                    $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
                  }else{
                    $file = '<a href='. url('admin/kerma/ambilFile/'.$kermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
                  }
                return $file;
            })

            ->rawColumns(['actions','status','file','negara','user','jenisKerma','jenisPartner'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
