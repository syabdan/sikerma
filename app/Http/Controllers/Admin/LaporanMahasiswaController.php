<?php
namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MahasiswaRequest;
use App\Mahasiswaln;
use App\Country;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;
use PDF;

class LaporanMahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $mahasiswalns = Mahasiswaln::all();
        $countries = Country::all();

        $title = "Data Mahasiswa";
        // Show the page
        return view('admin.laporan_mahasiswa.index', compact('mahasiswalns','title','countries'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));                
        $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at'])->orderBy('id', 'DESC')->get();
        return DataTables::of($Mahasiswalns)

            ->addColumn('actions',function($Mahasiswalns) {
                $actions = '<a onclick="showForm('.$Mahasiswalns->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Mahasiswa"></i></a>
                            <a onclick="editForm('.$Mahasiswalns->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Mahasiswa"></i></a>';
                $actions .= '<a onclick="deleteForm('.$Mahasiswalns->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Mahasiswa"></i></a>';
              
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    
    public function dataLaporan($tahun,$bulan,$status)
    {
       
        DB::statement(DB::raw('set @rownum=0'));    
        if($status != 'null'){
            $Mahasiswalns = Mahasiswaln::join('permits','permits.npm','mahasiswalns.npm')->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'mahasiswalns.id as id','mahasiswalns.npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir','status_permit'])
            ->where('status_permit', $status)->orderBy('id', 'DESC')->get();

        }else if($bulan != 'null'){
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at'])
            ->whereMonth('created_at', $bulan)
            ->whereYear('created_at', $tahun)->orderBy('id', 'DESC')->get();
        }else{
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at'])
            ->whereYear('created_at', $tahun)->orderBy('id', 'DESC')->get();
        }
        return DataTables::of($Mahasiswalns)

            ->addColumn('actions',function($Mahasiswalns) {
                $actions = '<a onclick="showForm('.$Mahasiswalns->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Mahasiswa"></i></a>
                            <a onclick="editForm('.$Mahasiswalns->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Mahasiswa"></i></a>';
                $actions .= '<a onclick="deleteForm('.$Mahasiswalns->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Mahasiswa"></i></a>';
              
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    
    public function cetakLaporan(Request $request){

        $jenisLaporan = $request->jenisLaporan;

        DB::statement(DB::raw('set @rownum=0'));
        if($jenisLaporan == 'status_permit'){
            $status_permit = $request->status_permit;

            $Mahasiswalns = Mahasiswaln::join('permits','permits.npm','mahasiswalns.npm')->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'mahasiswalns.id as id','mahasiswalns.npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir','status_permit'])
            ->where('status_permit', $status_permit)->orderBy('id', 'DESC')->get();

        }else if($jenisLaporan != 'bulanan'){
            $tahun = $request->thn;
            $bulan = $request->bln;
            
            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at'])
            ->whereMonth('created_at', $bulan)
            ->whereYear('created_at', $tahun)->get();
        }else if($jenisLaporan != 'tahunan'){
            $tahun = $request->tahunan;

            $Mahasiswalns = Mahasiswaln::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','npm','nama','fakultas','prodi','asalNegara','thnMasuk','agama','kelamin','benua','alamatDomisili','penanggungjawab','noHpPenanggungjawab','alamatPenanggungjawab','noPassport','passportBerakhir', 'created_at'])
            ->whereYear('created_at', $tahun)->orderBy('id', 'DESC')->get();
        }
        $kepala_kantor = "Dr. Husnul Kausarian, M.Sc.";

        $pdf = PDF::loadView('admin.laporan_mahasiswa.pdf', compact('Mahasiswalns','kepala_kantor'))->setPaper('a4', 'landscape');
        // return PDF::loadView('frontend.pdf', compact('cetakSkpd'))->save('app/lapor/my_stored_file.pdf')->stream('download.pdf');
        return $pdf->stream('laporan_mahasiswa.pdf');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $Mahasiswalns = Mahasiswaln::where('id',$id)->first();    
        return json_encode($Mahasiswalns);
    }
    
    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MahasiswaRequest $request)
    {

        $model = new Mahasiswaln();
        $model->npm                     = $request->npm;
        $model->nama                    = $request->nama;
        $model->fakultas                = $request->fakultas;
        $model->prodi                   = $request->prodi;
        $model->asalNegara              = $request->asalNegara;
        $model->thnMasuk                = $request->thnMasuk;
        $model->agama                   = $request->agama;
        $model->kelamin                 = $request->kelamin;
        $model->benua                   = $request->benua;
        $model->alamatDomisili          = $request->alamatDomisili;
        $model->penanggungjawab         = $request->penanggungjawab;
        $model->noHpPenanggungjawab     = $request->noHpPenanggungjawab;
        $model->alamatPenanggungjawab   = $request->alamatPenanggungjawab;
        $model->noPassport              = $request->noPassport;
        $model->passportBerakhir        = date('Y-m-d', strtotime($request->passportBerakhir));
        $model->save();
        
        return json_encode($model);
    }


    public function edit($Mahasiswalns)
    {
        
    }

    public function update(Request $request, $id)
    {
        $model = Mahasiswaln::find($id);
        $model->npm                     = $request->npm;
        $model->nama                    = $request->nama;
        $model->fakultas                = $request->fakultas;
        $model->prodi                   = $request->prodi;
        $model->asalNegara              = $request->asalNegara;
        $model->thnMasuk                = $request->thnMasuk;
        $model->agama                   = $request->agama;
        $model->kelamin                 = $request->kelamin;
        $model->benua                   = $request->benua;
        $model->alamatDomisili          = $request->alamatDomisili;
        $model->penanggungjawab         = $request->penanggungjawab;
        $model->noHpPenanggungjawab     = $request->noHpPenanggungjawab;
        $model->alamatPenanggungjawab   = $request->alamatPenanggungjawab;
        $model->noPassport              = $request->noPassport;
        $model->passportBerakhir        = date('Y-m-d', strtotime($request->passportBerakhir));
        $model->update();
        
        return json_encode($model);
    }

    public function destroy($id)
    {
        $Mahasiswalns = Mahasiswaln::find($id);
        if ($Mahasiswalns->delete()) {
            return json_encode($Mahasiswalns);
        } else {
            return Redirect::route('admin/laporan_mahasiswa')->withInput()->with('error', trans('laporan_mahasiswa/message.error.delete'));
        }
    }
}
