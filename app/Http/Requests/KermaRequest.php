<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KermaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kategori' => 'required',
            // 'user_id' => 'required',
            'namaMitra' => 'required',
            'tglPengesahan' => 'required',
            'tglAwal' => 'required|before_or_equal:tglAkhir',
            'tglAkhir' => 'required|after_or_equal:tglAwal',
            'jenisPartner' => 'required',
            'tingkat' => 'required',
            'dokumenKerma' => 'required|mimes:pdf|max:2000',
            'email' => 'required|email',

        ];
    }
}
