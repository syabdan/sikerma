<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	protected $fillable = ['id','namaUnit','jPendidikan', 'kode_fakultas', 'created_at'];

	public function user()
    {
        return $this->hasMany('App\User');
	}
	
	public function tamu()
    {
        return $this->hasMany('App\Tamu');
	}
    
}
