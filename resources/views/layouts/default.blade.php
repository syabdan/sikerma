<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>SIKERMA | Sistem Informasi Kerjasama & Mahasiswa Asing</title>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Sistem Informasi Kerja Sama dan Mahasiswa Asing Universitas Islam Riau" />
    <meta name="author" content="Syabdan Dalimunthe" />
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.theme.css">
    
    <!-- Animate -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/responsive.css">
    @yield('header_styles')
  </head>
  <body>

    @yield('top')

    @yield('content')

    <!-- Footer Section Start -->
    <footer id="footer" class="footer-area section-padding">
      <div class="container">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="footer-logo"><img src="{{ asset('frontend') }}/img/logo.png" alt=""></h3>
                <div class="textwidget">
                  <p>SIKERMA merupakan sistem informasi kerjasama dan mahasiswa asing, berfungsi sebagai pusat informasi yang berkaitan dengan kerjasama dalam negeri maupun luar negeri, serta informasi mahasiswa asing yang ada di Universitas Islam Riau</p>
                </div>
                <div class="social-icon">
                  <a class="facebook" href="#"><i class="lni-facebook-filled"></i></a>
                  <a class="twitter" href="#"><i class="lni-twitter-filled"></i></a>
                  <a class="instagram" href="#"><i class="lni-instagram-filled"></i></a>
                  <a class="linkedin" href="#"><i class="lni-linkedin-filled"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel">Tautan Terkait</h3>
              <ul class="footer-link">
                <li><a href="https://uir.ac.id/">Website UIR</a></li>
                <li><a href="https://pmb.uir.ac.id/">PMB UIR</a></li>
                <li><a href="https://alumni.uir.ac.id/">Sistem Alumni</a></li>        
              </ul>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
              <h3 class="footer-titel"></h3>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.786685554316!2d101.452555!3d0.447421!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe6f83f2deb032e8f!2sIslamic%20University%20of%20Riau!5e0!3m2!1sen!2sid!4v1609132713972!5m2!1sen!2sid" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

            </div>
          </div>
        </div>  
      </div> 
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-content">
                <p>Copyright © {{ date('Y') }} <a rel="nofollow" href="https://uir.ac.id">Universitas Islam Riau</a> All Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </div>   
    </footer> 
    <!-- Footer Section End -->

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a>
    
    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('frontend') }}/js/jquery-min.js"></script>
    <script src="{{ asset('frontend') }}/js/popper.min.js"></script>
    <script src="{{ asset('frontend') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('frontend') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('frontend') }}/js/wow.js"></script>
    <script src="{{ asset('frontend') }}/js/jquery.nav.js"></script>
    <script src="{{ asset('frontend') }}/js/scrolling-nav.js"></script>
    <script src="{{ asset('frontend') }}/js/jquery.easing.min.js"></script>  
    <script src="{{ asset('frontend') }}/js/main.js"></script>
    <script src="{{ asset('frontend') }}/js/form-validator.min.js"></script>
    <script src="{{ asset('frontend') }}/js/contact-form-script.min.js"></script>
    @yield('footer_scripts')
  </body>
</html>
