<?php
 use App\Mail\NotifikasiPermit; 
include_once 'web_builder.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('nonaktif_permit', function () {
    \Artisan::call('nonaktif:permit');
});
Route::get('nonaktif_kerma', function () {
    \Artisan::call('nonaktif:kerma');
});
Route::get('sendnotif_permit', function () {
    \Artisan::call('send:NotifPermit');
});
Route::get('insert_mahasiswa', function () {
    \Artisan::call('insert:mahasiswa');
});


Route::pattern('slug', '[a-z0-9- _]+');

Route::group(['prefix' => 'admin', 'namespace'=>'Admin'], function () {

    # Error pages should be shown without requiring login
    Route::get('404', function () {
        return view('admin/404');
    });
    Route::get('500', function () {
        return view('admin/500');
    });

    # Lock screen
    Route::get('{id}/lockscreen', 'UsersController@lockscreen')->name('lockscreen');
    Route::post('{id}/lockscreen', 'UsersController@postLockscreen')->name('lockscreen');
    # All basic routes defined here
    Route::get('login', 'AuthController@getSignin')->name('login');
    Route::get('signin', 'AuthController@getSignin')->name('signin');
    Route::post('signin', 'AuthController@postSignin')->name('postSignin');
    Route::post('signup', 'AuthController@postSignup')->name('admin.signup');
    Route::post('forgot-password', 'AuthController@postForgotPassword')->name('forgot-password');

    # Forgot Password Confirmation
    Route::get('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm')->name('forgot-password-confirm');
    Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm');

    #Update Permit Status -> Dalam Pengurusan Permit
    Route::get('update-permit/{npm}/{noPermit}', 'PermitController@getUpdatePermitConfirm')->name('update-permit-confirm');

    # Logout
    Route::get('logout', 'AuthController@getLogout')->name('logout');

    # Account Activation
    Route::get('activate/{userId}/{activationCode}', 'AuthController@getActivate')->name('activate');
});


Route::group(['prefix' => 'admin',  'as' => 'admin.'], function () {
    # Dashboard / Index
    Route::get('/dashboard', 'JoshController@showHome')->name('dashboard');

});

Route::group(['prefix' => 'admin','namespace'=>'Admin', 'as' => 'admin.'], function () {
    
    Route::group(['middleware' => 'superadmin'], function () {

        # User Management
        Route::group([ 'prefix' => 'users'], function () {
            Route::get('data', 'UsersController@data')->name('users.data');
            Route::get('{user}/delete', 'UsersController@destroy')->name('users.delete');
            Route::get('{user}/confirm-delete', 'UsersController@getModalDelete')->name('users.confirm-delete');
            Route::get('{user}/restore', 'UsersController@getRestore')->name('restore.user');
            Route::post('{user}/passwordreset', 'UsersController@passwordreset')->name('passwordreset');
            Route::post('passwordreset', 'UsersController@passwordreset')->name('passwordreset');

        });
        Route::resource('users', 'UsersController');

        Route::get('deleted_users',['before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'])->name('deleted_users');

        # Group Management
        Route::group(['prefix' => 'groups'], function () {
            Route::get('{group}/delete', 'GroupsController@destroy')->name('groups.delete');
            Route::get('{group}/confirm-delete', 'GroupsController@getModalDelete')->name('groups.confirm-delete');
            Route::get('{group}/restore', 'GroupsController@getRestore')->name('groups.restore');
        });
        Route::resource('groups', 'GroupsController');

        # Master Bidang
        Route::group([ 'prefix' => 'bidang'], function () {
            Route::get('data', 'BidangController@data')->name('bidang.data');
            Route::get('{bidang}/delete', 'BidangController@destroy')->name('bidang.delete');
        });
        Route::resource('bidang', 'BidangController');
        
        # Master Subbidang
        Route::group([ 'prefix' => 'subbidang'], function () {
            Route::get('data', 'SubbidangController@data')->name('subbidang.data');
            Route::get('{subbidang}/delete', 'SubbidangController@destroy')->name('subbidang.delete');
        });
        Route::resource('subbidang', 'SubbidangController');
        
        # Master Unit
        Route::group([ 'prefix' => 'unit'], function () {
            Route::get('data', 'UnitController@data')->name('unit.data');
            Route::get('{unit}/delete', 'UnitController@destroy')->name('unit.delete');
        });
        Route::resource('unit', 'UnitController');
        


    });
Route::resource('users', 'UsersController');

// Route::group(['middleware' => ['superadmin']], function () {   // Diedit 15-04-2020
    # Laporan Kerjasama
    Route::group([ 'prefix' => 'laporan_kerma'], function () {
        Route::get('data', 'LaporanKermaController@data')->name('laporan_kerma.data');
        Route::get('{laporan_kerma}/delete', 'LaporanKermaController@destroy')->name('laporan_kerma.delete');
        Route::get('data_laporan/{tahun}/{bulan}/{negara}', 'LaporanKermaController@dataLaporan')->name('laporan_kerma.dataLaporan');
        Route::post('data_laporan', 'LaporanKermaController@cetakLaporan')->name('laporan_kerma.cetakLaporan');
    });
    Route::resource('laporan_kerma', 'LaporanKermaController');    

    # Laporan Mahasiswa
    Route::group([ 'prefix' => 'laporan_mahasiswa'], function () {
        Route::get('data', 'LaporanMahasiswaController@data')->name('laporan_mahasiswa.data');
        Route::get('{laporan_mahasiswa}/delete', 'LaporanMahasiswaController@destroy')->name('laporan_mahasiswa.delete');
        Route::get('data_laporan/{tahun}/{bulan}/{negara}', 'LaporanMahasiswaController@dataLaporan')->name('laporan_mahasiswa.dataLaporan');
        Route::post('data_laporan', 'LaporanMahasiswaController@cetakLaporan')->name('laporan_mahasiswa.cetakLaporan');
    });
    Route::resource('laporan_mahasiswa', 'LaporanMahasiswaController');
// });

    # Permit
    Route::group([ 'prefix' => 'permit'], function () {
        Route::get('data', 'PermitController@data')->name('permit.data');
        Route::get('get_mahasiswa/{npm}', 'PermitController@getMahasiswa')->name('permit.get_mahasiswa');
        Route::get('{permit}/delete', 'PermitController@destroy')->name('permit.delete');
        Route::get('ambilFile/{permit}', 'PermitController@ambilFile')->name('permit.ambilFile');
        Route::get('data_status/{status}', 'PermitController@data_status')->name('permit.data_status');

    });
    Route::resource('permit', 'PermitController');
    
    # Kerjasama
    Route::group([ 'prefix' => 'kerma'], function () {
        Route::get('data', 'KermaController@data')->name('kerma.data');
        Route::get('data_status/{status}', 'KermaController@data_status')->name('kerma.data_status');
        Route::get('{kerma}/delete', 'KermaController@destroy')->name('kerma.delete');
        Route::get('ambilFile/{kerma}', 'KermaController@ambilFile')->name('kerma.ambilFile');
    });
    Route::resource('kerma', 'KermaController');    
    
    # MoU
    Route::group([ 'prefix' => 'mou'], function () {
        Route::get('data', 'MouController@data')->name('mou.data');
        Route::get('dataCountry', 'MouController@dataCountry')->name('mou.dataCountry');
    });
    Route::resource('mou', 'MouController');    
   
    # MoA
    Route::group([ 'prefix' => 'moa'], function () {
        Route::get('data', 'MoaController@data')->name('moa.data');
        Route::get('dataCountry', 'MoaController@dataCountry')->name('moa.dataCountry');

    });
    Route::resource('moa', 'MoaController');     
    
    # Rincian Kerjasama
    Route::group([ 'prefix' => 'rinciankerma'], function () {
        Route::get('data', 'RinciankermaController@data')->name('rinciankerma.data');
        Route::get('data_sub/{idSub}', 'RinciankermaController@data_sub')->name('rinciankerma.data_sub');
        Route::get('ambil_sub_by_bidang/{idSub}', 'RinciankermaController@ambil_sub')->name('rinciankerma.ambil_sub');
        Route::get('{rinciankerma}/delete', 'RinciankermaController@destroy')->name('rinciankerma.delete');
    });
    Route::resource('rinciankerma', 'RinciankermaController');
    
    # Rincian Sub Kerjasama
    Route::group([ 'prefix' => 'rincian-subkerjasama'], function () {
        Route::get('data', 'RinciansubkermaController@data')->name('rincian-subkerjasama.data');
        Route::get('{rinciansubkerjasama}/delete', 'RinciansubkermaController@destroy')->name('rincian-subkerjasama.delete');
        Route::get('ambilFile/{idrincian}', 'RinciansubkermaController@ambilFile')->name('rincian-subkerjasama.ambilFile');
    });
    Route::resource('rincian-subkerjasama', 'RinciansubkermaController');
    
    # Mahasiswa
    Route::group([ 'prefix' => 'mahasiswa'], function () {
        Route::get('data', 'MahasiswaController@data')->name('mahasiswa.data');
        Route::get('data_wisuda', 'MahasiswaController@data_wisuda')->name('mahasiswa.data_wisuda');
        Route::get('data_sex/{sex}', 'MahasiswaController@data_sex')->name('mahasiswa.data_sex');
        Route::get('data_status/{status}', 'MahasiswaController@data_status')->name('mahasiswa.data_status');
        Route::get('{mahasiswa}/delete', 'MahasiswaController@destroy')->name('mahasiswa.delete');
        Route::get('ambilFile/{mahasiswa}', 'MahasiswaController@ambilFile')->name('mahasiswa.ambilFile');
    });
    Route::resource('mahasiswa', 'MahasiswaController');
    
    # Tamu
    Route::group([ 'prefix' => 'tamu'], function () {
        Route::get('data', 'TamuController@data')->name('tamu.data');
        Route::get('{tamu}/delete', 'TamuController@destroy')->name('tamu.delete');
        Route::get('ambilFile/{tamu}', 'TamuController@ambilFile')->name('tamu.ambilFile');
    });
    Route::resource('tamu', 'TamuController');

             
    // route::get('test_email', function(){

    //     $data = "test permit";
    
    //     // Send the activation code through email
    //     Mail::to('portal.senuju@gmail.com')
    //         ->send(new notifPermit('dasdad'));
    // });
    
  

    // route::get('test_api_mahasiswa_status', function(){
    //     return api_mahasiswa_asing_status();
    // });
    
    // route::get('test_api_mahasiswa_aktif_all', function(){
    //     return mahasiswa_aktif_all();
    // });
    
    // route::get('test_api_fakultas', function(){
    //     return api_fakultas();
    // });
    
    // route::get('test_api_prodi', function(){
    //     return api_prodi();
    // });

    // route::get('test_notif','PermitController@sendNotif')->name('test_notif');


});

# Remaining pages will be called from below controller method
# in real world scenario, you may be required to define all routes manually

// Route::group(['prefix' => 'admin'], function () {
//     Route::get('{name?}', 'JoshController@showView');
// });


# Forgot Password Confirmation
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');
Route::get('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@getForgotPasswordConfirm')->name('forgot-password-confirm');
# My account display and update details
Route::group(['middleware' => 'user'], function () {
    Route::put('my-account', 'FrontEndController@update');
    Route::get('my-account', 'FrontEndController@myAccount')->name('my-account');
});
Route::get('logout', 'FrontEndController@getLogout')->name('logout');
# contact form
Route::post('contact', 'FrontEndController@postContact')->name('contact');

#frontend views
Route::get('/', 'JoshController@showHome');

# Clear with Artisan Automaticly
Route::get('/clear_cache', function(){
    Artisan::call('cache:clear');
});

Route::get('/clear_view', function(){
    Artisan::call('view:clear');
});

Route::get('/clear_route', function(){
    Artisan::call('route:clear');
});
#End Clear with Artisan Automaticly


// Route::get('blog','BlogController@index')->name('blog');
// Route::get('blog/{slug}/tag', 'BlogController@getBlogTag');
// Route::get('blogitem/{slug?}', 'BlogController@getBlog');
// Route::post('blogitem/{blog}/comment', 'BlogController@storeComment');

Route::get('{name?}', 'FrontEndController@showFrontEndView');
# End of frontend views

route::get('test_mhs_aktif/{npm}', function($npm){
    return api_mahasiswa_asing_aktif_bynpm($npm);
});