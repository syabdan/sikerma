<script>


    
// OPEN MODAL ADD
function addSub_form(idRincian){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addSub_modal').modal('show');
    $('#addSub_modal form')[0].reset();
    $('#addSub_modal .modal-title').text('Create Rincian Kegiatan Kerjasama');
    $('#btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $('#idSub').val(idRincian);
    $("#rincian_kerma_id option[value='" + idRincian + "']").prop("selected", true).change();
    $('select[name="rincian_kerma_id"]').attr('disabled',true);

    $.ajax({
        type :'GET',
        url  :  "{{ url('admin/rinciankerma/ambil_sub_by_bidang') }}" + '/' + idRincian,
        dataType: "json",
        success:function(data)
        {
            $('select[name="sub_bidang_id"]').empty();
            $('select[name="sub_bidang_id"]').append('<option value="">[Pilih Sub Bidang]</option>');
            $.each(data, function(key, value) 
            {
                $('select[name="sub_bidang_id"]').append('<option value="'+ value.id +'">'+ value.namaSubBidang+'</option>');
            });
        }
    });
}

// OPEN MODAL Tabel
function detailFormSub(idRincian){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#modalTableSub').modal('show');
    $('#modalTableSub .modal-title').text(' Rincian Kegiatan Kerjasama');

    var tablesub;
    $(function() {
        tablesub = $('#tableSub').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            bDestroy: true,
            ajax: '{!! url('admin/rinciankerma/data_sub') !!}'+'/'+idRincian,
            columns: [
                { data: 'rownum' },
                { data: 'bentuk_kegiatan', name: 'bentuk_kegiatan' },
                { data: 'manfaat', name: 'manfaat' },
                { data: 'namaMitra', name: 'namaMitra' },
                { data: 'namaSubBidang', name: 'namaSubBidang' },
                { data: 'tglKegiatan', name: 'tglKegiatan' },
                { data: 'file', name: 'file' }
            ]
        });
        
    });

}


$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addSub_modal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#idSub').val();
        
        
            if(save_method=="add"){
            url = "{{ route('admin.rincian-subkerjasama.store') }}";
            } 
            else {
            url = "{{ url('admin/rincian-subkerjasama') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                        $('.btnSave').attr('disabled', true);
                        $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    $('#addSub_modal').modal('hide');
                    tablesub.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                   
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});



$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});

$(".select2").select2();

</script>
