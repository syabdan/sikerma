<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Unit::truncate();

        $api_prodi = api_prodi();
        $list = json_decode($api_prodi, true);
   
		for($a=0; $a < count($list); $a++){

			Unit::updateOrCreate([
                'id'               => $list[$a]['kode_prodi'],
                'namaUnit'         => $list[$a]['nama_prodi'],
                'jPendidikan'      => $list[$a]['jenjang_pendidikan'],
                'kode_fakultas'    => $list[$a]['kode_fakultas']
            ]);
		}
        
        $api_fakultas = api_fakultas();
        $list = json_decode($api_fakultas, true);
   
		for($a=0; $a < count($list); $a++){

			Unit::updateOrCreate([
                'id'               => $list[$a]['kode_fakultas'],
                'namaUnit'         => $list[$a]['nama_fakultas'],
                'jPendidikan'      => 'Fakultas'
            ]);
		}
    }
}
