<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsBentukKegiatanToRincianSubKermas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rincian_sub_kermas', function (Blueprint $table) {
            $table->string('bentuk_kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rincian_sub_kermas', function (Blueprint $table) {
            $table->dropColumn('bentuk_kegiatan');
        });
    }
}
