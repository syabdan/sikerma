@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Permit List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/Buttons/css/buttons.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/pages/advbuttons.css') }}" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Permit</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Permit</a></li>
        <li class="active">Permit / Izin Tinggal</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
            <div class="list-group">
                <a onclick="StatusModal('F')" class="list-group-item vimeo" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-battery-full f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{ $permits}} </h4>
                    <p class="list-group-item-text"><b>Permit Aktif</b></p>
                </a>
            </div>
        </div>
       
        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
            <div class="list-group">
                <a onclick="StatusModal('H')" class="list-group-item visitor" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-battery-quarter f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_permit_akan_habis}} </h4>
                    <p class="list-group-item-text"><b>Permit akan habis</b></p>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                   Permit / Izin Tinggal
                </h4>
                <div class="pull-right">
                @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
                    <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                @endif
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <!-- <th>No</th> -->
                            <th>Mahasiswa</th>
                            <th>Prodi</th>
                            <th>No Permit</th>
                            <th>Tgl Dikeluarkan</th>
                            <th>Tgl Berakhir</th>
                            <th>Sisa Waktu</th>
                            <th>Dokumen</th>
                            <th>Status</th>
                            @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
                            <th>Actions</th>
                            @endif

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus <b id="pesan_hapus"></b> ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" id="btnSave"><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
  </div>
</div>
<div class="modal fade" id="addModal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">

                <div class="form-group {{ $errors->first('jenisPartner', 'has-error') }}">
                    <label for="jenisPartner" class="col-sm-3 control-label">NPM *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Pilih Mahasiswa..." name="npm" id="npm" style="width: 100%;" required>
                        <option value="">Pilih Mahasiswa ... </option>
                           @foreach($mahasiswa_asings as $asing)
                            <option value="{{ $asing->npm }}"
                                    @if(old('npm') === '{{ $asing->npm }}' ) selected="selected" @endif >{{ $asing->npm }} - {{ $asing->nama}}
                            </option>
                           @endforeach

                        </select>
                        <span style="color: red;"><i><p id="npmError"></p></i></span>
                    </div>
                    <input type="hidden" id="id" name="idPermit" class="form-control refresh">
                    <input type="hidden" id="mahasiswaln_id" name="mahasiswaln_id" class="form-control refresh">
                    {!! $errors->first('npm', '<span class="help-block">:message</span>') !!}
                </div>
                <!-- <div class="form-group {{ $errors->first('unit_id', 'has-error') }}">
                    <label for="unit_id" class="col-sm-3 control-label">Unit/Prodi/Fakultas Tujuan *</label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Unit..." name="unit_id" id="unit_id" style="width: 100%;" required >
                        <option value="">Select Unit ... </option>
                           @foreach($units as $unit)
                            <option value="{{ $unit->id }}"
                                    @if(old('unit_id') === '{{ $unit->id }}' ) selected="selected" @endif >{{ $unit->namaUnit}}
                            </option>
                           @endforeach

                        </select>
                        <span style="color: red;"><i><p id="unit_idError"></p></i></span>

                    </div>
                    <span class="help-block">{{ $errors->first('unit_id', ':message') }}</span>
                </div> -->
                <div class="form-group">
                    <label for="noPermit" class="col-sm-3 control-label">No. Permit *</label>
                    <div class="col-sm-9">
                        <input type="text" id="noPermit" name="noPermit" class="form-control refresh" placeholder="No Permit" value="{!! old('noPermit') !!}">
                        <span style="color: red;"><i><p id="noPermitError"></p></i></span>
                    </div>
                    {!! $errors->first('noPermit', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="tglDikeluarkan" class="col-sm-3 control-label">Tanggal Dikeluarkan *</label>
                    
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker" id="tglDikeluarkan" name="tglDikeluarkan" class="form-control refresh" value="{!! old('tglDikeluarkan') !!}" autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="tglDikeluarkanError"></p></i></span>
                    </div>
                    {!! $errors->first('tglDikeluarkan', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="tglBerakhir" class="col-sm-3 control-label">Tanggal Berakhir *</label>
                    
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker"  id="tglBerakhir" name="tglBerakhir" class="form-control refresh" value="{!! old('tglBerakhir') !!}" autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="tglBerakhirError"></p></i></span>
                    </div>
                    {!! $errors->first('tglBerakhir', '<span class="help-block">:message</span>') !!}
                </div>      

                <div class="form-group {{ $errors->first('dokumen', 'has-error') }}">
                    <label for="dokumen" class="col-sm-3 control-label">Dokumen (*.pdf  Max 2000 KB) *</label>
                        <div class="col-sm-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 500px; max-height: 200px;"></div>
                            <div>
                            <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select File</span>
                            <span class="fileinput-exists">Change</span>
                            <input id="dokumenPermit" name="dokumenPermit" type="file" class="form-control" accept="application/pdf" required/></span>
                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                        </div>
                        <span class="help-block">{{ $errors->first('dokumen', ':message') }}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status_permit" class="col-sm-3 control-label">Status</label>            
                    <div class="col-sm-9">
                        <input type="checkbox" name="my-checkbox" id="status_permit" checked data-on-color="success" data-off-color="warning" data-animate>
                    </div>
                </div>

                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
					<button type="submit" class="btn btn-primary pull-right btnSave" id="btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
				</div>
            </div>
        </form>

        </div>
  </div>
</div>
<div class="modal fade" id="StatusModal"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog">
    	<div class="modal-content modal-lg">
        <div class="modal-header bg-blue">
        <br>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
     
            <div class="modal-body">
                <div class="row">
                    <div class="panel-body">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-bordered " id="table_status">
                                <thead>
                                    <tr class="filters">
                                    <!-- <th>No</th> -->
                                    <th>Mahasiswa</th>
                                    <th>Prodi</th>
                                    <th>No Permit</th>
                                    <th>Tgl Dikeluarkan</th>
                                    <th>Tgl Berakhir</th>
                                    <th>Sisa Waktu</th>
                                    <th>Dokumen</th>
                                    <th>Status</th>
                                    @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
                                    <th>Actions</th>
                                    @endif
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
				</div>

        </div>
    </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>

@include('admin.permit.ajax')



@stop
