<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRincianSubKermasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rincian_sub_kermas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rincian_kerma_id')->unsigned();
            $table->foreign('rincian_kerma_id')->references('id')->on('rincian_kermas');
            $table->integer('sub_bidang_id')->unsigned();
            $table->foreign('sub_bidang_id')->references('id')->on('sub_bidangs');
			// $table->string('pic')->nullable();
			$table->date('tglKegiatan');
			$table->text('manfaat');
			$table->string('dokumenKegiatan');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rincian_sub_kermas');
    }
}
