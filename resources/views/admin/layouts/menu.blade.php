<li {!! (Request::is('admin/mahasiswa') || Request::is('admin/wisuda_mhs') || Request::is('admin/permit') ? 'class="active"' : '') !!}>
        <a href="#">
        <i class="livicon" data-name="flag" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
        Mahasiswa Asing 
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/mahasiswa') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/mahasiswa') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Data Mahasiswa
                </a>
            </li>
            <!-- <li {!! (Request::is('admin/wisuda_mhs') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/wisuda_mhs') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Mahasiswa Wisuda
                </a>
            </li> -->
            <li {!! (Request::is('admin/permit') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/permit') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Permit / Izin Tinggal
                </a>
            </li>
        </ul>
</li>
<li {!! (Request::is('admin/kerma') || Request::is('admin/rinciankerma') || Request::is('admin/rincian_subkerjasama') || Request::is('admin/mou') || Request::is('admin/moa') ? 'class="active"' : '') !!}>
        <a href="#">
        <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
            data-loop="true"></i>
        Data Kerjasama 
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/kerma') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/kerma') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Kerjasama
                </a>
            </li>
            <!-- <li {!! (Request::is('admin/mou') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/mou') }}">
                    <i class="fa fa-angle-double-right"></i>
                    MoU
                </a>
            </li>
            <li {!! (Request::is('admin/moa') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/moa') }}">
                    <i class="fa fa-angle-double-right"></i>
                    MoA
                </a>
            </li> -->
            <li {!! (Request::is('admin/rinciankerma') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/rinciankerma') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Ruang Lingkup Kerjasama
                </a>
            </li>
            <li {!! (Request::is('admin/rincian-subkerjasama') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/rincian-subkerjasama') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Rincian Kegiatan Kerjasama
                </a>
            </li>
            <!-- <li {!! (Request::is('admin/mou') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/mou') }}">
                    <i class="fa fa-angle-double-right"></i>
                    MoU
                </a>
            </li>
            <li {!! (Request::is('admin/moa') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/moa') }}">
                    <i class="fa fa-angle-double-right"></i>
                    MoA
                </a>
            </li> -->
    
        </ul>
</li>
@if(Sentinel::inRole('superadmin') || Sentinel::inRole('admin'))

<li {!! (Request::is('admin/laporan_kerma') || Request::is('admin/laporan_mahasiswa') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="info" data-size="18" data-c="#418bca" data-hc="#418bca"
               data-loop="true"></i>
            <span class="title">Laporan</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/laporan_kerma') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/laporan_kerma') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Laporan Kerjasama
                </a>
            </li>
            <li {!! (Request::is('admin/laporan_mahasiswa') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/laporan_mahasiswa') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Laporan Status Permit Mahasiswa
                </a>
            </li>
        </ul>
</li>
@endif
<li {!! (Request::is('admin/tamu') ? 'class="active"' : '') !!}>
    <a href="{{ URL::to('admin/tamu') }}">
    <i class="livicon" data-name="map" data-c="#67C5DF" data-hc="#67C5DF" data-size="18"
               data-loop="true"></i>
        Kunjungan tamu/Narasumber Luar Negeri
    </a>
</li>
@if(Sentinel::inRole('superadmin'))
<li {!! (Request::is('admin/unit') || Request::is('admin/bidang') || Request::is('admin/sub_bidang') ? 'class="active"' : '') !!}>
    <a href="#">
        <i class="livicon" data-name="table" data-c="#418BCA" data-hc="#418BCA" data-size="18"
            data-loop="true"></i>
        <span class="title">Data Master</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li {!! (Request::is('admin/unit') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/unit') }}">
                <i class="fa fa-angle-double-right"></i>
                Unit
            </a>
        </li>
        <li {!! (Request::is('admin/bidang') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/bidang') }}">
                <i class="fa fa-angle-double-right"></i>
                Bidang
            </a>
        </li>
        <li {!! (Request::is('admin/sub_bidang') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/subbidang') }}">
                <i class="fa fa-angle-double-right"></i>
                Sub Bidang
            </a>
        </li>
    </ul>
</li>

<li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Users</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Users
                </a>
            </li>
            <li {!! (Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New User
                </a>
            </li>
            <li {!! ((Request::is('admin/users/*')) && !(Request::is('admin/users/create')) ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::route('admin.users.show',Sentinel::getUser()->id) }}">
                    <i class="fa fa-angle-double-right"></i>
                    View Profile
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Deleted Users
                </a>
            </li>
        </ul>
</li>
<!-- <li {!! (Request::is('admin/groups') || Request::is('admin/groups/create') || Request::is('admin/groups/*') ? 'class="active"' : '') !!}>
    <a href="#">
        <i class="livicon" data-name="users" data-size="18" data-c="#418BCA" data-hc="#418BCA"
            data-loop="true"></i>
        <span class="title">Groups</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li {!! (Request::is('admin/groups') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('admin/groups') }}">
                <i class="fa fa-angle-double-right"></i>
                Group List
            </a>
        </li>
        <li {!! (Request::is('admin/groups/create') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('admin/groups/create') }}">
                <i class="fa fa-angle-double-right"></i>
                Add New Group
            </a>
        </li>
    </ul>
</li> -->
@endif

<li >
    <a href="https://drive.google.com/file/d/1h1y2C4gz75M7YaDCQjMbqDUAhYS6xBRz/view?usp=sharing" target="_blank">
    <i class="livicon" data-name="printer" data-c="#67C5DF" data-hc="#67C5DF" data-size="18"
               data-loop="true"></i>
        Template Laporan
    </a>
</li>