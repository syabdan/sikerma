<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsStatusNotifSmsToPermits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permits', function (Blueprint $table) {
            $table->integer('status_sms')->comment('null=tidak aktif, 1=aktif')->nullable();
            $table->integer('status_sms_2')->comment('null=tidak aktif, 1=aktif')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permits', function (Blueprint $table) {
            $table->dropColumn('status_sms');
            $table->dropColumn('status_sms_2');
        });
    }
}
