<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(MahasiswaSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(JenisKermaSeeder::class);
        $this->call(JenisPartnerSeeder::class);
    }
}
