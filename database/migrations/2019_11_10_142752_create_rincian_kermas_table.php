<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRincianKermasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rincian_kermas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kerma_id')->unsigned();
            $table->foreign('kerma_id')->references('id')->on('kermas');
            $table->integer('bidang_id')->unsigned();
            $table->foreign('bidang_id')->references('id')->on('bidangs');
            $table->timestamps();
            $table->softdeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rincian_kermas');
    }
}
