<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tamu extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['id','country_id','unit_id', 'nama','noHp','email','tglKedatangan', 'tujuan','keterangan','dokumentasiTamu', 'created_at'];

    protected $guarded = ['id'];

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    } 
    
    public function country()
    {
        return $this->belongsTo('App\Country');
    } 
}
