<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			// add pic, unit_id, level
			$table->softDeletes();
			$table->string('pic')->nullable();
			$table->integer('unit_id')->unsigned()->nullable();
			// $table->integer('level')->default(3)->comment("1=Superadmin, 2=Admin, 3=Userbiasa");
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			// delete above columns
			$table->dropColumn(array('pic', 'unit_id'));
		});
	}

}
