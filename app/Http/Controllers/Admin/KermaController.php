<?php

namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\JoshController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\KermaRequest;
use App\Kerma;
use App\Country;
use App\JenisKerma;
use App\JenisPartner;
use App\Unit;
use App\User;
use File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
use DB;
use Auth;

class KermaController extends JoshController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kermas                     = Kerma::all();
        $countries                  = Country::all();
        $jenisKermas                = JenisKerma::all();
        $jenisPartners              = JenisPartner::all();
        $units                      = Unit::select('id','namaUnit','jPendidikan')->where('kode_fakultas', Sentinel::getUser()->unit_id)->get();
        if(Sentinel::inRole('user')){
            $tot_kerma                  = Kerma::whereIn('user_id', [Sentinel::getUser()->id, 4])->count();
            // $tot_kerma                  = Kerma::whereIn('user_id', [Sentinel::getUser()->id, 4])->count();
            $tot_kerma_aktif            = Kerma::where('status_kerma',1)->whereIn('user_id', [Sentinel::getUser()->id, 4])->count();
            // $tot_kerma_aktif            = Kerma::where('status_kerma',1)->whereIn('unit_id', [Sentinel::getUser()->unit_id, 111])->count();
            $tot_kerma_habis            = Kerma::where('status_kerma',2)->whereIn('user_id', [Sentinel::getUser()->id, 4])->count();
            // $tot_kerma_habis            = Kerma::where('status_kerma',2)->whereIn('unit_id', [Sentinel::getUser()->unit_id, 111])->count();
            $tot_kerma_akan_habis       = Kerma::whereMonth('tglAkhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))->whereIn('user_id', [Sentinel::getUser()->id, 4])->whereYear('tglAkhir', date('Y'))->count();
            // $tot_kerma_akan_habis       = Kerma::whereMonth('tglAkhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))->whereIn('user_id', [Sentinel::getUser()->id, 4])->count();
        }else{
            $tot_kerma                  = Kerma::count();
            $tot_kerma_aktif            = Kerma::where('status_kerma',1)->count();
            $tot_kerma_habis            = Kerma::where('status_kerma',2)->count();
            $tot_kerma_akan_habis       = Kerma::whereMonth('tglAkhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))->whereYear('tglAkhir', date('Y'))->count();
        }
        
        // Show the page
        return view('admin.kerma.index', compact('kermas','countries','jenisKermas','jenisPartners','tot_kerma','tot_kerma_aktif','tot_kerma_habis','tot_kerma_akan_habis','units'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));
        if(Sentinel::inRole('user')){
            $kermas = Kerma::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit')
            ->join('units','units.id','kermas.unit_id')
            ->where('status_kerma',1)
            // ->whereIn('kermas.user_id', [Sentinel::getUser()->id])
            ->whereIn('kermas.user_id', [Sentinel::getUser()->id, 4])
            ->orderBy('kermas.id', 'DESC')->get();

        }else{

            $kermas = Kerma::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit')
            ->join('units','units.id','kermas.unit_id')
            ->orderBy('kermas.id', 'DESC')
            ->get();

        }
        return DataTables::of($kermas)

            ->addColumn('actions',function($kermas) {
                if($kermas->status_kerma != 2){
                    $actions = '<a onclick="showForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                    <a onclick="editForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$kermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
                }else{
                    $actions = '-';
                }
              
                return $actions;
            })
            ->addColumn('status',function($kermas) {
                if($kermas->status_kerma == 1){
                    $status_kerma = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else{
                    $status_kerma = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }
                // else{
                //     $status_kerma = '<span class="btn btn-warning btn-xs btn-xs">Lain-lain</span>';
                // }
              
                return $status_kerma;
            })
            ->addColumn('negara',function($kermas) {
       
                return $kermas->country->name;
            })
            ->addColumn('user',function($kermas) {
       
                return $kermas->user->first_name;
            })
            ->addColumn('jenisPartner',function($kermas) {
       
                return $kermas->jenisPartner->namaJenisPartner;
            })

            ->addColumn('file',function($kermas) {
              if($kermas->dokumenKerma == ''){
                $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
              }else{
                $file = '<a href='. url('admin/kerma/ambilFile/'.$kermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
              }
                return $file;
            })

            ->addColumn('tglPengesahan',function($data) {
                return tanggal_indonesia($data->tglPengesahan);
            })
            
            ->addColumn('tglAwal',function($data) {
                return tanggal_indonesia($data->tglAwal);
            })
            
            ->addColumn('tglAkhir',function($data) {
                return tanggal_indonesia($data->tglAkhir);
            })

            ->rawColumns(['actions','status','file','negara','user','jenisPartner'])
            ->make(true);
    }
    
    public function data_status($status)
    {
  
        DB::statement(DB::raw('set @rownum=0'));
        if(Sentinel::inRole('user')){
            $kermas = Kerma::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit')
            ->join('units','units.id','kermas.unit_id')
            ->whereIn('kermas.user_id', [Sentinel::getUser()->id, 4])
            // ->whereIn('kermas.user_id', [Sentinel::getUser()->id, 4])
            ->where('status_kerma', $status)
            ->orderBy('kermas.id', 'DESC')->get();

        }else{
            $kermas = Kerma::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit'])
            ->join('units','units.id','kermas.unit_id')
            ->where('status_kerma', $status)
            ->orderBy('kermas.id', 'DESC')
            ->get();
        }

        // Jika Status Kerja Sama Akan Berakhir
        if($status == 3){
            if(Sentinel::inRole('user')){
                $kermas = Kerma::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit')
                ->join('units','units.id','kermas.unit_id')
                ->whereIn('kermas.user_id', [Sentinel::getUser()->id, 4])
                // ->whereIn('kermas.user_id', [Sentinel::getUser()->id, 4])
                ->whereMonth('tglAkhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))
                ->whereYear('tglAkhir', date('Y'))
                ->orderBy('kermas.id', 'DESC')->get();
    
            }else{
                $kermas = Kerma::select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'kermas.id as id','user_id','country_id','no_kerma','kategori', 'kermas.email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenis_kerma_id','jenis_partner_id','tingkat','dokumenKerma','status_kerma','namaUnit')
                ->join('units','units.id','kermas.unit_id')
                ->whereMonth('tglAkhir', date('m', strtotime('+3 month', strtotime(date('Y-m-d')))))
                ->whereYear('tglAkhir', date('Y'))
                ->orderBy('kermas.id', 'DESC')
                ->get();
            }
        }
        // End Jika status kerja sama akan berakhir

        return DataTables::of($kermas)

            ->addColumn('actions',function($kermas) {
                $actions = '<a onclick="showForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view Permit"></i></a>
                            <a onclick="editForm('.$kermas->id.')"><i class="livicon clearCacheForm" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update Permit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$kermas->id.')"><i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete Permit"></i></a>';
              
                return $actions;
            })
            ->addColumn('status',function($kermas) {
                if($kermas->status_kerma == 1){
                    $status_kerma = '<span class="btn btn-success btn-xs" >Aktif </span>';
                }else {
                    $status_kerma = '<span class="btn btn-danger btn-xs">Tidak Aktif </span>';
                }
                // else{
                //     $status_kerma = '<span class="btn btn-warning btn-xs btn-xs">Lain-lain</span>';
                // }
              
                return $status_kerma;
            })
            ->addColumn('negara',function($kermas) {
       
                return $kermas->country->name;
            })
            ->addColumn('user',function($kermas) {
       
                return $kermas->user->first_name;
            })
            ->addColumn('jenisPartner',function($kermas) {
       
                return $kermas->jenisPartner->namaJenisPartner;
            })

            ->addColumn('file',function($kermas) {
              if($kermas->dokumenKerma == ''){
                $file = '<i class="livicon" data-name="ban" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i>File is empty';
              }else{
                $file = '<a href='. url('admin/kerma/ambilFile/'.$kermas->id) .' target="_blank"><i class="livicon" data-name="download" data-size="14" data-c="#428BCA" data-hc="#000" data-loop="true"></i> Unduh</a>';
              }
                return $file;
            })

            ->addColumn('tglPengesahan',function($data) {
                return tanggal_indonesia($data->tglPengesahan);
            })
            
            ->addColumn('tglAwal',function($data) {
                return tanggal_indonesia($data->tglAwal);
            })
            
            ->addColumn('tglAkhir',function($data) {
                return tanggal_indonesia($data->tglAkhir);
            })

            ->rawColumns(['actions','status','file','negara','user','jenisPartner'])
            ->make(true);
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $kermas = Kerma::where('id',$id)->first();    
        return json_encode($kermas);
    }
    
    public function ambilFile($id)
    {
        $kermas = Kerma::where('id',$id)->first(); 

        return response()->file(storage_path('/uploads/kerma/'.$kermas->dokumenKerma));   
    
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(KermaRequest $request)
    {
        $model      =   new Kerma();
        //upload dokumen Permit
        if ($file = $request->file('dokumenKerma')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/kerma/'))
            {
                File::makeDirectory( storage_path() . '/uploads/kerma/');
            }
            $destinationPath = storage_path() . '/uploads/kerma/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenKerma = $gambarName;
        }
        

        $model->kategori                = $request->kategori;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->country_id              = $request->country_id;
        if($request->prodi_id != ''){
            $model->unit_id                 = $request->prodi_id;
        }else{
            $model->unit_id                 = Sentinel::getUser()->unit_id;
        }

        $model->user_id                 = Sentinel::getUser()->id;
        $model->jenis_partner_id        = $request->jenisPartner;
        $model->tingkat                 = $request->tingkat;
        $model->namaMitra               = $request->namaMitra;
        $model->tglPengesahan           = date('Y-m-d', strtotime($request->tglPengesahan));
        $model->tglAwal                 = date('Y-m-d', strtotime($request->tglAwal));
        $model->tglAkhir                = date('Y-m-d', strtotime($request->tglAkhir));
        $model->status_kerma            = $request->status_kerma;

        $model->save();

        return json_encode($model);
     
    }


    public function edit($kermas)
    {
        
    }

    public function update(Request $request, $id)
    {

        $model = Kerma::find($id);
  
        //upload dokumen Permit
        if ($file = $request->file('dokumenKerma')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            if(!file_exists( storage_path() . '/uploads/kerma/'))
            {
                File::makeDirectory( storage_path() . '/uploads/kerma/');
            }
            $destinationPath = storage_path() . '/uploads/kerma/';

            $file->move($destinationPath, $gambarName);
            $model->dokumenKerma = $gambarName;
        }
        
  

        $model->kategori                = $request->kategori;
        $model->noHp                    = $request->noHp;
        $model->email                   = $request->email;
        $model->country_id              = $request->country_id;
        if($request->prodi_id != ''){
            $model->unit_id                 = $request->prodi_id;
        }else{
            $model->unit_id                 = Sentinel::getUser()->unit_id;
        }

        $model->user_id                 = Sentinel::getUser()->id;
        $model->jenis_partner_id        = $request->jenisPartner;
        $model->tingkat                 = $request->tingkat;
        $model->namaMitra               = $request->namaMitra;
        $model->tglPengesahan           = date('Y-m-d', strtotime($request->tglPengesahan));
        $model->tglAwal                 = date('Y-m-d', strtotime($request->tglAwal));
        $model->tglAkhir                = date('Y-m-d', strtotime($request->tglAkhir));
        $model->status_kerma            = $request->status_kerma;

        if($request->status_kerma == 1){
            $model->status_cron_kerma            = NULL;

        }
        $model->update();

        return json_encode($model);

    }

    public function destroy($id)
    {
        $kermas = Kerma::find($id);
        if ($kermas->delete()) {
            return json_encode($kermas);
        } else {
            return Redirect::route('admin/kerma')->withInput()->with('error', trans('kerma/message.error.delete'));
        }
    }
}
