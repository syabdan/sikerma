<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.moa.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'kategori', name:'kategori'},
            { data: 'user'},
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'},
            { data: 'jenisKerma', name:'jenisKerma'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'file', orderable: false, searchable: false },
            { data: 'status', name:'status', orderable: false, searchable: false },
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});
function moa_by_countryForm(){
    $('#moaModal').modal('show');
    $('#moaModal .modal-title').text('Memorandum of Agreement List');

    table = $('#table2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.moa.dataCountry') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'negara'},
            { data: 'tglPengesahan', name:'tglPengesahan'},
            { data: 'tglAwal', name:'tglAwal'},
            { data: 'tglAkhir', name:'tglAkhir'},
            { data: 'jenisKerma', name:'jenisKerma'},
            { data: 'jenisPartner', name:'jenisPartner'},
            { data: 'tingkat', name:'tingkat'},
            { data: 'status', name:'status', orderable: false, searchable: false },
        ]
    });

};

$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});

$(".select2").select2();

</script>
