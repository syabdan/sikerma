@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{$title}}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/formelements.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/Buttons/css/buttons.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/pages/advbuttons.css') }}" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">{{$title}}</a></li>
        <li class="active">{{$title}}</li>
    </ol>
</section>


<div class="panel-body">
    <div class="row">
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a onclick="StatusModal('A')" class="list-group-item visitor" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-check-circle-o f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_mahasiswa_aktif}}</h4>
                    <p class="list-group-item-text">Mahasiswa Aktif</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a onclick="StatusModal('N')" class="list-group-item tumblr" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-close f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_mahasiswa_tidak_aktif}}</h4>
                    <p class="list-group-item-text" style="font-size:9pt"> Mahasiswa Tidak Aktif</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                
                <a onclick="SexModal('L')" class="list-group-item google-plus" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-male f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_pria}}</h4>
                    <p class="list-group-item-text">Laki-laki</p>
                </a>
                
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
            <div class="list-group">
                <a onclick="SexModal('P')" class="list-group-item vimeo" style="cursor: pointer;">
                    <p class="pull-right">
                        <i class="fa fa-female f"></i>
                    </p>
                    <h4 class="list-group-item-heading count">{{$tot_wanita}}</h4>
                    <p class="list-group-item-text">Perempuan</p>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                {{$title}} 
                </h4>
                <div class="pull-right">
                @if(Sentinel::inRole('superadmin'))
                    <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                @endif
                    <a onclick="wisudaForm()" class="btn btn-sm btn-default clearCacheForm"><span class="fa fa-graduation-cap"></span> Mahasiswa Wisuda</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <!-- <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>NPM</th>
                            <th>Nama</th>
                            <th>Fakultas</th>
                            <th>Prodi</th>
                            <th>Asal Negara</th>
                            <th>Tahun Masuk</th>
                            <th>Agama</th>
                            <th>Kelamin</th>
                            <th>Benua</th>
                            <th>Alamat Domisili</th>
                            <th>Penanggung Jawab (PJ)</th>
                            <th>No.HP PJ</th>
                            <th>Alamat PJ</th>
                            <th>No. Passport</th>
                            <th>Passport Berakhir</th>
                            @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin'))
                            <th>Actions</th>
                            @endif
                        </tr>
                    </thead> -->
                    <thead>
                        <tr>
                            <!-- <th>No</th> -->
                            @if(Sentinel::inRole('admin') || Sentinel::inRole('superadmin') || Sentinel::getUser()->unit_id == 111)
                            <th>Actions</th>
                            @endif
                            <th>NPM</th>
                            <th>Nama</th>
                            <th>Fakultas</th>
                            <th>Prodi</th>
                            <th>Asal Negara</th>
                            <th>Tahun Masuk</th>
                            <th>Agama</th>
                            <th>Kelamin</th>
                            <th>No Hp</th>
                            <th>Email</th>
                            <th>Benua</th>
                            <th>Alamat Domisili</th>
                            <th>Penanggung Jawab (PJ)</th>
                            <th>No.HP PJ</th>
                            <th>Alamat PJ</th>
                            <th>No. Passport</th>
                            <th>Passport Berakhir</th>
                            <th>Status</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus <b id="pesan_hapus"></b> ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" id="btnSave"><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
  </div>
</div>

<div class="modal fade" id="wisudaModal"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog" style="width: 80%">
    	<div class="modal-content modal-lg" style="width: 100%">
        <div class="modal-header bg-blue">
        <br>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
     
            <div class="modal-body">
                <div class="row">
                    <div class="panel-body">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-bordered " id="table_wisuda">
                                <thead>
                                    <tr class="filters">
                                        <th width="30">No</th>
                                        <th>NPM</th>
                                        <th>Nama</th>
                                        <th>Fakultas</th>
                                        <th>Prodi</th>
                                        <th>Kewarganegaraan</th>
                                        <th>Tahun Masuk</th>
                                        <th>Kelamin</th>
                                        <th>No HP</th>
                                        <th>Email</th>
                                        <th>Tempat Lahir</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Tahun Wisuda</th>
                                        <th>Periode Wisuda</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
				</div>

        </div>
    </div>
</div>

<div class="modal fade" id="SexModal"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog" style="width: 80%">
    	<div class="modal-content modal-lg" style="width: 100%">
        <div class="modal-header bg-blue">
        <br>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
     
            <div class="modal-body">
                <div class="row">
                    <div class="panel-body">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-bordered " id="table_sex">
                                <thead>
                                    <tr class="filters">
                                    <!-- <th>No</th> -->
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Fakultas</th>
                                    <th>Prodi</th>
                                    <th>Asal Negara</th>
                                    <th>Tahun Masuk</th>
                                    <th>Agama</th>
                                    <th>Kelamin</th>
                                    <th>Benua</th>
                                    <th>Alamat Domisili</th>
                                    <th>Penanggung Jawab (PJ)</th>
                                    <th>No.HP PJ</th>
                                    <th>Alamat PJ</th>
                                    <th>No. Passport</th>
                                    <th>Passport Berakhir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
				</div>

        </div>
    </div>
</div>

<div class="modal fade" id="StatusModal"  role="dialog"  aria-hidden="true"> 
	<div class="modal-dialog" style="width: 80%">
    	<div class="modal-content modal-lg" style="width: 100%">
        <div class="modal-header bg-blue">
        <br>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
     
            <div class="modal-body">
                <div class="row">
                    <div class="panel-body">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-bordered " id="table_status">
                                <thead>
                                    <tr class="filters">
                                    <!-- <th>No</th> -->
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Fakultas</th>
                                    <th>Prodi</th>
                                    <th>Asal Negara</th>
                                    <th>Tahun Masuk</th>
                                    <th>Agama</th>
                                    <th>Kelamin</th>
                                    <th>Benua</th>
                                    <th>Alamat Domisili</th>
                                    <th>Penanggung Jawab (PJ)</th>
                                    <th>No.HP PJ</th>
                                    <th>Alamat PJ</th>
                                    <th>No. Passport</th>
                                    <th>Passport Berakhir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
				</div>

        </div>
    </div>
</div>

<div class="modal fade" id="addModal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" style="width: 80%">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
        <br>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
        
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />
        <input type="hidden" id="id" name="id" class="form-control refresh">

            <div class="modal-body">  
                <div class="form-group">
                    <label for="npm" class="col-sm-3 control-label">NPM</label>
                    <div class="col-sm-9">
                        <input type="text" id="npm" name="npm" class="form-control refresh" placeholder="NPM" value="{!! old('npm') !!}" readonly>
                        <span style="color: red;"><i><p id="npmpError"></p></i></span>
                    </div>
                    {!! $errors->first('npm', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="nama" class="col-sm-3 control-label">Nama </label>
                    <div class="col-sm-9">
                        <input type="text" id="nama" name="nama" class="form-control refresh" placeholder="Nama" value="{!! old('nama') !!}" readonly>
                        <span style="color: red;"><i><p id="namaError"></p></i></span>
                    </div>
                    {!! $errors->first('nama', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="fakultas" class="col-sm-3 control-label">Fakultas </label>
                    <div class="col-sm-9">
                        <input type="text" id="fakultas" name="fakultas" class="form-control refresh" placeholder="Fakultas" value="{!! old('fakultas') !!}" readonly>
                        <span style="color: red;"><i><p id="fakultasError"></p></i></span>
                    </div>
                    {!! $errors->first('fakultas', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="prodi" class="col-sm-3 control-label">Prodi </label>
                    <div class="col-sm-9">
                        <input type="text" id="prodi" name="prodi" class="form-control refresh" placeholder="Prodi" value="{!! old('prodi') !!}" readonly>
                        <span style="color: red;"><i><p id="prodiError"></p></i></span>
                    </div>
                    {!! $errors->first('prodi', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="asalNegara" class="col-sm-3 control-label">Asal Negara </label>
                    <div class="col-sm-9">
                        <input type="text" id="asalNegara" name="asalNegara" class="form-control refresh" placeholder="Asal Negara" value="{!! old('asalNegara') !!}" readonly>
                        <span style="color: red;"><i><p id="asalNegaraError"></p></i></span>
                    </div>
                    {!! $errors->first('asalNegara', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="thnMasuk" class="col-sm-3 control-label">Tahun Masuk </label>
                    <div class="col-sm-9">
                        <input type="number" id="thnMasuk" name="thnMasuk" class="form-control refresh" placeholder="Tahun Masuk" value="{!! old('thnMasuk') !!}" readonly>
                        <span style="color: red;"><i><p id="thnMasukError"></p></i></span>
                    </div>
                    {!! $errors->first('thnMasuk', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <!-- <div class="form-group">
                    <label for="agama" class="col-sm-3 control-label">Agama </label>
                    <div class="col-sm-9">
                        <input type="text" id="agama" name="agama" class="form-control refresh" placeholder="Agama" value="{!! old('agama') !!}" readonly>
                        <span style="color: red;"><i><p id="agamaError"></p></i></span>
                    </div>
                    {!! $errors->first('agama', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="kelamin" class="col-sm-3 control-label">Kelamin </label>
                    <div class="col-sm-9">
                        <input type="text" id="kelamin" name="kelamin" class="form-control refresh" placeholder="Kelamin" value="{!! old('kelamin') !!}" readonly>
                        <span style="color: red;"><i><p id="kelaminError"></p></i></span>
                    </div>
                    {!! $errors->first('kelamin', '<span class="help-block">:message</span>') !!}
                </div>   -->
                
                <div class="form-group">
                    <label for="benua" class="col-sm-3 control-label">Benua </label>
                    <div class="col-sm-9">
                        <input type="text" id="benua" name="benua" class="form-control refresh" placeholder="Benua" value="{!! old('benua') !!}">
                        <span style="color: red;"><i><p id="benuaError"></p></i></span>
                    </div>
                    {!! $errors->first('benua', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="alamatDomisili" class="col-sm-3 control-label">Alamat Domisili </label>
                    <div class="col-sm-9">
                        <input type="text" id="alamatDomisili" name="alamatDomisili" class="form-control refresh" placeholder="Alamat Domisili" value="{!! old('alamatDomisili') !!}">
                        <span style="color: red;"><i><p id="alamatDomisiliError"></p></i></span>
                    </div>
                    {!! $errors->first('alamatDomisili', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="penanggungjawab" class="col-sm-3 control-label">Penanggung Jawab </label>
                    <div class="col-sm-9">
                        <input type="text" id="penanggungjawab" name="penanggungjawab" class="form-control refresh" placeholder="Penanggung Jawab" value="{!! old('penanggungjawab') !!}">
                        <span style="color: red;"><i><p id="penanggungJawabError"></p></i></span>
                    </div>
                    {!! $errors->first('penanggungjawab', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="noHpPenanggungjawab" class="col-sm-3 control-label">No.Hp PJ </label>
                    <div class="col-sm-9">
                        <input type="text" id="noHpPenanggungjawab" name="noHpPenanggungjawab" class="form-control refresh" placeholder="No.HP Penanggung Jawab" value="{!! old('noHpPenanggungjawab') !!}">
                        <span style="color: red;"><i><p id="noHpPenanggungJawabError"></p></i></span>
                    </div>
                    {!! $errors->first('noHpPenanggungjawab', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="alamatPenanggungjawab" class="col-sm-3 control-label">Alamat PJ </label>
                    <div class="col-sm-9">
                        <input type="text" id="alamatPenanggungjawab" name="alamatPenanggungjawab" class="form-control refresh" placeholder="Alamat Penanggung Jawab" value="{!! old('alamatPenanggungjawab') !!}">
                        <span style="color: red;"><i><p id="alamatPenanggungJawabError"></p></i></span>
                    </div>
                    {!! $errors->first('alamatPenanggungJawab', '<span class="help-block">:message</span>') !!}
                </div>
                
                <div class="form-group">
                    <label for="noPassport" class="col-sm-3 control-label">No.Passport </label>
                    <div class="col-sm-9">
                        <input type="text" id="noPassport" name="noPassport" class="form-control refresh" placeholder="No. Passport" value="{!! old('noPassport') !!}">
                        <span style="color: red;"><i><p id="noPassportError"></p></i></span>
                    </div>
                    {!! $errors->first('noPassport', '<span class="help-block">:message</span>') !!}
                </div>  
                
                <div class="form-group">
                    <label for="passportBerakhir" class="col-sm-3 control-label">Passport Berakhir </label>
                    
                    <div class="col-sm-9">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="livicon" data-name="laptop" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                    </div>
                        <input type="text" class="form-control datepicker"  id="passportBerakhir" name="passportBerakhir" class="form-control refresh" value="{!! old('passportBerakhir') !!}" autocomplete="off"/>
                    </div>
                        
                        <span style="color: red;"><i><p id="passportBerakhirError"></p></i></span>
                    </div>
                    {!! $errors->first('passportBerakhir', '<span class="help-block">:message</span>') !!}
                </div> 

                <div class="form-group">
                    <label for="noHp" class="col-sm-3 control-label">No.Hp </label>
                    <div class="col-sm-9">
                        <input type="text" id="noHp" name="noHp" class="form-control refresh" placeholder="No.HP" value="{!! old('noHp') !!}">
                        <span style="color: red;"><i><p id="noHpError"></p></i></span>
                    </div>
                    {!! $errors->first('noHp', '<span class="help-block">:message</span>') !!}
                </div>       
                
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email </label>
                    <div class="col-sm-9">
                        <input type="text" id="email" name="email" class="form-control refresh" placeholder="Email Mahasiswa" value="{!! old('email') !!}">
                        <span style="color: red;"><i><p id="emailError"></p></i></span>
                    </div>
                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                </div>       

                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
					<button type="submit" class="btn btn-primary pull-right btnSave" id="btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
				</div>
            </div>
        </form>

        </div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <!-- <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script> -->
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <!-- <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
@include('admin.mahasiswa.ajax')



@stop
