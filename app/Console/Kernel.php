<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\loggerLink',
        'App\Console\Commands\nonAktifkanKerma',
        'App\Console\Commands\nonAktifkanPermit',
        'App\Console\Commands\sendNotifPermit',
        'App\Console\Commands\insert_data_api_mahasiswa',

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('nonaktif:kerma')->everyMinute();
        $schedule->command('nonaktif:permit')->everyMinute();
        $schedule->command('send:NotifPermit')->everyMinute();
        $schedule->command('insert:mahasiswa')->weeklyOn(1, '6:00');

        // $schedule->command('nonaktif:kerma')->dailyAt('23:59');
        // $schedule->command('nonaktif:permit')->dailyAt('23:59');
        // $schedule->command('send:NotifPermit')->dailyAt('23:59');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
