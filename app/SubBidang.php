<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubBidang extends Model
{
    // protected $table = 'sub_bidangs';
	protected $fillable = ['id','bidang_id','namaSubBidang', 'created_at'];

    protected $guarded = ['id'];

    public function bidang()
    {
        return $this->belongsTo('App\Bidang');
    }  
    
    public function rincianSubKerma()
    {
        return $this->hasMany('App\RincianSubKerma');
    }
}
