@extends('layouts/default')

{{-- Page title --}}
@section('title')
SIKERMA
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
@stop

{{-- slider --}}
@section('top')
    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
           <img class="navbar-brand" src="{{ asset('frontend') }}/img/logo.png" alt="">       
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
                <li class="nav-item active">
                  <a class="nav-link" href="#hero-area">
                    Home
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#total_data">
                    Kerjasama
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#total_mhs_asing">
                    Mahasiswa Asing
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#footer">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- Navbar End -->
  
        <!-- Hero Area Start -->
        <div id="hero-area" class="hero-area-bg">
          <div class="container">      
            <div class="row">
              <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="contents">
                  <h2 class="head-title">Sistem Informasi Kerjasama dan Mahasiswa Asing</h2>
                  <p>SIKERMA merupakan sistem informasi kerjasama dan mahasiswa asing, berfungsi sebagai pusat informasi yang berkaitan dengan kerjasama dalam negeri maupun luar negeri, serta informasi mahasiswa asing yang ada di Universitas Islam Riau</p>
                  <div class="header-button">
                    <a href="{{ url('admin/signin') }}" class="btn btn-common">Masuk</i></a>
                    {{-- <a href="#" class="btn btn-border video-popup">Learn More</i></a> --}}
                  </div>
                </div>
              </div>
              <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="intro-img">
                  <img class="img-fluid" src="{{ asset('frontend') }}/img/undraw_business_deal_cpi9.svg" alt="">
                </div>            
              </div>
            </div> 
          </div> 
        </div>
        <!-- Hero Area End -->
  
      </header>
      <!-- Header Area wrapper End -->
@stop

{{-- content --}}
@section('content')
   <!-- Total data Section Start -->
  <section id="total_data" class="section-padding">
    <div class="container">
      <div class="row">
        <!-- Services item -->
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.3s">
            <div class="icon mb-2">
              <i class="lni-hand"></i>
            </div>
            <div class="services-content">
              <div class="number section-title mb-2" id="tamuElement"></div>
              <h3><a href="#rasio_kerjasama">Kunjungan Tamu/Narasumber</a></h3>
            </div>
          </div>
        </div>
        <!-- Services item -->
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
            <div class="icon mb-2">
              <i class="lni-users"></i>
            </div>
            <div class="services-content">
              <div class="number section-title mb-2" id="mahasiswaElement"></div>
              <h3><a href="#total_mhs_asing">Mahasiswa Asing Aktif</a></h3>
            </div>
          </div>
        </div>
        <!-- Services item -->
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.9s">
            <div class="icon  mb-2">
              <i class="lni-flag"></i>
            </div>
            <div class="services-content">
              <div class="number section-title mb-2" id="kermaElement"></div>
              <h3><a class="nav-link" href="#rasio_kerjasama">Jumlah Kerjasama</a></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Total data Section End -->

  <!-- Total Mahasiswa Asing Section start -->
  <section id="total_mhs_asing" class="section-padding">
    <div class="about-area section-padding bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-12 col-xs-12 info">
            <div class="about-wrapper wow fadeInLeft" data-wow-delay="0.3s">
              <div>
                <div class="site-heading">
                  <h2 class="section-title">Total Mahasiswa Asing</h2>
                </div>
                <div class="content">
                  <p>
                    Berikut Kami Sajikan Grafik Total Mahasiswa Asing Berdasarkan Negara di Universitas Islam Riau 
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 col-md-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
            {!! $bar->html() !!}
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Total Mahasiswa Asing Section End -->

   <!-- Rasio kerjasama section Start --> 
   <section id="rasio_kerjasama" class="pricing section-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-xs-12">
          <div class="table wow fadeInLeft" data-wow-delay="1.2s">
          
            <div class="title">
              <h4 class="section-title">Jenis Kerjasama</h4>
            </div>
            {!! $jenis_kerma_pie->html() !!}
          </div> 
        </div>
        <div class="col-lg-4 col-md-6 col-xs-12 active">
          <div class="table wow fadeInUp" id="active-tb" data-wow-delay="1.2s">
            
            <div class="title">
              <h4 class="section-title">Tamu / Narasumber</h4>
            </div>
            {!! $tamu_pie->html() !!}
         </div> 
        </div>
        <div class="col-lg-4 col-md-6 col-xs-12">
          <div class="table wow fadeInRight" data-wow-delay="1.2s">
    
            <div class="title">
              <h4 class="section-title">Kerjasama</h4>
            </div>
            {!! $kermas_pie->html() !!}
          </div> 
        </div>
      </div>
    </div>
  </section>
  <!-- Rasio kerjasama Table Section End -->

  <!-- kerjasama Section Start -->
  <section id="kerjasama_terkini" class="testimonial section-padding">
    <div class="container">
      <div class="section-header text-center">          
        <h2 class="section-title wow fadeInDown text-white" data-wow-delay="0.3s">Kerjasama Terkini</h2>
        <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div id="testimonials" class="owl-carousel wow fadeInUp" data-wow-delay="1.2s">
            @foreach($kerma_terkini as $kerma )

              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    {{-- <img src="assets/img/testimonial/img1.jpg" alt=""> --}}
                  </div>
                  <div class="info">
                    <h2><a href="#">{{ $kerma->country->name }}</a></h2>
                    <h3><a href="#">{{ $kerma->kategori }}</a></h3>
                  </div>
                  <div class="content">
                    <p class="description">{{ $kerma->jenisPartner->namaJenisPartner }} <br> <h6> {{ tanggal_indonesia($kerma->tglPengesahan) }} </h6> </p>
                  </div>
                </div>
              </div>

            @endforeach

            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- kerjasama Section End -->
    
@stop
{{-- footer scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/countUp_js/js/countUp.js') }}"></script>

    <script>
        var useOnComplete = false,
            useEasing = false,
            useGrouping = false,
        options = {
            useEasing: useEasing, // toggle easing
            useGrouping: useGrouping, // 1,000,000 vs 1000000
            separator: ',', // character to use as a separator
            decimal: '.' // character to use as a decimal
        };
        var demo = new CountUp("kermaElement", 12.52, {{ $kerma_count }}, 0, 6, options);
        demo.start();        
        var demo = new CountUp("tamuElement", 24.02, {{ $tamu_count }}, 0, 6, options);
        demo.start();
        var demo = new CountUp("mahasiswaElement", 125, {{ $mahasiswa_count }}, 0, 6, options);
        demo.start();



        var week_data;
        var permit_data;


    </script>
    {!! Charts::scripts() !!}
    {!! $kermas_pie->script() !!}
    {!! $jenis_kerma_pie->script() !!}
    {!! $tamu_pie->script() !!}
    {!! $bar->script() !!}
@stop
