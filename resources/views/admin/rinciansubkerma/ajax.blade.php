<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.rincian-subkerjasama.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'bentuk_kegiatan', name: 'bentuk_kegiatan' },
            { data: 'manfaat', name: 'manfaat' },
            { data: 'namaMitra', name: 'namaMitra' },
            { data: 'namaSubBidang', name: 'namaSubBidang' },
            { data: 'tglKegiatan', name: 'tglKegiatan' },
            { data: 'file', name: 'file' },
            { data: 'unit', name: 'unit' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

    
// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Rincian Sub Kerja Sama');
    $('#btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "editRinciansubkerma";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Rincian Sub Kerja Sama');
    $('#btnSave').removeClass('hide');
    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('admin/rincian-subkerjasama') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#sub_bidang_id option[value='" + data.sub_bidang_id + "']").prop("selected", true).change();
                $("#rincian_kerma_id option[value='" + data.rincian_kerma_id + "']").prop("selected", true).change();
                $("#tglKegiatan").val(data.tglKegiatan);
                $("#manfaat").val(data.manfaat);
                $("#unit_id").val(data.unit_id);
                $("#bentuk_kegiatan").val(data.bentuk_kegiatan);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Rincian Sub Kerja Sama');

    $.ajax({
        url: "{{ url('admin/rincian-subkerjasama') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#sub_bidang_id option[value='" + data.sub_bidang_id + "']").prop("selected", true).change();
                $("#rincian_kerma_id option[value='" + data.rincian_kerma_id + "']").prop("selected", true).change();
                $("#tglKegiatan").val(data.tglKegiatan);
                $("#manfaat").val(data.manfaat);
                $("#unit_id").val(data.unit_id);
                $("#bentuk_kegiatan").val(data.bentuk_kegiatan);
                $('#btnSave').addClass('hide');
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete Rincian Sub Kerja Sama');

    $.ajax({
        url: "{{ url('admin/rincian-subkerjasama') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#pesan_hapus").text(data.id);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();

            url = "{{ url('admin/rincian-subkerjasama/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#id').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.rincian-subkerjasama.store') }}";
            } 
            else {
            url = "{{ url('admin/rincian-subkerjasama') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                        $('.btnSave').attr('disabled', true);
                        $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    $('#addModal').modal('hide');
                    table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                   
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});



$(function () {
    $('.datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    })
});
</script>
