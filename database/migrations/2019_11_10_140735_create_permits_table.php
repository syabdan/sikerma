<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('npm');
            $table->string('noPermit');
            $table->date('tglDikeluarkan');
            $table->date('tglBerakhir');
            $table->string('dokumenPermit');
            $table->integer('status_permit')->comment("0=Tidak Aktif, 1=Aktif, 2=Proses Perpanjangan");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permits');
    }
}
