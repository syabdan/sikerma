<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kerma extends Model
{
    protected $fillable = ['id','user_id','country_id','no_kerma','kategori', 'email','noHp','tglPengesahan','tglAwal','tglAkhir','namaMitra','jenisPartner','tingkat','dokumenKerma','status_kerma','created_at','status_cron_kerma'];

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    } 
    
    public function country()
    {
        return $this->belongsTo('App\Country');
    } 
    
    public function jenisKerma()
    {
        return $this->belongsTo('App\JenisKerma');
    } 
    
    public function jenisPartner()
    {
        return $this->belongsTo('App\JenisPartner');
    } 
    
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    } 
}
