<!DOCTYPE html>
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Laporan Kantor Urusan Internasional & Kerjasaama Universitas Islam Riau</title>
    <style type="text/css" >
      body, td, th, input, select {
       font: 8pt Verdana, Arial, sans-serif;
      }
      td, th {
       padding: 1px;
       vertical-align: bottom;
       text-align: left;
      }
      td {
       vertical-align: top;
      }
      th {
       font-weight: bold;
       vertical-align: bottom;
       border-bottom: 1pt solid silver;
       background-color: #9ACD32;
       text-align: center;
       padding: 5px;

      }
      h1,h2{
        margin: 0;
        padding: 0;
      }
      img{
        height: 40;
      }
    }
    .tanda_tangan{
      float:left;
      text-align:center;
      width:50%;
      margin-top: 50px;
    }
   </style>
  </head>
  <table cellPadding=0>
    <tr>
      <td width="10"><img src="{{ asset('assets/img/uir.png') }}" alt="" width="80" ></td>
      <td>
        <h1>Kantor Urusan Internasional & Kerjasama</h1>
        <h2>Universitas Islam Riau</h2>
    </td>
    </tr>
  </table>

  <br><br>


  <body>
    <table id=titleTable cellSpacing=0 cellPadding=0 width="100%" border=1>
    <thead>
    <tr>
    <th>No</th>
    <th>Kategori</th>
    <!-- <th>User</th> -->
    <th>Nama Mitra</th>
    <th>Negara</th>
    <th>Tgl Pengesahan</th>
    <th>Tgl Awal</th>
    <th>Tgl Akhir</th>
    <!-- <th>Jenis Kerma</th> -->
    <th>Jenis Partner</th>
    <th>Tingkat</th>
    <th>Status Kerma</th>
    </tr>
    </thead>
    <tbody>
      <?php $no=1;?>
      @foreach($kermas as $kerma)
      @if($kerma->status_kerma == 1)
        {{$status_kerma = "Aktif"}}
      @elseif($kerma->status_kerma == 2)
        {{$status_kerma = "Tidak Aktif"}}
      @elseif($kerma->status_kerma == 3)
        {{$status_kerma = "Lain-lain"}}
      @endif
        <tr>
            <td>{{$no}}</td>
            <td>{{$kerma->kategori}}</td>
            <!-- <td>{{$kerma->user->first_name}}</td> -->
            <td>{{$kerma->namaMitra}}</td>
            <td>{{$kerma->country->name}}</td>
            <td>{{tanggal_indonesia($kerma->tglPengesahan)}}</td>
            <td>{{tanggal_indonesia($kerma->tglAwal)}}</td>
            <td>{{tanggal_indonesia($kerma->tglAkhir)}}</td>
            <!-- <td>{{$kerma->jenisKerma}}</td> -->
            <td>{{$kerma->jenisPartner->namaJenisPartner}}</td>
            <td>{{$kerma->tingkat}}</td>
            <td>{{$status_kerma}}</td>
        </tr>
        <?php $no++;?>
      @endforeach
    </tbody>
    </table>

      <!-- <h5>Tanggal Cetak: {!! date('d/m/Y') !!}</h5> -->

    <br><br>
    <div class="tanda_tangan" style="float:right">
    <div>Pekanbaru, {{ tanggal_indonesia(date('Y-m-d'),false) }}</div>


        <h5>KEPALA KUI & Kerjasama</h5>


    <br><br>
    <div ><span style="text-transform:uppercase;text-decoration:underline;font-weight:bold">{{$kepala_kantor}}</span></div>

</div>


  </body>

</html>
