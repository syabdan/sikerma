<?php

use Illuminate\Database\Seeder;

class JenisPartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statement = "INSERT INTO ".env('DB_PREFIX')."`jenis_partners` (`id`, `namaJenisPartner`) VALUES
            (1, 'Instansi Pemerintahan Dalam Negeri'),
            (2, 'Instansi Pemerintahan Luar Negeri'),
            (3, 'Institusi Pendidikan Dalam Negeri'),
            (4, 'Institusi Pendidikan Luar Negeri'),
            (5, 'Dunia Usaha Dalam Negeri'),
            (6, 'Dunia Usaha Luar Negeri'),
            (7, 'Organisasi Dalam Negeri'),
            (8, 'Organisasi Luar Negeri')
            ;";
        DB::unprepared($statement);
    }
}
