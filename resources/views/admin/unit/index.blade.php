@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Unit List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Units</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Units</a></li>
        <li class="active">Daftar Unit</li>
    </ol>
</section>


<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                   Daftar Unit
                </h4>
                <div class="pull-right">
                    <a onclick="addForm()" class="btn btn-sm btn-default clearCacheForm"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>No</th>
                            <th>Nama Unit</th>
                            <th>Jenjang Pendidikan</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
<div class="modal fade" id="delete_confirm"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="DELETE" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="row">
                    Apakah Anda yakin untuk menghapus <b id="pesan_hapus"></b> ?
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>Nanti Saja</button>
                        <button type="submit" class="btn btn-primary pull-right" id="btnSave"><i class="fa fa-save"></i>   Hapus</button>
                    </div>
                </div>
            </div>
        </form>

        </div>
  </div>
</div>
<div class="modal fade" id="addModal"  role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        <div class="modal-header bg-blue">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="_method"  />

            <div class="modal-body">
                <div class="form-group {{ $errors->first('jPendidikan', 'has-error') }}">
                    <label for="jPendidikan" class="col-sm-3 control-label">Jenjang Pendidikan </label>
                    <div class="col-sm-9">
                        <select class="form-control select2" title="Select Jenjang Pendidikan..." name="jPendidikan" id="jPendidikan" style="width: 100%;">
                           
                            <option value="D3"
                                    @if(old('D3') === 'D3' ) selected="selected" @endif >D3
                            </option>
                            <option value="S1"
                                    @if(old('S1') === 'S1' ) selected="selected" @endif >S1
                            </option>
                            <option value="S2"
                                    @if(old('S2') === 'S2' ) selected="selected" @endif >S2
                            </option>
                            <option value="S3"
                                    @if(old('S3') === 'S3' ) selected="selected" @endif >S3
                            </option>
                            <option value=''
                                    @if(old('') === '' ) selected="selected" @endif >Lainnya
                            </option>
                           

                        </select>
                    </div>
                    <span class="help-block">{{ $errors->first('jPendidikan', ':message') }}</span>
                </div>
               
                <div class="form-group {{ $errors->first('namaUnit', 'has-error') }}">
                <label for="namaUnit" class="col-sm-3 control-label">Nama Unit *</label>
                    <div class="col-sm-9">
                        <input type="text" id="namaUnit" name="namaUnit" class="form-control refresh" placeholder="Nama Unit" value="{!! old('namaUnit') !!}">
                        <span style="color: red;"><i><p id="namaUnitError"></p></i></span>
                    </div>
                    <input type="hidden" id="idUnit" name="idUnit" class="form-control refresh">
                    {!! $errors->first('namaUnit', '<span class="help-block">:message</span>') !!}
                
                </div>

               
                <div class="modal-footer">
					<button type="button" class="btn btn-danger " data-dismiss="modal"><i class="fa fa-times"></i>@lang('button.cancel')</button>
					<button type="submit" class="btn btn-primary pull-right btnSave" id="btnSave"><i class="fa fa-save"></i>   @lang('button.save')</button>
				</div>
            </div>
        </form>

        </div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/custom_sweetalert.js') }}" type="text/javascript"></script>

@include('admin.unit.ajax')



@stop
