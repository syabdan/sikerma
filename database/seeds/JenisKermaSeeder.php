<?php

use Illuminate\Database\Seeder;

class JenisKermaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statement = "INSERT INTO ".env('DB_PREFIX')."`jenis_kermas` (`id`, `namaJenisKerma`) VALUES
            (1, 'Dunia Usaha'),
            (2, 'Dunia Industri'),
            (3, 'Instansi Pendidikan'),
            (4, 'Instansi Pemerintahan'),
            (5, 'Organisasi');";
        DB::unprepared($statement);

    }
}
