<script>
var table;
$(function() {
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.bidang.data') !!}',
        columns: [
            { data: 'rownum' },
            { data: 'namaBidang', name: 'namaBidang' },
            { data: 'created_at', name:'created_at'},
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

    
// OPEN MODAL ADD
function addForm(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Create Bidang');
    $('#btnSave').removeClass('hide');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "editBidang";
    $('input[name=_method]').val('PUT');
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Edit Bidang');
    $('#btnSave').removeClass('hide');
    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('admin/bidang') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#idBidang').val(data.id);
                $("#namaBidang").val(data.namaBidang);

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL SHOW
function showForm(id){
    $('#addModal').modal('show');
    $('#addModal form')[0].reset();
    $('#addModal .modal-title').text('Show Bidang');

    $.ajax({
        url: "{{ url('admin/bidang') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#idBidang').val(data.id);
                $("#namaBidang").val(data.namaBidang).attr('readonly', true);
                $('#btnSave').addClass('hide');
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL DELETE
function deleteForm(id){
    $('#delete_confirm').modal('show');
    $('#delete_confirm form')[0].reset();
    $('#delete_confirm .modal-title').text('Delete Bidang');

    $.ajax({
        url: "{{ url('admin/bidang') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#idBidang').val(data.id);
                $("#pesan_hapus").text(data.namaBidang);
           
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

$('#delete_confirm form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#idBidang').val();

            url = "{{ url('admin/bidang/') }}/"+id+"/delete";
          
            $.ajax({
            
                url : url,
                type : "GET",
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menghapus data",
                        type: 'success',
                        timer: 2000
                    });
                    
                        $('#delete_confirm').modal('hide');
                        table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menghapus data",
                        type: 'error',
                        timer: 2000
                    });
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});
// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#addModal form').on('submit', function(e){
    if (!e.isDefaultPrevented()){
            var id=$('#idBidang').val();
        
            if(save_method=="add"){
            url = "{{ route('admin.bidang.store') }}";
            } 
            else {
            url = "{{ url('admin/bidang') }}"+'/'+id;
            } 
        
            var inputData = new FormData($(this)[0]);
            $.ajax({
            
                url : url,
                type : "POST",
                enctype: 'multipart/form-data',
                data : inputData,
            //  data: $('#addModal form').serialize(),
                beforeSend: function() {
                        $('.btnSave').attr('disabled', true);
                        $('.btnSave').html('<i class="fa fa-gear"></i> Prosessing ... ');
                },
                complete: function() {
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                },
                processData: false,
                contentType: false,
                cache: false,
                dataType: "JSON",
                success : function(syabdan){
                
                swal({
                        title: 'Success',
                        text: "Berhasil menyimpan data",
                        type: 'success',
                        timer: 2000
                    });
                    $('.btnSave').attr('disabled', false);
                    $('.btnSave').html('<i class="fa fa-save"></i> Save');
                    $('#addModal').modal('hide');
                    table.ajax.reload();
                
                },
                error : function(syabdan){
                    swal({
                        title: 'Terjadi Kesalahan!',
                        text: "Gagal Menyimpan data",
                        type: 'error',
                        timer: 2000
                    });
                    if(syabdan['responseJSON']['errors']) {
                            if(syabdan['responseJSON']['errors']['namaBidang']){
                                $( '#namaBidangError' ).html(syabdan['responseJSON']['errors']['namaBidang'][0]);
                            }

                          
                    }
                    // alert("Tidak dapat menyimpan data!");
                }
            });
            return false;
        }
});


$(".select2").select2();

</script>
