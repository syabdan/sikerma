<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    protected $fillable = ['id','npm','noPermit','tglDikeluarkan','tglBerakhir','dokumenPermit','status_permit', 'created_at','mahasiswaln_id','status_notif','token_notif','status_notif_2','status_sms','status_sms_2','status_cron_permit','unit_id'];

    protected $guarded = ['id'];

    public function mahasiswaln()
    {
        return $this->belongsTo('App\Mahasiswaln');
    } 
}
